<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
        'class'=>"form-control input-lg" ,
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
        'class'=>"form-control input-lg" ,
	'id'	=> 'password',
	'size'	=> 30,
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
        'class'=>"form-control input-lg" ,
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);

?>
<div class="container">

<div class="page-header">
    <h1>Welcome To Big Time Story <small>Login For some Awesome !!</small></h1>
</div>

<!-- Login with Social Buttons - START -->
<form action="<?=  base_url('auth/login')?>" class="col-md-6 col-md-offset-3" method="post">
    <div class="clearfix"></div>
        <div class="form-group">
		<?php echo form_label($login_label, $login['id']); ?>
		<?php echo form_input($login); ?>
            <span class="bg-danger"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></span>
	</div>
	<div class="form-group">
		<?php echo form_label('Password', $password['id']); ?>
		<?php echo form_password($password); ?>
            <span class="bg-danger"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></span>
	</div>

	<div class="form-inline">
			<?php echo form_checkbox($remember); ?>
			<?php echo form_label('Remember me', $remember['id']); ?>
	</div>
    <div class="form-group">
        <button class="btn btn-primary btn-lg btn-block">Sign In</button>
    </div>
    <div class="form-group">
        <a href="<?=  base_url('auth/register')?>"   class="btn btn-success btn-lg btn-block">Not a User</a>
    </div>
    <div class="form-group">
        <a href="<?=  base_url('auth/forgot_password')?>"   class="btn btn-warning btn-lg btn-block">Forgot Password</a>
    </div>
</form>
