<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
                'class'=>"form-control input-lg" ,
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
        'class'=>"form-control input-lg" ,
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
        'class'=>"form-control input-lg" ,
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
        'class'=>"form-control input-lg" ,

	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
);

?>
<div class="container">

<div class="page-header">
    <h1>Welcome To Big Time Story <small>Login For some Awesome !!</small></h1>
</div>

<!-- Login with Social Buttons - START -->
<form action="<?=  base_url('auth/register')?>" class="col-md-6 col-md-offset-3" method="post">
    <div class="clearfix"></div>
    <div class="form-group">
		<td><?php echo form_label('Email Address', $email['id']); ?></td>
		<td><?php echo form_input($email); ?></td>
                <span class="warning" style="color: red;"><?php echo form_error($email['name']); ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?></span>
    </div>
	<div class="form-group">
		<td><?php echo form_label('Password', $password['id']); ?></td>
		<td><?php echo form_password($password); ?></td>
		<td style="color: red;"><?php echo form_error($password['name']); ?></td>
	</div>
	<div class="form-group">
		<td><?php echo form_label('Confirm Password', $confirm_password['id']); ?></td>
		<td><?php echo form_password($confirm_password); ?></td>
		<td style="color: red;"><?php echo form_error($confirm_password['name']); ?></td>
	</div>

    <div class="clearfix"></div>
    <div class="form-group">
        <button class="btn btn-primary btn-lg btn-block">Sign Up</button>
    </div>
    
    <div class="form-group">
        <a href="<?=  base_url('auth/login')?>"   class="btn btn-success btn-lg btn-block">Not a User</a>
    </div>
</form>
