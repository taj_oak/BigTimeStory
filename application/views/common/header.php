<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Login with Social Buttons Template | PrepBootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>user/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>user/font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="<?php echo base_url(); ?>user/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>user/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>