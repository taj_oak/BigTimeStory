<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Form_validation extends CI_Form_validation {
    /**
     * MY_Form_validation::alpha_extra().
     * Alpha-numeric with periods, underscores, spaces and dashes.
     */
    function alpha_extra($email) {
        $this->CI->form_validation->set_message('alpha_extra', 'Please use your company email');
        $udomain = (explode('@', $email));
        $udomain = $udomain[1];
        $domains = array("aol.com", "gmail.com", "hotmail.com", "yahoo.com", "yahoo.co.uk", "email.com", "ymail.com","live.com", "msn.com");
        // add more domains which you want to block here 
        return (in_array($udomain, $domains)) ? FALSE : TRUE;
    }
    
}