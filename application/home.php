<!DOCTYPE html>
 <html lang="en">
 <head> 
 <base href=""> 
 <meta charset="UTF-8"> 
 <title>Toggl - Free Time Tracking Software</title> 
 <meta name="viewport" content="width=device-width,height=device-height,user-scalable=0,initial-scale=1,maximum-scale=1"> 
 <meta property="og:image" content="https://toggl.com/images/share-img/fb-share-img.jpg">
 <meta property="og:image:secure_url" content="https://toggl.com/images/share-img/fb-share-img.jpg">
 <meta name="robots" content="all"> <meta name="google-site-verification" content="214Nwhq57-16b88ylORRroEZsU5-JLSUGiLIN4Jyou4"> 
 <meta name="fragment" content="!"> <link href="fonts.googleapis.com/css.css" rel="stylesheet" type="text/css"> 
 <link rel="stylesheet" href="stylesheets/abdb94c9.main.css"> <script>window.prerenderReady = false;</script> 
 <link rel="canonical" href="toggl_default.html"> 
 <link rel="apple-touch-icon" sizes="57x57" href="images/share-img/apple-touch-icon-57x57.png"> 
 <link rel="apple-touch-icon" sizes="114x114" href="images/share-img/apple-touch-icon-114x114.png"> 
 <link rel="apple-touch-icon" sizes="72x72" href="images/share-img/apple-touch-icon-72x72.png"> 
 <link rel="apple-touch-icon" sizes="144x144" href="images/share-img/apple-touch-icon-144x144.png"> 
 <link rel="apple-touch-icon" sizes="60x60" href="images/share-img/apple-touch-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="120x120" href="images/share-img/apple-touch-icon-120x120.png"> 
 <link rel="apple-touch-icon" sizes="76x76" href="images/share-img/apple-touch-icon-76x76.png"> 
 <link rel="apple-touch-icon" sizes="152x152" href="images/share-img/apple-touch-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="images/share-img/apple-touch-icon-180x180.png">
 <meta name="apple-mobile-web-app-title" content="Toggl"> 
 <link rel="icon" type="image/png" href="images/share-img/favicon-192x192.png" sizes="192x192"> 
 <link rel="icon" type="image/png" href="images/share-img/favicon-160x160.png" sizes="160x160">
 <link rel="icon" type="image/png" href="images/share-img/favicon-96x96.png" sizes="96x96"> 
 <link rel="icon" type="image/png" href="images/share-img/favicon-16x16.png" sizes="16x16"> 
 <link rel="icon" type="image/png" href="images/share-img/favicon-32x32.png" sizes="32x32"> 
 <meta name="msapplication-config" content="browserconfig.xml"> <meta name="application-name" content="Toggl"> 
 <meta name="apple-itunes-app" content="app-id=885767775"> <meta name="google-play-app" content="app-id=com.toggl.timer">  
 <script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','//connect.facebook.net/en_US/fbevents.js');

  fbq('init', '1686732631542263');
  fbq('track', "PageView");</script> 
  <noscript><img height="1" width="1" style="display:none" src="www.facebook.com/tr.gif" /></noscript>  
  </head> 
  <body data-version="1.19.0"> <div class="application"> 
  </div> 
  <script src="javascripts/fda946a2.vendor.js"></script> 
  <script src="javascripts/458056ce.toggl.js"></script> 
<body data-version="1.19.0" id=""> <div class="application"><div class="page"><header>
  <div class="wrapper">
    <a href="/" class="logo" data-clicky-goal="Clicked navbar Toggl logo">Toggl</a>
    <button class="nav-opener nav-opener--red"></button>
    
        <nav class="main-nav main-nav--light">
      <ol>
        <li><a href="<?=  base_url()?>features" data-clicky-goal="Clicked navbar Features">Features</a></li>
        <li><a href="<?=  base_url()?>pricing" data-clicky-goal="Clicked navbar Pricing">Pricing</a></li>
        <li><a href="" data-clicky-goal="Clicked navbar Blog" target="_blank">Blog</a></li>
        <li><a href="<?=  base_url()?>business/" data-clicky-goal="Clicked navbar Business">Business</a></li>
        <li class="logged" style="display:none"><a href="#" class="js-logout-button" data-clicky-goal="Clicked navbar Log out">Log out</a></li>
        <li class="logged" style="display:none"><a href="/app/" clickthrough="" class="cta-button cta-button--small cta-button--inline cta-button--filled" data-clicky-goal="Clicked navbar Go to timer">Go to timer</a></li>

        <li class="not-logged" style=""><a href="<?=  base_url('auth/login')?>" class="js-login-button" data-clicky-goal="Clicked navbar Log in">Log in</a></li>
        <li class="not-logged" style=""><a href="<?=  base_url('auth/register')?>" class="cta-button cta-button--small cta-button--inline cta-button--filled cta-button--no-arrow js-ripple-button" data-clicky-goal="Clicked navbar Sign up">Sign up</a></li>

      </ol>
      <ol class="secondary">
          <li><a href="<?=  base_url()?>/pricing">Pricing</a></li>
        <li><a href="<?=  base_url()?>/about">About</a></li>
        <li><a href="#" target="_blank">Jobs</a></li>
        <li><a href="<?=  base_url()?>/legal/terms">Terms</a></li>
        <li><a href="<?=  base_url()?>/legal/privacy">Privacy</a></li>
        <li><a href="<?=  base_url()?>/features#download">Download</a></li>
        <li><a href="https://github.com/toggl/toggl_api_docs/blob/master/toggl_api.md" target="_blank">API</a></li>
        <li><a href="http://blog.toggl.com/media-kit/" target="_blank">Media</a></li>
        <li><a href="<?=  base_url()?>/tools">Tools</a></li>
        <li><a href="#" target="_blank">Support</a></li>
      </ol>
          </nav>
  </div>
</header>
<section class="hero hero--front">
  <div class="hero__background">
    <section class="hero__manual-video js-manual-video" style="display:none;"></section>
    <div class="video js-automatic-video" style="transform: scale(1.13341) translateY(-50%);">
      <div class="wrapper"><iframe src="https://player.vimeo.com/video/120347656?api=1&amp;autoplay=0&amp;loop=1&amp;color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" webkitallowfullscreenmozallowfullscreenallowfullscreen="" __idm_id__="14337"></iframe></div>
    </div>
  </div>
  <div class="hero__content content--light hero__content--centered">
    <section class="js-manual-video" style="display:none;">
      <h1 class="hero-timer-heading hero-timer-heading__manual">Market Leader in Time Tracking</h1>
      <h3 class="manual-time-start">
        <div class="seen-wrapper">
          <a href="#" class="video-force-start">See the video
            <div class="seen-btn">
              <svg xmlns="http://www.w3.org/2000/svg" fill="white" width="60" height="60" viewBox="0 0 24 24">
                <path d="M8 5v14l11-7z"></path>
                <path d="M0 0h24v24H0z" fill="none"></path>
              </svg>
            </div>
          </a>
        </div>
      </h3>
    </section>
    <section class="js-automatic-video">
      <h1 class="hero-timer-heading dynamic" data-default-text="Toggl anything">Track</h1>
      <h1 class="hero-timer-heading dynamic">running late</h1>
      <span class="hero-timer" style="opacity: 1; display: block;">
        <span class="seconds">1</span>.<span class="milliseconds">72</span>
      </span>
    </section>

  </div>

  <a href="/app" class="logged cta-button cta-button--filled js-redirect-to-app js-ripple-button" data-clicky-goal="Clicked bottom 'Start tracking' on the homepage while logged-in - goes to /app" style="display: none;">Start tracking</a>
  <a href="/signup" class="not-logged cta-button cta-button--filled js-ripple-button" data-clicky-goal="Clicked bottom 'Start tracking' on the homepage as a new user - goes to /signup" style="">Start tracking</a>

  <div class="js-play-pause-controls" style="">
    <div class="video-pause-button" style="">
      <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 24 24" fill="white">
        <path d="M0 0h24v24H0z" fill="none"></path>
        <path d="M9 16h2V8H9v8zm3-14C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm1-4h2V8h-2v8z"></path>
      </svg>
    </div>
    <div class="video-play-button" style="display: none;">
      <svg xmlns="http://www.w3.org/2000/svg" fill="white" width="36" height="36" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="none"></path>
        <path d="M10 16.5l6-4.5-6-4.5v9zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"></path>
      </svg>
    </div>
  </div>
  <div class="video-mute-button js-automatic-video">
    <svg height="32" version="1.1" width="32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
      <defs></defs>
      <g class="audio-waves">
        <path class="first" fill="white" d="m 38,23 -3,3 c 2,2 3,4 3,6 0,2 -1,5 -3,6 l 3,3 c 2,-2 4,-6 4,-9 0,-4 -1,-7 -4,-9 z"></path>
        <path class="second" fill="white" d="m 50,32 c 0,-6 -2,-11 -6,-15 L 41,20 c 3,3 5,8 5,12 0,5 -2,9 -5,12 l 3,3 C 48,43 50,38 50,32 z"></path>
        <path class="third" fill="white" d="m 59,32 c 0,-8 -3,-16 -9,-22 l -3,3 c 5,5 8,11 8,18 0,7 -3,14 -8,18 l 3,3 c 6,-5 9,-13 9,-22 z"></path>
      </g>
      <polygon fill="white" points="12,40 26,52 26,10 12,22 0,22 0,40"></polygon>
    </svg>
  </div>
</section>
<section class="content-section content-section--centered content-section--centered--padded-more">
  <div class="timer cf">
    <div class="timer__task">Reading pyxists.com</div>
    <button class="timer__button timer__button--stop js-ripple-button">Stop</button>
    <div class="timer__time">16:28 min</div>
  </div>
  <h2>The ultimate timer. It's insanely simple.</h2>
  <p>
    Toggl’s time tracker is built for speed and ease of use. Time keeping with Toggl is so simple that you’ll actually use it. Toggl drives a stake in the heart of timesheets.
  </p>
  <a href="/signup" class="cta-button cta-button--empty js-ripple-button" data-clicky-goal="Clicked bottommost 'Get Started' on the homepage as a new user">Get Started</a>
</section>
<section class="content-section">
  <section class="content-section__centered-block">
    <img src="images/pyxists.png" alt="pyxists logo icon">
    <h2>1,256,842 PRODUCTIVE PYXISLERS</h2>
    <p>Our leading time tracking software improves workplace productivity.</p>
  </section>
  <blockquote class="content-section__background-tile" style="background-image: url('photos/makeable.jpg?1');">
    <div class="content-section__background-tile-content">
      One break per hour recharges willpower!
      <footer>Emily, Pyxisler from Makeable</footer>
    </div>
  </blockquote>
  <blockquote class="content-section__background-tile" style="background-image: url('photos/webhomes.jpg?1');">
    <div class="content-section__background-tile-content">
      Do one thing at a time.
      <footer>Raine, Pyxisler from Station</footer>
    </div>
  </blockquote>
  <blockquote class="content-section__background-tile" style="background-image: url('photos/offerpop.jpg?1');">
    <div class="content-section__background-tile-content">
      Define priorities, assign focus.
      <footer>Melissa, Pyxisler from Offerpop</footer>
    </div>
  </blockquote>
  <blockquote class="content-section__background-tile" style="background-image: url('photos/matt_alexander.jpg');">
    <div class="content-section__background-tile-content">
      Measure it, improve it.
      <footer>Matt, Pyxisler from Chicago</footer>
    </div>
  </blockquote>
</section>
<footer class="cf">
  <div class="wrapper">
    <a href="" class="logo">
   <svg width="80" height="23.999999999999996" xmlns="http://www.w3.org/2000/svg">
 <!-- Created with SVG-edit - http://svg-edit.googlecode.com/ -->
 <g>
  <title>Layer 1</title>
  <text xml:space="preserve" text-anchor="middle" font-family="Monospace" font-size="134" id="svg_9" y="-348.333328" x="51.215048" stroke-width="0" stroke="#000000" fill="#779fc6">pyxists</text>
  <text font-style="normal" font-weight="bold" transform="matrix(0.17172336749175057,0,0,0.1403048631123741,242.1869119697687,59.811965487407136) " xml:space="preserve" text-anchor="middle" font-family="Cursive" font-size="134" id="svg_10" y="-304.902647" x="-1177.3845" stroke-linecap="null" stroke-linejoin="null" stroke-width="0" stroke="#000000" fill="#7f7f7f">Pyxists</text>
 </g>
</svg>
    </a>
    <nav class="footer-nav">
      <ol>
        <li><a href="/pricing">Pricing</a></li>
        <li><a href="/about">About</a></li>
        <li><a href="http://jobs.toggl.com/" target="_blank">Jobs</a></li>
        <li><a href="/legal/terms">Terms</a></li>
        <li><a href="/legal/privacy">Privacy</a></li>
        <li><a href="/features#download">Download</a></li>
        <li><a href="https://github.com/toggl/toggl_api_docs/blob/master/toggl_api.md" target="_blank">API</a></li>
        <li><a href="http://blog.toggl.com/media-kit/" target="_blank">Media</a></li>
        <li><a href="/tools">Tools</a></li>
        <li><a href="http://support.toggl.com/" target="_blank">Support</a></li>
      </ol>
    </nav>
    <nav class="social">
      <ol>
        <li><a href="https://www.facebook.com/Toggl" target="_blank">
          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ev="http://www.w3.org/2001/xml-events" height="18px" width="11px">
          <path fill="currentColor" d="M3,10 C3,10 3,18 3,18 C3,18 7,18 7,18 C7,18 7,10 7,10 C7,10 10,10 10,10 C10,10 10,7 10,7 C10,7 7,7 7,7 C7,7 7,5 7,4 C7,4 7,3 8,3 C9,3 10,3 10,3 C10,3 10,0 10,0 C10,0 8,0 6,0 C5,0 3,2 3,3 C3,5 3,7 3,7 C3,7 1,7 1,7 C1,7 1,10 1,10 C1,10 3,10 3,10 Z "></path>
          </svg>
        </a></li>
        <li><a href="https://twitter.com/pyxists" target="_blank">
          <svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:ev="http://www.w3.org/2001/xml-events" height="16px" width="18px">
          <path fill="currentColor" d="M18,2 C17,3 16,3 16,3 C16,3 17,2 17,1 C17,1 16,2 15,2 C14,1 13,1 12,1 C10,1 9,2 9,4 C9,5 9,5 9,5 C6,5 3,4 1,1 C1,2 1,3 1,3 C1,5 1,6 2,6 C2,6 1,6 1,6 C1,6 1,6 1,6 C1,8 2,9 4,9 C3,10 3,10 3,10 C2,10 2,10 2,10 C3,11 4,12 5,12 C4,13 3,14 1,14 C1,14 0,14 0,14 C2,15 4,15 6,15 C12,15 16,10 16,5 C16,5 16,4 16,4 C17,4 17,3 18,2 Z "></path>
          </svg>
        </a></li>
        <li><a href="https://plus.google.com/+toggl" rel="publisher" target="_blank">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="2 0 14 15" enable-background="new 0 0 14 15" xml:space="preserve" height="25px" width="25px" style="margin-top:3px; padding: 0">
          <g>
            <path fill="currentColor" d="M6.6,6.7c0,0.2,0.1,0.3,0.2,0.5c0.1,0.2,0.3,0.3,0.5,0.5s0.4,0.3,0.6,0.5c0.2,0.2,0.4,0.4,0.5,0.7
                c0.1,0.3,0.2,0.6,0.2,1c0,0.4-0.1,0.8-0.3,1.2c-0.3,0.5-0.8,0.9-1.4,1.2c-0.6,0.3-1.3,0.4-2,0.4c-0.6,0-1.1-0.1-1.7-0.3
                c-0.5-0.2-0.9-0.5-1.1-0.9C2,11,1.9,10.7,1.9,10.4c0-0.4,0.1-0.7,0.3-1S2.6,8.8,3,8.6C3.6,8.3,4.5,8,5.7,8C5.5,7.8,5.4,7.6,5.4,7.5
                C5.3,7.3,5.3,7.2,5.3,7c0-0.2,0-0.4,0.1-0.6c-0.2,0-0.4,0-0.5,0c-0.7,0-1.2-0.2-1.7-0.6c-0.5-0.4-0.7-1-0.7-1.6
                c0-0.4,0.1-0.7,0.2-1.1s0.4-0.6,0.7-0.9c0.3-0.3,0.8-0.5,1.2-0.7c0.5-0.1,1-0.2,1.5-0.2H9L8,1.9H7.2c0.3,0.3,0.6,0.6,0.8,0.9
                s0.3,0.7,0.3,1.1c0,0.3-0.1,0.6-0.2,0.9S7.8,5.2,7.6,5.4C7.5,5.5,7.3,5.7,7.1,5.8S6.9,6.1,6.7,6.2C6.6,6.4,6.6,6.5,6.6,6.7z
                 M5.6,11.9c0.3,0,0.5,0,0.7-0.1s0.5-0.1,0.7-0.3c0.2-0.1,0.4-0.3,0.5-0.5c0.1-0.2,0.2-0.5,0.2-0.7c0-0.1,0-0.2,0-0.3
                c0-0.1-0.1-0.2-0.1-0.3c0-0.1-0.1-0.2-0.2-0.3S7.2,9.2,7.2,9.2C7.1,9.1,7,9.1,6.9,9C6.8,8.9,6.7,8.8,6.7,8.8S6.5,8.7,6.4,8.6
                C6.2,8.5,6.2,8.4,6.2,8.4c-0.1,0-0.2,0-0.3,0c-0.2,0-0.5,0-0.7,0c-0.2,0-0.5,0.1-0.7,0.2C4.2,8.7,3.9,8.8,3.8,8.9
                C3.6,9,3.4,9.2,3.3,9.4s-0.2,0.4-0.2,0.7c0,0.3,0.1,0.6,0.2,0.8s0.4,0.4,0.6,0.6c0.3,0.1,0.5,0.2,0.8,0.3C5,11.8,5.3,11.9,5.6,11.9
                z M5.6,6c0.2,0,0.3,0,0.5-0.1c0.2-0.1,0.3-0.2,0.4-0.3C6.8,5.4,6.9,5,6.9,4.6c0-0.3,0-0.5-0.1-0.8C6.7,3.4,6.6,3.1,6.5,2.9
                C6.3,2.6,6.2,2.3,5.9,2.2C5.7,2,5.4,1.9,5.1,1.9c-0.2,0-0.4,0-0.6,0.1C4.4,2.1,4.3,2.2,4.1,2.4C3.9,2.6,3.8,3,3.8,3.4
                c0,0.2,0,0.4,0.1,0.7c0,0.2,0.1,0.5,0.2,0.7C4.2,5,4.3,5.2,4.5,5.4C4.6,5.6,4.8,5.7,5,5.9C5.2,6,5.4,6,5.6,6z M11,6h1.4v0.7H11v1.5
                h-0.7V6.7H8.9V6h1.4V4.6H11V6z"></path>
          </g>
          </svg>
        </a></li>
      </ol>
    </nav>
  </div>
</footer>
</div></div> <script src="javascripts/fda946a2.vendor.js"></script> <script src="javascripts/458056ce.toggl.js"></script> <script type="application/ld+json">{
     "@context": "http://schema.org",
     "@type": "Organization",
     "name": "Toggl",
     "url": "https://toggl.com",
     "logo": "https://toggl.com/images/logo.png",
     "sameAs": [
       "https://www.facebook.com/Toggl",
       "https://twitter.com/toggl",
       "https://plus.google.com/+toggl",
       "http://www.linkedin.com/company/toggl",
       "https://www.youtube.com/user/toggltimer",
       "https://instagram.com/togglteam/"
     ]
   }</script>  </body>
   
   </body> 
   </html>