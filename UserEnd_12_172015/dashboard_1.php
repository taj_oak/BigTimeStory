<?php
session_start();
$_SESSION = (unserialize(base64_decode($_REQUEST['token'])));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
    <title>My Calendar </title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link href="css/dailog.css" rel="stylesheet" type="text/css" />
    <link href="css/calendar.css" rel="stylesheet" type="text/css" /> 
    <link href="css/dp.css" rel="stylesheet" type="text/css" />   
    <link href="css/alert.css" rel="stylesheet" type="text/css" /> 
    <link href="css/main.css" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"></link>
    <script src="src/jquery.js" type="text/javascript"></script>  
    
    <script src="src/Plugins/Common.js" type="text/javascript"></script>    
    <script src="src/Plugins/datepicker_lang_US.js" type="text/javascript"></script>     
    <script src="src/Plugins/jquery.datepicker.js" type="text/javascript"></script>

    <script src="src/Plugins/jquery.alert.js" type="text/javascript"></script>    
    <script src="src/Plugins/jquery.ifrmdailog.js" defer="defer" type="text/javascript"></script>
    <script src="src/Plugins/wdCalendar_lang_US.js" type="text/javascript"></script>    
    <script src="src/Plugins/jquery.calendar.js" type="text/javascript"></script>   

<script type="text/javascript">
$(document).ready(function(){    
    //Check if the current URL contains '#'
    if(document.URL.indexOf("#")==-1){
        // Set the URL to whatever it was plus "#".
        url = document.URL+"#";
        location = "#";

        //Reload the page
        location.reload(true);
    }
});
</script>
    <script type="text/javascript">

       

        $(document).ready(function() {
        	//setProjectId(-11 , -11);
        	//getProjectList();
           var view="week";          
          
            var DATA_FEED_URL = "php/datafeed.php";
            var op = {
                view: view,
                theme:3,
                showday: new Date(),
                EditCmdhandler:Edit,
                DeleteCmdhandler:Delete,
                ViewCmdhandler:View,    
                onWeekOrMonthToDay:wtd,
                onBeforeRequestData: cal_beforerequest,
                onAfterRequestData: cal_afterrequest,
                onRequestDataError: cal_onerror, 
                autoload:true,
                url: DATA_FEED_URL + "?method=list",  
                quickAddUrl: DATA_FEED_URL + "?method=add", 
                quickUpdateUrl: DATA_FEED_URL + "?method=update",
                quickDeleteUrl: DATA_FEED_URL + "?method=remove"        
            };
            var $dv = $("#calhead");
            var _MH = document.documentElement.clientHeight;
            var dvH = $dv.height() + 2;
            op.height = _MH - dvH;
            op.eventItems =[];
            var p = $("#gridcontainer").bcalendar(op).BcalGetOp();
            if (p && p.datestrshow) {
            	getTimeSheetDetails(p.vstart , p.vend);
                $("#txtdatetimeshow").text(p.datestrshow);
            }
            $("#caltoolbar").noSelect();
            
            $("#hdtxtshow").datepicker({ picker: "#txtdatetimeshow", showtarget: $("#txtdatetimeshow"),
            onReturn:function(r){                          
                            var p = $("#gridcontainer").gotoDate(r).BcalGetOp();
                            if (p && p.datestrshow) {
                                $("#txtdatetimeshow").text(p.datestrshow);
                            }
                     } 
            });
            function cal_beforerequest(type)
            {
                var t="Loading data...";
                switch(type)
                {
                    case 1:
                        t="Loading data...";
                        break;
                    case 2:                      
                    case 3:  
                    case 4:    
                        t="The request is being processed ...";                                   
                        break;
                }
                $("#errorpannel").hide();
                $("#loadingpannel").html(t).show();    
            }
            function cal_afterrequest(type)
            {
                switch(type)
                {
                    case 1:
                        $("#loadingpannel").hide();
                        break;
                    case 2:
                    case 3:
                    case 4:
                        $("#loadingpannel").html("Success!");
                        window.setTimeout(function(){ $("#loadingpannel").hide();},2000);
                    break;
                }              
               
            }
            function cal_onerror(type,data)
            {
                $("#errorpannel").show();
            }
            function Edit(data)
            {
               var eurl="edit.php?id={0}&start={2}&end={3}&isallday={4}&title={1}";   
                if(data)
                {
                    var url = StrFormat(eurl,data);
                    OpenModelWindow(url,{ width: 600, height: 400, caption:"Manage  The Task",onclose:function(){
                       $("#gridcontainer").reload();
                    }});
                }
            }    
            function View(data)
            {
                var str = "";
                $.each(data, function(i, item){
                    str += "[" + i + "]: " + item + "\n";
                });
               // alert(str);               
            }    
            function Delete(data,callback)
            {           
                
                $.alerts.okButton="Ok";  
                $.alerts.cancelButton="Cancel";  
                hiConfirm("Are You Sure to Delete this Task", 'Confirm',function(r){ r && callback(0);});           
            }
            function wtd(p)
            {
               if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $("#showdaybtn").addClass("fcurrent");
            }
            //to show day view
            $("#showdaybtn").click(function(e) {
                //document.location.href="#day";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                $("#showTotalHours").css({"float" : "right"}).empty();
                var p = $("#gridcontainer").swtichView("day").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            //to show week view
            $("#showweekbtn").click(function(e) {
                //document.location.href="#week";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                var p = $("#gridcontainer").swtichView("week").BcalGetOp();
                if (p && p.datestrshow) {
                	getTimeSheetDetails(p.vstart , p.vend);
                	//showTotalHours(p.eventItems)
                    $("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            //to show month view
            $("#showmonthbtn").click(function(e) {
                //document.location.href="#month";
                $("#caltoolbar div.fcurrent").each(function() {
                    $(this).removeClass("fcurrent");
                })
                $(this).addClass("fcurrent");
                $("#showTotalHours").css({"float" : "right"}).empty();
                var p = $("#gridcontainer").swtichView("month").BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
            
            $("#showreflashbtn").click(function(e){
                $("#gridcontainer").reload();
            });
            
            //Add a new event
            $("#faddbtn").click(function(e) {
                var url ="edit.php";
                OpenModelWindow(url,{ width: 500, height: 400, caption: "Create New Task"});
            });
            //go to today
            $("#showtodaybtn").click(function(e) {
            	$("#showTotalHours").css({"float" : "right"}).empty();
                var p = $("#gridcontainer").gotoDate().BcalGetOp();
                if (p && p.datestrshow) {
                    $("#txtdatetimeshow").text(p.datestrshow);
                }


            });
            //previous date range
            $("#sfprevbtn").click(function(e) {
            	$("#showTotalHours").css({"float" : "right"}).empty();
                var p = $("#gridcontainer").previousRange().BcalGetOp();
                if (p && p.datestrshow) {
                	//showTotalHours(p.eventItems);
                    $("#txtdatetimeshow").text(p.datestrshow);
                }

            });
            //next date range
            $("#sfnextbtn").click(function(e) {
            	$("#showTotalHours").css({"float" : "right"}).empty();
                var p = $("#gridcontainer").nextRange().BcalGetOp();
                if (p && p.datestrshow) {
                	//showTotalHours(p.eventItems);
                    $("#txtdatetimeshow").text(p.datestrshow);
                }
            });
           
        });

function convert(str) {
    	var date = new Date(str),
    	mnth = ("0" + (date.getMonth()+1)).slice(-2),
    	day  = ("0" + date.getDate()).slice(-2);
    	return [ date.getFullYear(), mnth, day ].join("-");
}
function getTimeSheetDetails(startTime , endTime){
//	alert(startTime+"  ed: "+endTime);
	

	var sTime =  convert(startTime);
	var eTime =  convert(endTime);
	//alert(sTime+"  ed: "+eTime);
		$.ajax({
			  method: "POST",
			  url: "php/datafeed.php?method=getTimeSheetDetails",
			  asyn:true,
			  cache:true,
			  data: 'startTime='+sTime+'&endTime='+eTime,
			  dataType: "html",
			  success: function(data){
					if(data != null){
						var eventItems = JSON.parse(data);
						showTotalHours(eventItems["events"])
					}
				}
			});
}
function showTotalHours(eventItems){
	console.log(eventItems.length);
	$("#showTotalHours").css({"float" : "right"}).empty();
	$span = $("<span />");
	$button =$("<input />");
	var Total = null;
	var totalHours = "00:00:00";
	var ids = null;
	if(eventItems !=  null && eventItems.length > 0){
		for(var $j = 0 ; $j < eventItems.length; $j++){
			var eventArr = eventItems[$j];
			if(eventArr != null && eventArr.length > 0){
				var end_time = eventArr[3];
				var start_time = eventArr[2];
				var diff =  Math.abs(new Date(end_time) - new Date(start_time));
				if(Total  == null && $j == 0){
					Total = diff;
					if(eventArr[0] != null)
					ids = eventArr[0];
				}else{
					Total = Total + diff;
					if(eventArr[0] != null)
					ids = ids +","+eventArr[0];
				}
				diff = null;end_start_time = null;end_time=null;
			}
		}
		var seconds = Math.floor(Total/1000); //ignore any left over units smaller than a second
		var minutes = Math.floor(seconds/60); 
		seconds = seconds % 60;
		var hours = Math.floor(minutes/60);
		minutes = minutes % 60;
		totalHours = hours + ":" + minutes + ":" + seconds;
		$span.text("Total hours work :"+totalHours+"   ");
	}else{
		$span.text("Total hours work :"+totalHours+"   ");
	}
	$button.attr({"type" : "button" , "id" : "approvedId" , "value" : "Submit" , "totalHours" :  totalHours , "timeSheetIds" : ids});
	$button.bind("click" , function(){
			var _totalHrs = $(this).attr("totalHours");
			var _timeSheetIds = $(this).attr("timeSheetIds");
			approvedTimeSheet(_totalHrs , _timeSheetIds);
	});
	$("#showTotalHours").append($span);	
	$("#showTotalHours").append($button);
	checkStatus(ids);
}
function checkStatus(_timeSheetIds){
	if(_timeSheetIds != null && typeof _timeSheetIds != 'undefined' && _timeSheetIds != ""){
	$.ajax({
		  method: "POST",
		  url: "php/datafeed.php?method=checkApproval",
		  asyn:true,
		  cache:true,
		  data: 'timeSheetIds='+_timeSheetIds,
		  dataType: "html",
		  success: function(data){
				if(data != null){
					var text = JSON.parse(data);
					if(text["Msg"] == "Requested"){
						$("#approvedId").attr({"value" : "Pending","disabled" : true});
					}else if(text["Msg"] == "Approved"){
						$("#approvedId").attr({"value" : "Approved","disabled" : true});
					}else if(text["Msg"] == "Rejected"){
						$("#approvedId").attr({"value" : "Rejected","disabled" : true});
					}			    	
				}
			}
		});
	}
}
function approvedTimeSheet(_totalHrs , _timeSheetIds){
	if(_timeSheetIds != null && typeof _timeSheetIds != 'undefined' && _timeSheetIds != ""){
	$.ajax({
		  method: "POST",
		  url: "php/datafeed.php?method=approved",
		  asyn:true,
		  cache:true,
		  data: 'timeSheetIds='+_timeSheetIds,
		  dataType: "html",
		  success: function(data){
				if(data != null){
			    	//alert( "Time Sheet Approval Submitted Successfully"+data);
			    	$("#approvedId").attr({"value" : "Pending","disabled" : true});
				}
			}
		});
	}else{
		alert("There is no Task to approved..,Please add task to approved");
	}
}
function setProjectId(projectId , categoryId){
	$.ajax({
		  method: "GET",
		  url: "php/datafeed.php?method=setProjectId",
		  asyn: false,
		  cache:true,
		  data : 'projectId='+projectId+'&categoryId='+categoryId,
		  success: function(data){
				if(data != null){
					
				}
			}
		});
}
//Geting the project list based on user id
function getProjectList(){
  var $li = $("<li />").text("Filter your search").addClass("sb_filter");
  	$("#liList").empty().append($li);
		$.ajax({
			  method: "GET",
			  url: "php/datafeed.php?method=getProjectList",
			  asyn:true,
			  cache:true,
			  success: function(data){debugger;
					if(data != null){
						var object = JSON.parse(data);
						var projectList = object.project;
						for(var pl = 0 ; pl < projectList.length;pl++){
							var project = projectList[pl];
							var $label= $("<label for="+project[1]+"><strong>"+project[1]+"</strong></label>");
							$li = $("<li />").attr({"projectId" : project[0]}).append($label);
							$li.bind("click" , function(){
							  	  var pId = $(this).attr("projectId");
							  	loadCalenderView(pId,0);
							  });
													
							$("#liList").append($li);
						}
						//var id = $("#project").val();
	    				//loadCategoryView(id);
					}
				}
			});
		
}
function loadCategoryView(projectId){
	var $select = $("<select />");
	var $option = $("<option />");
	$select.attr({"id" : "category" , "name" : "category" , "projectId" : projectId});
	$select.bind("change" , function(){
	  	  var cId = $(this).val();
	  	  var pId = $(this).attr("projectId");
	  	loadCalenderView(pId , cId);
	  });
	$("#categoryDropDown").empty().append($select);
    if(projectId != null && projectId != undefined && projectId > 0){
    	$.ajax({
				  method: "GET",
				  url: "php/datafeed.php?method=getProjectCategoryList",
				  asyn:true,
				  data : 'projectId='+projectId,
				  cache:true,
				  success: function(data){debugger;
						if(data != null){
							var object = JSON.parse(data);
							var categoryList = object.category;
							if(categoryList != null && categoryList != undefined && categoryList.length > 0){
								for(var pl = 0 ; pl < categoryList.length;pl++){
	  								var category = categoryList[pl];
	  								$option = $("<option />").attr({"value" : category[0]}).text(category[1]);
	  								$select.append($option);
  								}
							}else{
								$option = $("<option />").attr({"value" : "-11"}).text("---No Category Available---");
								$select.append($option);
							}
							var pid = $("#project").val();
							var cid = $("#category").val();
							loadCalenderView(pid , cid);
						}
					}
				});
    }else{
    	$option = $("<option />").attr({"value" : "-11"}).text("---No Category Available---");
		$select.append($option);
    }  	
}
function loadCalenderView(selectedId , categoryId){
	//alert(selectedId);
	setProjectId(selectedId , categoryId);
	var viewSelection = "week";
	var eventSelected = $("#caltoolbar div.fcurrent").find("span").text();
	if(eventSelected == "Day"){
		viewSelection = "day";
	}else if(eventSelected == "Month"){
		viewSelection = "month";
	}
    var p = $("#gridcontainer").swtichView(viewSelection).BcalGetOp();
    $("#showTotalHours").css({"float" : "right"}).empty();
    if (p && p.datestrshow) {
        if(viewSelection == "week"){
    		getTimeSheetDetails(p.vstart , p.vend);
        }
        $("#txtdatetimeshow").text(p.datestrshow);
    }
    $("#gridcontainer").reload();
}
    </script>  
  
</head>
<body>
    <div>

      <div id="calhead" style="padding-left:1px;padding-right:1px;">                   
            <div class="cHead"><div class="ftitle">Time Sheet</div>
            <div id="loadingpannel" class="ptogtitle loadicon" style="display: none;">Loading data...</div>
             <div id="errorpannel" class="ptogtitle loaderror" style="display: none;">Sorry, could not load your data, please try again later</div>
            </div>          
            <div id="caltoolbar" class="ctoolbar">
              	<div id="faddbtn" class="fbutton">
                	<div>
                			<span title='Click to Create New Event' class="addcal">
                				New Task                
                			</span>
                	</div>
            	</div>
            	<div class="btnseparator"></div>
             	<div id="showtodaybtn" class="fbutton">
                		<div>
                			<span title='Click to back to today ' class="showtoday">
                				Today
                			</span>
                		</div>
            	</div>
              	<div class="btnseparator"></div>
            	<div id="showdaybtn" class="fbutton">
                		<div>
                				<span title='Day' class="showdayview">Day</span>
                		</div>
            	</div>
              	<div  id="showweekbtn" class="fbutton fcurrent">
                		<div>
                				<span title='Week' class="showweekview">Week</span>
                		</div>
            	</div>
              	<div  id="showmonthbtn" class="fbutton">
                		<div>
                				<span title='Month' class="showmonthview">Month</span>
                		</div>
            	</div>
            	<div class="btnseparator"></div>
              	<div  id="showreflashbtn" class="fbutton">
                		<div>
                				<span title='Refresh view' class="showdayflash">Refresh</span>
                		</div>
                </div>
             	<div class="btnseparator"></div>
            	<div id="sfprevbtn" title="Prev"  class="fbutton">
              			<span class="fprev"></span>
            	</div>
            	<div id="sfnextbtn" title="Next" class="fbutton">
                		<span class="fnext"></span>
            	</div>
            	<div class="fshowdatep fbutton">
                		<div>
                        		<input type="hidden" name="txtshow" id="hdtxtshow" />
                        		<span id="txtdatetimeshow">Loading</span>
                    </div>
            	</div>
            	
            	
            	<div id="showTotalHours"></div>
            	<div class="clear"></div>
            </div>
      </div>
      <div style="padding:1px;">
      		<div class="t1 chromeColor">&nbsp;</div>
        	<div class="t2 chromeColor">&nbsp;</div>
        	<div id="dvCalMain" class="calmain printborder">
            		<div id="gridcontainer" style="overflow-y: visible;"></div>
        	</div>
        	<div class="t2 chromeColor">&nbsp;</div>
        	<div class="t1 chromeColor">&nbsp;</div>   
      </div>
  </div> 
</body>
</html>