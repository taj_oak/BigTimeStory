<?php
function js2PhpTime($jsdate){
  if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
    $ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
    //echo $matches[4] ."-". $matches[5] ."-". 0  ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
  }else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
    $ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
    //echo 0 ."-". 0 ."-". 0 ."-". $matches[1] ."-". $matches[2] ."-". $matches[3];
  }
  return $ret;
}

function php2JsTime($phpDate){
    //echo $phpDate;
    //return "/Date(" . $phpDate*1000 . ")/";
    return date("m/d/Y H:i", $phpDate);
}

function php2MySqlTime($phpDate){
    return date("Y-m-d H:i:s", $phpDate);
}

function mySql2PhpTime($sqlDate){
    $arr = date_parse($sqlDate);
    return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);

}
function gethoursManagerProject($timesheetIds) {
 $userId=$_SESSION['user_id'];
    $ids= implode(',', $timesheetIds);
   // $status = checkApprovalStatusTimesheet($ids);
    $db = new DBConnection();
    $db->getConnection();
    $sql = "select *, tp.project_name , te.emp_name from `tms_time_sheet` as tts Join tms_employee as te on te.user_id = tts.manager_id JOIN tms_project as tp on tp.project_id = tts.project_id where time_sheet_id in($ids)"
                        ." and tts.`user_id` = ".$userId." GROUP BY tts.project_id  ";
	    			//." and `manager_id` = ".$managerId." and `user_id` = ".$userId;
                                $handle = mysql_query($sql);
                                while ($row = mysql_fetch_object($handle)){
			        $ret['timesheet'][] = $row;
                                }
    return $ret;
}
function checkApprovalStatusTimesheet($timeSheetId){
	$ret = array();
	try{
		$db = new DBConnection();
		$db->getConnection();
		$submittedBy = $_SESSION['user_id'];//Take User From Session
		$sql = "select status from `tms_time_sheet_approval` where `time_sheet_id` like '%"
				.mysql_real_escape_string($timeSheetId)
				."%' and `submitted_by` =". $submittedBy;
		$ret['sql']= $sql;
		$handle = mysql_query($sql);
		if($row = mysql_fetch_object($handle)){
				
                    return $row->status;
		}else{
                    return FALSE;
		}
	}catch(Exception $e){
		$ret['IsSuccess'] = false;
		$ret['Msg'] = $e->getMessage();
	}
	return $ret;
}



?>