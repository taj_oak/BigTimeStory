<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Sk Mamtajuddin
 */
class User extends CI_Controller {
    
       function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->helper("url");
        if (!isset($_SESSION['role']) || $_SESSION['role']!='user')
        {
          redirect(main_url());
        }
    }
    
     public function index() {
         if(!isset($_SESSION['project_id'])){
             $_SESSION['project_id'] = -11;
         }
        if(!isset($_SESSION['category_id'])){
           $_SESSION['category_id'] = -11;
         }
	$_SESSION['manager_id']='';
       // print_r($_SESSION);
       $data['page']='pages/dashboard';
        $this->load->view('main', $data);
    }
    
   
    
    
}

