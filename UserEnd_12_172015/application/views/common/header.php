<head>
<link rel="stylesheet" type="text/css"
  href="http://getbootstrap.com/dist/css/bootstrap.css">
<link rel="stylesheet" type="text/css"
  href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" 
        rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
</head>
<body class="hold-transition skin-blue layout-top-nav">
    <div class="wrapper">

       <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
                <a href="<?= base_url() ?>" class="navbar-brand"><b><?= $_SESSION['username'] ?></b></a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
              <ul class="nav navbar-nav">
                  <li class="active"><a href="<?= base_url() ?>"><i title="Time Sheet" class="fa fa-clock-o"></i></a></li>
                <li><a href="<?= base_url('summaryReport_ctrl'); ?>"><i title="Report" class="fa fa-file-excel-o "></i></a></li>
                 <li><a href="#"><i title="Expenses" class="fa fa-dollar"></i> </a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Vacation <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                    <li class="divider"></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>
                 <form class="navbar-form navbar-left" role="search">
			        <div class="form-group">
			          <div class="input-group">
			            <input type="text" class="form-control" placeholder="Search"
			              id="inputGroup" /> <span class="input-group-addon" onclick="javascript:clickSeach();"> <i
			              class="fa fa-search"></i>
			            </span>
			          </div>
			      </div>
              	</form>
               
              </ul>
            </div><!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- Messages: style can be found in dropdown.less-->
                  <li class="dropdown messages-menu">
                    <!-- Menu toggle button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-envelope-o"></i>
                      <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">You have 4 messages</li>
                      <li>
                        <!-- inner menu: contains the messages -->
                        <ul class="menu">
                          <li><!-- start message -->
                            <a href="#">
                              <div class="pull-left">
                                <!-- User Image -->
                                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                              </div>
                              <!-- Message title and timestamp -->
                              <h4>
                                Support Team
                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                              </h4>
                              <!-- The message -->
                              <p>Why not buy a new awesome theme?</p>
                            </a>
                          </li><!-- end message -->
                        </ul><!-- /.menu -->
                      </li>
                      <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                  </li><!-- /.messages-menu -->

                
                  <!-- User Account Menu -->
                  <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <!-- The user image in the navbar-->
                      <i class="fa fa-user"></i>
                      <!-- hidden-xs hides the username on small devices so only the image appears. -->
                      <span class="hidden-xs"><?= $_SESSION['username'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                      <!-- The user image in the menu -->
                      <li class="user-header">
                          <i class="fa fa-2x fa-user"></i>
                        <p>
                          <?= $_SESSION['username'] ?> - User
                         
                        </p>
                      </li>
                      <!-- Menu Body -->
                      
                      <!-- Menu Footer-->
                      <li class="user-footer">
                        <div class="pull-left">
                          <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="<?= main_url().'auth/logout' ?>" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>
<style type='text/css'>

.input-group-unstyled input.form-control {
  -webkit-border-radius: 4px !important;
  -moz-border-radius: 4px !important;
  border-radius: 4px !important;
}
.input-group-unstyled .input-group-addon {
  border-radius: 4px;
  border: 0px;
  background-color: transparent;
}
 .ui-autocomplete { 
            cursor:pointer; 
            height:120px; 
            overflow-y:scroll;
        }  
</style>
<script type="text/javascript">
$(document).ready(function(){    
    //Check if the current URL contains '#'
    if(document.URL.indexOf("#")==-1){
        // Set the URL to whatever it was plus "#".
        url = document.URL+"#";
        location = "#";

        //Reload the page
        location.reload(true);
    }
});
</script>
<script>
        $(document).ready(function() {
            getProjectList();
        });
        function getProjectList(){
	        var arrayOfObject = [];
	        var obj = {};
			obj.value = -11;
			obj.label = "All Project";
            obj.manager = -11;
            obj.client = -11;
			arrayOfObject.push(obj);
	        $.ajax({
				  method: "GET",
				  url: "php/datafeed.php?method=getProjectList",
				  asyn:true,
				  cache:true,
				  dataType: 'json',
				  success: function(data){
						if(data != null){
							//var object = JSON.parse(data);
							var projectList = data.project;
							for(var pl = 0 ; pl < projectList.length;pl++){
								var project = projectList[pl];
								obj = {};
								obj.value = project[0];
								obj.label = project[1];
	                            obj.manager = project[3];
	                            obj.client = project[4];
								arrayOfObject.push(obj);
							}
							BindControls(arrayOfObject);
						}
					},error: function(XMLHttpRequest, textStatus, errorThrown) { 
				       // alert("Status: " + textStatus); alert("Error: " + errorThrown); 
				    } 
				});
       }
	function clickSeach(){
		var id = $("#inputGroup").attr("valId");
		var val = $("#inputGroup").val();
        var managerId = $("#inputGroup").attr("managerId");
        var clientId = $("#inputGroup").attr("clientId");
		setProjectId(id,-11,managerId , clientId);
	}
    function BindControls(arrayOfObject) {
            $('#inputGroup').autocomplete({
                source: arrayOfObject,
                minLength: 0,
                scroll: true,
				select: function (event, ui) {
		        			$("#inputGroup").val(ui.item.label); // display the selected text
		        			$("#inputGroup").attr("valId" ,ui.item.value); // save selected id to hidden input
		                    $("#inputGroup").attr("managerId" ,ui.item.manager); // save selected manager id to hidden input
		                    $("#inputGroup").attr("clientId" ,ui.item.client); // save selected client id to hidden input
		 				return false;
		   		}
            }).focus(function() {
                $(this).autocomplete("search", "");
            }).val('All Project').data('autocomplete')._trigger('select');;
    }
    function setProjectId(projectId , categoryId, managerId ,clientId){
    	$.ajax({
    		  method: "GET",
    		  url: "php/datafeed.php?method=setProjectId",
    		  asyn: true,
    		  cache:true,
    		  data : 'projectId='+projectId+'&categoryId='+categoryId+'&managerId='+managerId+"&clientId="+clientId,
    		  success: function(data){
    				if(data != null){
                        var height =  getViewport();
    					$( "iframe" ).css( "height",height.viewPortHeight);
    					$("iframe").attr("src",'<?= base_url() ?>dashboard.php?token='+data);
    				}
    			}
    		});
    }
        
    </script>