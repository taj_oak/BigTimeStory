<!-- jQuery 2.1.4 -->

<script src="<?= main_url().'Admin/' ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?= main_url().'Admin/' ?>js/admin.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?= main_url().'Admin/' ?>bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?= main_url().'Admin/' ?>plugins/select2/select2.full.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= main_url().'Admin/' ?>plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?= main_url().'Admin/' ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= main_url().'Admin/' ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= main_url().'Admin/' ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= main_url().'Admin/' ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= main_url().'Admin/' ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= main_url().'Admin/' ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= main_url().'Admin/' ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= main_url().'Admin/' ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= main_url().'Admin/' ?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= main_url().'Admin/' ?>dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script>
    $().ready(function () {
        //alert(baseurl) 
    });
</script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
    $(".select2").select2();
</script>
<script>
 function getViewport() {

 var viewPortWidth;
 var viewPortHeight;

 // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
 if (typeof window.innerWidth != 'undefined') {
   viewPortWidth = window.innerWidth,
   viewPortHeight = window.innerHeight
 }

// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
 else if (typeof document.documentElement != 'undefined'
 && typeof document.documentElement.clientWidth !=
 'undefined' && document.documentElement.clientWidth != 0) {
    viewPortWidth = document.documentElement.clientWidth,
    viewPortHeight = document.documentElement.clientHeight
 }

 // older versions of IE
 else {
   viewPortWidth = document.getElementsByTagName('body')[0].clientWidth,
   viewPortHeight = document.getElementsByTagName('body')[0].clientHeight
 }
 return {'viewPortWidth':viewPortWidth,'viewPortHeight':viewPortHeight};
}
$().ready(function (){
   var height =  getViewport();
  $( "iframe" ).css( "height",height.viewPortHeight);
  $("iframe").attr("src",'<?= base_url() ?>dashboard.php?token=<?=  base64_encode(serialize($_SESSION))?>');
});
</script>
<!-- AdminLTE for demo purposes -->
<script src="<?= main_url().'Admin/' ?>dist/js/demo.js"></script>
<script src="<?= main_url().'Admin/' ?>dist/js/pages/dashboard2.js"></script>
</body>
</html>