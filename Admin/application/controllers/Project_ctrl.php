<?php

class Project_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('project_model');
        $this->load->helper("url");
        $this->load->library('form_validation');
        if (!isset($_SESSION['role']) || $_SESSION['role'] != 'admin') {
            redirect(main_url());
        }
    }

    public function index() {
        $data['allmanagers'] = $this->project_model->getAllManager();
        $data['allclients'] = $this->project_model->getAllClient();
        $data['allprojects'] = $this->project_model->getAllProject();
        $data['page'] = 'pages/project';
        $this->load->view('main', $data);
    }

    public function createProject() {
        $this->form_validation->set_error_delimiters('<div class="error"><font color="red">', '</font></div>');
        //Validating Name Field
        $this->form_validation->set_rules('pname', 'Name', 'required|min_length[5]|max_length[50]');

        if ($this->form_validation->run() == FALSE) {
            $data['allmanagers'] = $this->project_model->getAllManager();
            $data['allclients'] = $this->project_model->getAllClient();
            $data['allprojects'] = $this->project_model->getAllProject();
            $data['page'] = 'pages/project';
            $this->load->view('main', $data);
        } else {
            $id = $this->input->post('did');
            //Setting values for tabel columns
            $data = array(
                'project_name' => $this->input->post('pname'),
                'manager_id' => $this->input->post('pmanagerId'),
                'client_id' => $this->input->post('pclientId'),
                'color' => $this->selectcolr(),
                'status' => '1'
            );

            //Transfering data to Model
            if ($id != "") {
                $this->project_model->update_project($id, $data);
                redirect(base_url('project_ctrl'));
            } else {
                $this->project_model->addProject($data);
            }
            //Loading View
            $data['allmanagers'] = $this->project_model->getAllManager();
            $data['allclients'] = $this->project_model->getAllClient();
            $data['allprojects'] = $this->project_model->getAllProject();
            $data['page'] = 'pages/project';
            $this->load->view('main', $data);
        }
    }
    function selectcolr(){
       $value = rand(0, 21);
       if($value<=21){
           return $value;
       }else{
           $this->selectcolr();
       }
    }
            function randomColor() {
        $str = '#';
        for ($i = 0; $i < 6; $i++) {
            $randNum = rand(0, 15);
            switch ($randNum) {
                case 10: $randNum = 'A';
                    break;
                case 11: $randNum = 'B';
                    break;
                case 12: $randNum = 'C';
                    break;
                case 13: $randNum = 'D';
                    break;
                case 14: $randNum = 'E';
                    break;
                case 15: $randNum = 'F';
                    break;
            }
            $str .= $randNum;
        }
        return $str;
    }

    public function deleteProject() {
        $this->load->database();
        $this->load->model('project_model');
        $id = $this->uri->segment(3);
        $this->project_model->delete_project_id($id);
        $data['allmanagers'] = $this->project_model->getAllManager();
        $data['allclients'] = $this->project_model->getAllClient();
        $data['allprojects'] = $this->project_model->getAllProject();
        $data['page'] = 'pages/project';
        $this->load->view('main', $data);
    }

    public function select_project_id($id) {
        $id = $this->uri->segment(3);
        $data['single_project'] = $this->project_model->getProjectDetailsById($id);
        $data['allmanagers'] = $this->project_model->getAllManager();
        $data['allclients'] = $this->project_model->getAllClient();
        $data['allprojects'] = $this->project_model->getAllProject();
        $data['page'] = 'pages/project';
        $this->load->view('main', $data);
    }

}