<?php

class Manager_detailView_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('manager_detailView_model');
        $this->load->helper("url");
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }
    public function showDeails() {
        $id = $this->uri->segment(3);
	$data['allprojects']= $this->manager_detailView_model->getAllProjectforManager($id);
        $data['allemployees']= $this->manager_detailView_model->getAllEmployeeforManager($id);     
	$data['page']='pages/manager_detailView';
        $this->load->view('main', $data);
    }
    
    }