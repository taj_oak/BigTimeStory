<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class Project extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('ProjectModel');
        $this->load->helper("url");
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }
    public function index() {
		$data['error']='';
		$data['allusers']= $this->EmployeeModel->getAllUser();
		$data['page']='pages/employee';
        $this->load->view('main', $data);
    }
    public function invite() {
	
        //print_r($_POST);
        if($this->EmployeeModel->is_email_available($_POST['email'])){
           $data['new_email_key'] = md5(rand().microtime());
           $data['email'] =  $_POST['email'];
           $data['role']= $_POST['role'];
           $data['cmp_id']=$_SESSION['user_id'];
           $this->EmployeeModel->create_user($data);
		   redirect(base_url('employee'));
        }else{
             $data['error']='Email Already used by another';
        }
         $data['page']='pages/employee';
         $this->load->view('main', $data);
    }
	public function editInvite($id) {
		if(isset($_POST['email'])){
        //print_r($_POST);
        if($this->EmployeeModel->is_email_available($_POST['email'])){
           $data['new_email_key'] = md5(rand().microtime());
           $data['email'] =  $_POST['email'];
           $data['role']= $_POST['role'];
           $data['cmp_id']=$_SESSION['user_id'];
           $this->EmployeeModel->update_user($id, $data);
		   redirect(base_url('employee'));
        }else{
             $data['error']='Email Already used by another';
        }
		$data['allusers']= $this->EmployeeModel->getAllUser();
         $data['page']='pages/employee';
         $this->load->view('main', $data);
    }else{
	$data['allusers']= $this->EmployeeModel->getAllUser();
		 $data['user']= $this->EmployeeModel->getAllUserById($id);
		 $data['page']='pages/employee';
         $this->load->view('main', $data);
	}
   }
    
}