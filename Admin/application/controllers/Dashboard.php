<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashbord
 *
 * @author SKM
 */
class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('client_model');
        $this->load->model('project_model');
        $this->load->model('vendor_model');
        $this->load->model('employeeModel');
        $this->load->helper("url");
        $this->load->library('form_validation');
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }

    public function index() {
        
        $u_info = $this->session->userdata('user_id');
        
        
       $data['totalcount']= array(
           'totalproject'=> count($this->project_model->getAllProject()),
           'totalclient'=> count($this->project_model->getAllClient()),
           'totalvendor'=>  count($this->vendor_model->getAllVendor()),
           'totalmembers'=>  count($this->employeeModel->getAllUser()),
           'totalprojectexpense' => $this->project_model->getTotalProjectExpense($u_info),
           'totalprojecttime' => $this->project_model->getTotalProjectTimeSheet($u_info)
       );
       $data['latestTime'] = $this->employeeModel->getalltimesheet();
       $data['latestMembers'] = $this->employeeModel->getAllLettestUser();
        $data['latestProjects'] = $this->project_model->getAllLattestProject();
        
        $data['time_sheet_chart'] = $this->project_model->getTimeSheetChart($u_info);
        $data['expense_chart'] = $this->project_model->getExpenseChart($u_info);
        
        $data['time_sheet_piechart'] = $this->project_model->getTimeSheetPieChart($u_info);
        $data['time_sheet_approvedchart'] = $this->project_model->getTimeSheetApprovedChart($u_info);
        $data['time_sheet_rejectedchart'] = $this->project_model->getTimeSheetRejectedChart($u_info);
        $data['time_sheet_pendingchart'] = $this->project_model->getTimeSheetPendingChart($u_info);
        
        
        $data['expense_sheet_piechart'] = $this->project_model->getExpenseSheetPieChart($u_info);
        $data['expense_sheet_approvedchart'] = $this->project_model->getExpenseSheetApprovedChart($u_info);
        $data['expense_sheet_rejectedchart'] = $this->project_model->getExpenseSheetRejectedChart($u_info);
        $data['expense_sheet_pendingchart'] = $this->project_model->getExpenseSheetPendingChart($u_info);
        
       $data['page']='pages/dashboard';
        $this->load->view('main', $data);
    }

}
