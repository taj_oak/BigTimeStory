<?php  
   class Vendor_select_ctrl extends CI_Controller  
   {  
      public function index()  
      {  
         //load the database  
         $this->load->database();  
         //load the model  
         $this->load->model('vendor_select_model');  
         //load the method of model  
         $data['h']=$this->vendor_select_model->select();  
         //return the data in view  
         $this->load->view('vendor_select_view', $data);  
      } 
	
		// Function to Delete selected record from database.
		function delete_vendor_id() {
                    //load the database  
                    $this->load->database();  
                    //load the model  
                    $this->load->model('vendor_select_model');  
                    $id = $this->uri->segment(3);
                    $this->vendor_select_model->delete_vendor_id($id);
                    //load the method of model  
                    $data['h']=$this->vendor_select_model->select();  
                    //return the data in view  
                    $this->load->view('vendor_select_view', $data);    
		}	
   }  