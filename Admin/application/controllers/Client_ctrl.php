<?php

class Client_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('client_model');
        $this->load->helper("url");
        $this->load->library('form_validation');
        if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    //print_r($_SESSION);die;
    }
    public function index() {
		$data['allclients']= $this->client_model->getAllClient();
		$data['page']='pages/client';
                $this->load->view('main', $data);
    }
    
    public function createClient() {
        $this->form_validation->set_error_delimiters('<div class="error"><font color="red">', '</font></div>');
        //Validating Name Field
        $this->form_validation->set_rules('dname', 'Name', 'required|min_length[5]|max_length[15]');
        //Validating Address Field
        $this->form_validation->set_rules('daddress', 'Address', 'required|min_length[10]|max_length[500]');
        //Validating Email Field
        $this->form_validation->set_rules('demail', 'Email', 'required|valid_email');
        //Validating Mobile no. Field
        $this->form_validation->set_rules('dmobile', 'Phone Number', 'required|regex_match[/^[0-9]{10}$/]');
        //Validating Per Hour Field
        $this->form_validation->set_rules('dperhr', 'Per Hour', 'required|regex_match[/^[0-9]{1,3}$/]');
        if ($this->form_validation->run() == FALSE) {
            $data['allclients']= $this->client_model->getAllClient();
            $data['page']='pages/client';
            $this->load->view('main', $data);
 
        } else {
            $id= $this->input->post('did');
            //Setting values for tabel columns
            $data = array(
                'client_name' => $this->input->post('dname'),
                'client_address' => $this->input->post('daddress'),
                'client_email_id' => $this->input->post('demail'),
                'phone_number' => $this->input->post('dmobile'),
                'per_hour' => $this->input->post('dperhr'),
                'skype_id' => $this->input->post('dskype'),
                'payment_cycle' => $this->input->post('dpcycle'),
                'cmp_id' => $_SESSION['user_id']
            );
           
            //Transfering data to Model
            if($id != ""){
                    $this->client_model->update_client($id,$data);
                    redirect(base_url('client_ctrl'));
            }else{
                $this->client_model->createClient($data);
            }
            //Loading View
            $data['allclients']= $this->client_model->getAllClient();
            $data['page']='pages/client';
            $this->load->view('main', $data);
        }
    }
    // Function to Delete selected record from database.
		function delete_client_id() {
                    //load the database  
                    $this->load->database();  
                    //load the model  
                    $this->load->model('client_model');  
                    $id = $this->uri->segment(3);
                    $this->client_model->delete_client_id($id);
                    $data['allclients']= $this->client_model->getAllClient();
                    $data['page']='pages/client';
                    $this->load->view('main', $data); 
		}
                
                public function select_client_id($id) {
                 $id = $this->uri->segment(3);
                 $data['single_client'] = $this->client_model->getClientDetailsById($id);
                 $data['allclients']= $this->client_model->getAllClient();
		 $data['page']='pages/client';
                 $this->load->view('main', $data);
                }

    }