<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class Groupmanage extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('EmployeeModel');
        $this->load->model('GroupModel');
        $this->load->helper("url");
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }
    public function index() {
		$data['error']='';
		$data['allemployees']= $this->EmployeeModel->getAllUser();
                $data['allgroups']= $this->GroupModel->getteamsList();
		$data['page']='pages/group';
                $this->load->view('main', $data);
    }
    public function getTeamList(){
        if(isset($_POST['team_id'])){
      
      $data = $this->GroupModel->getAllUserByTeam($_POST['team_id']);
     
      echo  json_encode($data);
        }
    }
    public function AddGroup() {
	
           $data['team_name'] =  $_POST['group'];
           $data['team_mngr_id']=$_SESSION['user_id'];
           $data1['user_id'] = $_POST['pemployeeId'];
           
           $this->GroupModel->AddGroup($data,$data1);
           redirect(base_url('groupmanage'));
     
        $data['allusers']= $this->EmployeeModel->getAllUser();
         $data['page']='pages/group';
         $this->load->view('main', $data);
    }
	public function editInvite($id) {
		if(isset($_POST['email'])){
        //print_r($_POST);
        if($this->EmployeeModel->is_email_available($_POST['email'])){
           $data['new_email_key'] = md5(rand().microtime());
           $data['username'] =  $_POST['ename'];
           $data['email'] =  $_POST['email'];
           $data['role']= $_POST['role'];
           $data['cmp_id']=$_SESSION['user_id'];
           
           $data1['emp_name'] =  $_POST['ename'];
           $data1['emp_email_id'] =  $_POST['email'];
           
           $this->EmployeeModel->update_user($id, $data,$data1);
		   redirect(base_url('employee'));
        }else{
             $data['error']='Email Already used by another';
        }
		$data['allusers']= $this->EmployeeModel->getAllUser();
         $data['page']='pages/employee';
         $this->load->view('main', $data);
    }else{
	$data['allusers']= $this->EmployeeModel->getAllUser();
		 $data['user']= $this->EmployeeModel->getAllUserById($id);
		 $data['page']='pages/employee';
         $this->load->view('main', $data);
	}
   }
    
}