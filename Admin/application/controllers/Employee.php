<?php
require_once('phpass-0.1/PasswordHash.php');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class Employee extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('EmployeeModel');
        $this->load->helper("url");
        if (!isset($_SESSION['role']) || $_SESSION['role'] != 'admin') {
            redirect(main_url());
        }
    }

    public function index() {
        $data['error'] = '';
        $data['allusers'] = $this->EmployeeModel->getAllUser();
        $data['page'] = 'pages/employee';
        $this->load->view('main', $data);
    }

    public function invite() {
        
        //print_r($_POST);
        if ($this->EmployeeModel->is_email_available($_POST['email'])) {
           
            $password = $this->generateRandomString();
             $hasher = new PasswordHash(8,FALSE);
            $hashed_password = $hasher->HashPassword($password);
            $data['new_email_key'] = md5(rand() . microtime());
            $data['username'] = $_POST['ename'];
            $data['email'] = $_POST['email'];
            $data['role'] = $_POST['role'];
            $data['cmp_id'] = $_SESSION['user_id'];
            $data['password'] = $hashed_password;
            $data1['emp_name'] = $_POST['ename'];
            $data1['emp_email_id'] = $_POST['email'];

            if ($this->EmployeeModel->create_user($data, $data1)) {
                $message['msg'] ="Your Account has created, Please login with $data[email] and $password";
                $message['inviteBy']=$_SESSION['username'];
                $message['username']=$data['username'];
                $this->sendMail($data['email'], "Invited for Timesheet", $message);
            }
            redirect(base_url('employee'));
        } else {
            $data['error'] = 'Email Already used by another';
            $data['allusers'] = $this->EmployeeModel->getAllUser();
        }
        $data['allusers'] = $this->EmployeeModel->getAllUser();
        $data['page'] = 'pages/employee';
        $this->load->view('main', $data);
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function sendMail($userEmail, $subject, $message) {
        $config = Array(
            'protocol' => 'sendmail',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 25,
            'smtp_user' => 'mamtaj786@gmail.com',
            'smtp_timeout' => '4',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from('mamtaj786@gmail.com', 'Time Sheet');
        $data = array(
            'message' => $message
        );
        $this->email->to($userEmail);  // replace it with receiver mail id
        $this->email->subject($subject); // replace it with relevant subject 

        $body = $this->load->view('email/verify.php', $data, TRUE);
        $this->email->message($body);
        $this->email->send();
        return array('emailSucess' => 'sent email');
    }

    public function editInvite($id) {
        if (isset($_POST['email'])) {
            //print_r($_POST);
            if ($this->EmployeeModel->is_email_available($_POST['email'])) {
                $data['new_email_key'] = md5(rand() . microtime());
                $data['username'] = $_POST['ename'];
                $data['email'] = $_POST['email'];
                $data['role'] = $_POST['role'];
                $data['cmp_id'] = $_SESSION['user_id'];

                $data1['emp_name'] = $_POST['ename'];
                $data1['emp_email_id'] = $_POST['email'];

                $this->EmployeeModel->update_user($id, $data, $data1);
                redirect(base_url('employee'));
            } else {
                $data['error'] = 'Email Already used by another';
            }
            $data['allusers'] = $this->EmployeeModel->getAllUser();
            $data['page'] = 'pages/employee';
            $this->load->view('main', $data);
        } else {
            $data['allusers'] = $this->EmployeeModel->getAllUser();
            $data['user'] = $this->EmployeeModel->getAllUserById($id);
            $data['page'] = 'pages/employee';
            $this->load->view('main', $data);
        }
    }
  
}
