<?php

class Expense_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('expense_model');
        $this->load->helper("url");
        $this->load->library('form_validation');
        if (!isset($_SESSION['role']) || $_SESSION['role'] != 'admin') {
            redirect(main_url());
        }
    }

    public function index() {
        $data['allcategory'] = $this->expense_model->getAllCategory();
        $data['page'] = 'pages/expense';
        $this->load->view('main', $data);
    }

    public function createExpenseCategory() {
        $this->form_validation->set_error_delimiters('<div class="error"><font color="red">', '</font></div>');
        $this->form_validation->set_rules('catname', 'Category Name', 'required');
        $this->form_validation->set_rules('cstatus', 'Status', 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['allcategory'] = $this->expense_model->getAllCategory();
            $data['page'] = 'pages/expense';
            $this->load->view('main', $data);
        } else {
            $id = $this->input->post('catid');
            //Setting values for tabel columns
            $color = $this->input->post('ccolor');
            if($color == ""){
                $color = "#7fb6d6";
            }
            $data = array(
                'cat_name' => $this->input->post('catname'),
                'mileage' => $this->input->post('mileage'),
                'color' => $color,
                'status' => $this->input->post('cstatus')
            );
            //print_r($data);
            
            //Transfering data to Model
            if ($id != "") {
                $this->expense_model->updateCategory($id, $data);
                //redirect('expense_ctrl');
                //redirect('expense_ctrl', 'refresh');
            } else {
                $this->expense_model->addCategory($data);
            }
            //Loading View
            $data['allcategory'] = $this->expense_model->getAllCategory();
            $data['page'] = 'pages/expense';
            $this->load->view('main', $data);
        }
    }
   
    public function deleteExpenseCategory() {
        $this->load->database();
        $this->load->model('expense_model');
        $id = $this->uri->segment(3);
        $this->expense_model->deleteCategory($id);
        $data['allcategory'] = $this->expense_model->getAllCategory();
        $data['page'] = 'pages/expense';
        $this->load->view('main', $data);
    }
    
     public function updateExpenseCategory($id) {
        $id = $this->uri->segment(3);
        $data['selectedCat'] = $this->expense_model->getCategoryById($id);
        $data['allcategory'] = $this->expense_model->getAllCategory();
        $data['page'] = 'pages/expense';
        $this->load->view('main', $data);
    }
}