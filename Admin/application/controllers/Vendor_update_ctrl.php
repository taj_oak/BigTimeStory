<?php
class Vendor_update_ctrl extends CI_Controller{
function __construct(){
parent::__construct();
$this->load->model('vendor_update_model');
}
/*Update*/
function show_vendor_id() {        
$id = $this->uri->segment(3);
$data['single_vendor'] = $this->vendor_update_model->show_vendor_id($id);
$this->load->view('vendor_update_view', $data);
}
function update_vendor_id() {
$id= $this->input->post('did');
//Setting values for tabel columns
$data = array(
'vendor_name' => $this->input->post('dname'),
'vendor_address' => $this->input->post('daddress'),
'vendor_email_id' => $this->input->post('demail'),
'phone_number' => $this->input->post('dmobile'),
'per_hour'=>$this->input->post('dperhr'),
'skype_id'=>   $this->input->post('dskype'),
'payment_cycle'=>$this->input->post('dpcycle')    
);
$this->vendor_update_model->update_vendor_id($id,$data);
//load the database  
$this->load->database(); 
//load the method of model  
$data['h']=$this->vendor_update_model->select();  
//return the data in view  
$this->load->view('vendor_select_view', $data);    
}
}



