<?php

class ProjectAssigned_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('projectAssigned_model');
      $this->load->model('project_detailView_model');
       if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
        $this->load->helper("url");
        $data['allprojects']= $this->projectAssigned_model->getAllProject();
        $projects = $data['allprojects'];
        $k = 0;
        foreach ($projects as $project) {
           $data['allprojects'][$k]->who_created = $this->project_detailView_model->getAllProjectforEmployee($project->project_id);
           $k++;
        }
        $this->data=$data;
        //$this->load->library('form_validation');
    }
    public function index() {
	//$data['allvendors']= $this->projectAssigned_model->getAllVendor();
        
        $data = $this->data;
        $data['allemployees']= $this->projectAssigned_model->getAllEmployee();
//        $data['allprojects']= $this->projectAssigned_model->getAllProject();
//        $projects = $data['allprojects'];
//        $k = 0;
//        foreach ($projects as $project) {
//           $data['allprojects'][$k]->who_created = $this->project_detailView_model->getAllProjectforEmployee($project->project_id);
//           $k++;
//        }
       // print_r( $data['allprojects']);die;
        //$data['allVendorAssignedProjects']= $this->projectAssigned_model->getAllVendorAssignedProject();
        $data['allEmployeeAssignedProjects']= $this->projectAssigned_model->getAllEmployeeAssignedProject();
	$data['page']='pages/projectAssigned';
        $this->load->view('main', $data);
    }
    
    public function createAssignedProject() {
         $data = $this->data;
            $id= $this->input->post('did');
            //Setting values for tabel columns
           $emp_ids = $this->input->post('pemployeeId');
            //Transfering data to Model
            
            if($id != ""){
                    if($this->input->post('pemployeeId') != ""){
                        $data = array(
                                    'project_id' => $this->input->post('projectId'),
                                    'employee_id' => $this->input->post('pemployeeId'),
                        );
           
                        $this->projectAssigned_model->update_employee_assigned_project($id,$data);
                    }
                    redirect(base_url('projectAssigned_ctrl'));
            }else{
                if($this->input->post('pemployeeId') != ""){
                    foreach ($emp_ids as $emp_id) {
                        
                         $data = array(
                                    'project_id' => $this->input->post('projectId'),
                                    'employee_id' => $emp_id,
                    );
                    $this->projectAssigned_model->addAssignedProjectEmployee($data);
                    }
                   
                }
                redirect(base_url('projectAssigned_ctrl'));
            }
            
            $data['allemployees']= $this->projectAssigned_model->getAllEmployee();
           
            //$data['allVendorAssignedProjects']= $this->projectAssigned_model->getAllVendorAssignedProject();
            $data['allEmployeeAssignedProjects']= $this->projectAssigned_model->getAllEmployeeAssignedProject();
            $data['page']='pages/projectAssigned';
            $this->load->view('main', $data);
        //}
    }
    
    public function deleteAssignedProject() {  
         $data = $this->data;
            $this->load->database();  
            $this->load->model('projectAssigned_model');  
            $id = $this->uri->segment(3);
            $projectId = $this->uri->segment(4);
            if($projectId != ""){
                $this->projectAssigned_model->delete_employee_assigned_project_id($id,$projectId);
                 redirect(base_url('projectAssigned_ctrl'));
            }
            //$data['allvendors']= $this->projectAssigned_model->getAllVendor();
            $data['allemployees']= $this->projectAssigned_model->getAllEmployee();
          
            
            $data['page']='pages/projectAssigned';
            $this->load->view('main', $data); 
	}
                
    public function select_assigned_project_id() {
            $id = $this->uri->segment(3);
            $type = $this->uri->segment(4);
            /*if($type == "vendor"){
                $data['single_projectAssigned'] = $this->projectAssigned_model->getVendorAssignedProjectDetailsById($id);
            }else*/ if($type == "employee"){
                $data['single_projectAssigned'] = $this->projectAssigned_model->getEmployeeAssignedProjectDetailsById($id);
            }
            //$data['allvendors']= $this->projectAssigned_model->getAllVendor();
            $data['allemployees']= $this->projectAssigned_model->getAllEmployee();
           $data = $this->data;
            //$data['allVendorAssignedProjects']= $this->projectAssigned_model->getAllVendorAssignedProject();
            $data['allEmployeeAssignedProjects']= $this->projectAssigned_model->getAllEmployeeAssignedProject();
            $data['page']='pages/projectAssigned';
            $this->load->view('main', $data);
        }
}