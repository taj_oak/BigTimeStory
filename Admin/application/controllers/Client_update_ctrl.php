<?php
class Client_update_ctrl extends CI_Controller{
function __construct(){
parent::__construct();
$this->load->model('client_update_model');
}
/*Update*/
function show_client_id() {        
$id = $this->uri->segment(3);
$data['single_client'] = $this->client_update_model->show_client_id($id);
$this->load->view('client_update_view', $data);
}
function update_client_id() {
$id= $this->input->post('did');
//Setting values for tabel columns
$data = array(
'client_name' => $this->input->post('dname'),
'client_address' => $this->input->post('daddress'),
'client_email_id' => $this->input->post('demail'),
'phone_number' => $this->input->post('dmobile'),
'per_hour'=>$this->input->post('dperhr'),
'skype_id'=>   $this->input->post('dskype'),
'payment_cycle'=>$this->input->post('dpcycle')    
);
$this->client_update_model->update_client_id($id,$data);
//load the database  
$this->load->database(); 
//load the method of model  
$data['h']=$this->client_update_model->select();  
//return the data in view  
$this->load->view('client_select_view', $data);    
}
}
?>



