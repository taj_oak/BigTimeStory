<?php

class Vendor_ctrl extends CI_Controller {

function __construct() {
parent::__construct();
$this->load->model('vendor_model');
$this->load->helper("url");
$this->load->library('form_validation');
 if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
}

public function index() {
		$data['allvendors']= $this->vendor_model->getAllVendor();
		$data['page']='pages/vendor';
                $this->load->view('main', $data);
    }
    
    public function createVendor() {
        $this->form_validation->set_error_delimiters('<div class="error"><font color="red">', '</font></div>');
        //Validating Name Field
        $this->form_validation->set_rules('dname', 'Name', 'required|min_length[5]|max_length[15]');
        //Validating Address Field
        $this->form_validation->set_rules('daddress', 'Address', 'required|min_length[10]|max_length[500]');
        //Validating Email Field
        $this->form_validation->set_rules('demail', 'Email', 'required|valid_email');
        //Validating Mobile no. Field
        $this->form_validation->set_rules('dmobile', 'Phone Number', 'required|regex_match[/^[0-9]{10}$/]');
        //Validating Per Hour Field
        $this->form_validation->set_rules('dperhr', 'Per Hour', 'required|regex_match[/^[0-9]{1,3}$/]');
        if ($this->form_validation->run() == FALSE) {
            $data['allvendors']= $this->vendor_model->getAllVendor();
            $data['page']='pages/vendor';
            $this->load->view('main', $data);
 
        } else {
            $id= $this->input->post('did');
            //Setting values for tabel columns
            $data = array(
                'vendor_name' => $this->input->post('dname'),
                'vendor_address' => $this->input->post('daddress'),
                'vendor_email_id' => $this->input->post('demail'),
                'phone_number' => $this->input->post('dmobile'),
                'per_hour' => $this->input->post('dperhr'),
                'skype_id' => $this->input->post('dskype'),
                'payment_cycle' => $this->input->post('dpcycle')
            );
           
            //Transfering data to Model
            if($id != ""){
                    $this->vendor_model->update_vendor($id,$data);
                    redirect(base_url('vendor_ctrl'));
            }else{
                $this->vendor_model->createVendor($data);
            }
            //Loading View
            $data['allvendors']= $this->vendor_model->getAllVendor();
            $data['page']='pages/vendor';
            $this->load->view('main', $data);
        }
    }
    // Function to Delete selected record from database.
		function delete_vendor_id() {
                    //load the database  
                    $this->load->database();  
                    //load the model  
                    $this->load->model('vendor_model');  
                    $id = $this->uri->segment(3);
                    $this->vendor_model->delete_vendor_id($id);
                    $data['allvendors']= $this->vendor_model->getAllVendor();
                    $data['page']='pages/vendor';
                    $this->load->view('main', $data); 
		}
                
                public function select_vendor_id($id) {
                 $id = $this->uri->segment(3);
                 $data['single_vendor'] = $this->vendor_model->getVendorDetailsById($id);
                 $data['allvendors']= $this->vendor_model->getAllVendor();
		 $data['page']='pages/vendor';
                 $this->load->view('main', $data);
                }


}