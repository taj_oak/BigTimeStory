<?php

class Project_detailView_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('project_detailView_model');
        $this->load->helper("url");
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }
    public function showDeails() {
        $id = $this->uri->segment(3);
	$data['allmanagers']= $this->project_detailView_model->getAllProjectforManager($id);
        $data['allclients']= $this->project_detailView_model->getAllProjectforClient($id);
        $data['allemployees']= $this->project_detailView_model->getAllProjectforEmployee($id);
        $data['allvendors']= $this->project_detailView_model->getAllProjectforVendor($id);
        $data['allcat']= $this->project_detailView_model->getAllProjectCat($id);
        $data['project_id']=$id;
	$data['page']='pages/project_detailView';
        $this->load->view('main', $data);
    }
    function savecat() {
        if($_POST['cat_name']!=''){
        $this->project_detailView_model->saveCat($_POST);
           redirect($_SERVER['HTTP_REFERER']);
        }  else {
        $_SESSION['error']="Please enter Category Name";
           redirect($_SERVER['HTTP_REFERER']);
        }
    }       
    }