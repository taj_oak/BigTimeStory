<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class Timesheet extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('TimesheetModel');
        $this->load->helper("url");
         $this->load->library('Datatables');
       if (!isset($_SESSION['role']) || $_SESSION['role'] != 'admin') {
            redirect(main_url());
        }
    }

   
 
     public function report() {
        
        $data['allprojects'] = $this->TimesheetModel->getallProjectbyManager();
        $data['allcategory'] = $this->TimesheetModel->getAllCategory();
        $data['page'] = 'pages/report_timesheet';
        $this->load->view('main', $data);
    }

    public function expanseReportTable() {
        //print_r($_REQUEST);

 
        if (isset($_REQUEST['reporttype']) && $_REQUEST['reporttype'] == 'D') {
            //print_r($_REQUEST);
            
            $where = ''; 
            if (isset($_REQUEST['startdate']) && isset($_REQUEST['enddate']) && $_REQUEST['startdate'] != '') {

                $la_today = date('Y-m-d h:i:s');
                $ls_30days = date('Y-m-d h:i:s', strtotime("-30 days"));
                $startdate = (isset($_REQUEST['startdate'])) ? $_REQUEST['startdate'] : $ls_30days;
                $enddate = (isset($_REQUEST['enddate'])) ? $_REQUEST['enddate'] : $la_today;
                $this->datatables->where("start_time BETWEEN '$startdate' AND '$enddate' ");
            } elseif (isset($_REQUEST['weekno'])) {
                
                /* if(isset($_REQUEST['year'])) { $year = $_REQUEST['year']; } else { $year = date('Y');}
                
                $la_date = $this->getStartAndEndDate($_REQUEST['weekno'], $year);
               
                $startdate = $la_date[0];
                $enddate = $la_date[1];
                
                echo $startdate.' '.$enddate; 
                
                $this->datatables->where("start_time BETWEEN '$startdate' AND '$enddate' "); */
            }
            if (isset($_REQUEST['status']) && $_REQUEST['status'] != '' ) {
                $this->datatables->where_in('tms_time_sheet.status', stripslashes(implode($_REQUEST['status'], "','")));
            }
            if (isset($_REQUEST['projects'])) {
                //$this->datatables->where_in('tms_time_sheet.project_id', stripslashes(implode($_REQUEST['projects'], "','")));
                $this->datatables->where_in('tms_time_sheet.project_id', $_REQUEST['projects']);
            }
            if (isset($_REQUEST['category'])) {
                $this->datatables->where_in('tms_time_sheet.project_category_id', stripslashes(implode($_REQUEST['category'], "','")));
            }
            $this->datatables->select('time_sheet_id,project_name,task_name,start_time,tms_time_sheet.status,hours,overtime,emp_name');
            $this->datatables->from('tms_time_sheet');
            $this->datatables->join('tms_project', 'tms_project.project_id=tms_time_sheet.project_id');
            $this->datatables->join('tms_employee','tms_employee.user_id=tms_time_sheet.user_id');
         
            //  $this->datatables->filter();
            $this->datatables->add_column('status', '<span class="btn $1">$1</span>', 'status');
            // $this->datatables->add_column('file_path', '<a href="'+ base_url() +'upload/$1"  >View</a>', 'file_path');
            echo $this->datatables->generate('json', 'utf-8');
        } else {
            if (isset($_REQUEST['reporttype'])) {
                $startdate = (isset($_REQUEST['startdate'])) ? $_REQUEST['startdate'] : $ls_30days;
                $enddate = (isset($_REQUEST['enddate'])) ? $_REQUEST['enddate'] : $la_today;
                $this->datatables->where("start_time BETWEEN '$startdate' AND '$enddate' ");

                if (isset($_REQUEST['status'])) {
                    $this->datatables->where_in('tms_time_sheet.status', stripslashes(implode($_REQUEST['status'], "','")));
                }
                if (isset($_REQUEST['projects'])) {
                    $this->datatables->where_in('tms_time_sheet.project_id', stripslashes(implode($_REQUEST['projects'], "','")));
                }
            }
            $this->datatables->select('project_name,tms_time_sheet.project_id,weekNumber,tms_time_sheet.STATUS,SUM(hours+overtime) as totaltime');
            $this->datatables->from('tms_time_sheet');
            $this->datatables->join('tms_project', 'tms_project.project_id=tms_time_sheet.project_id');
            $this->datatables->group_by('tms_time_sheet.weekNumber');
              
           
            //$this->datatables->where();
            $this->datatables->add_column('STATUS', '<span class="btn $1">$1</span>', 'STATUS'); //?projects[]=$3weekno=$1&year=$2&reporttype=D
            $this->datatables->add_column('weekNumber', '<a target="_blank" href="?projects[]=$2&weekno=$1&reporttype=D" class="btn btn-primary ">Week $1 </span>', 'weekNumber,project_id');
           
            
            // $this->datatables->add_column('weekno', '<span onclick="getdetailsView($1,$2,$3)" class="btn btn-primary ">Week $1 of $2 </span>', 'weekno,year,project_id');
            echo $this->datatables->generate('json', 'utf-8');
            
        }
        // echo $this->db->last_query();
    }

    function getStartAndEndDate($week, $year) {
         $year = date('y', strtotime($year));
        $time = strtotime("1 January $year", time());
        $day = date('w', $time);
        $time += ((7 * $week) + 1 - $day) * 24 * 3600;
        $return[0] = date('Y-m-d', $time);
        $time += 6 * 24 * 3600;
        $return[1] = date('Y-m-d', $time);
        return $return;
    }

    public function getallProjectExpanse() {

        $la_allexpase['project'] = $this->TimesheetModel->getProjectwiseData();
        $la_allexpase['cat'] = $this->TimesheetModel->getCatwiseData();

        echo json_encode($la_allexpase);
    }

    public function getallProjectExpanseBar() {
        $la_allexpase['project'] = $this->TimesheetModel->getbarchartData();
        echo json_encode($la_allexpase);
    }
}
