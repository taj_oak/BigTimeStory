<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserProfile
 *
 * @author Sk Mamtajuddin
 */
class UserProfile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('GroupModel');
        $this->load->helper("url");
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }

    public function save() {
       
        $logo =   $this->do_upload($_FILES);
        $_POST['company_logo']=$logo;
   
    
        if($logo!=FALSE){
         $this->UserModel->create_company($_POST);
       
         redirect($_SERVER['HTTP_REFERER']);
           
         
        }  else {
             redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function do_upload($FILES) {
        $target_dir = "uploads/";
        $target_file = $target_dir .base64_encode($_SESSION['user_id']). basename($FILES["file"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if file already exists
        if (file_exists($target_file)) {
             $_SESSION['error']= "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
        if ($FILES["file"]["size"] > 500000) {
            $_SESSION['error']= "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
          //  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $_SESSION['error']="Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
             $_SESSION['error']="Sorry, Unable to upload.";
            return FALSE;
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($FILES["file"]["tmp_name"], $target_file)) {
                return $target_file;               
            } else {
                return FALSE;
            }
        }
    }

    //put your code here
}
