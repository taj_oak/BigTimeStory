<?php

class Consultant_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('consultant_model');
        $this->load->helper("url");
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }
    public function index() {
	$data['allvendors']= $this->consultant_model->getAllVendor();
        $data['allemployees']= $this->consultant_model->getAllEmployee();
        $data['allconsultant']= $this->consultant_model->getAllConsultant();
	$data['page']='pages/consultant';
        $this->load->view('main', $data);
    }
    
    public function createConsultant() {
            $user_id = $this->input->post('pemployeeId');
            if($user_id != ""){
                    
                    $data = array(
                                    'vendor_id' => $this->input->post('pvendorId'),
                        );
           
                        $this->consultant_model->addConsultant($user_id,$data);
                    
                    redirect(base_url('consultant_ctrl'));
            }else{
                    $data = array(
                                     'vendor_id' => $this->input->post('pvendorId'),
                        );
           
                        $this->consultant_model->addConsultant($user_id,$data);
            }
            //Loading View
            $data['allvendors']= $this->consultant_model->getAllVendor();
            $data['allemployees']= $this->consultant_model->getAllEmployee();
            $data['allconsultant']= $this->consultant_model->getAllConsultant();
            $data['page']='pages/consultant';
            $this->load->view('main', $data);
    }
    
    public function deleteConsultant() {  
            $this->load->database();  
            $this->load->model('consultant_model');  
            $id = $this->uri->segment(3);
            $data['vendor_id'] = 0;
            $this->consultant_model->delete_consultant($id,$data);        
            $data['allvendors']= $this->consultant_model->getAllVendor();
            $data['allemployees']= $this->consultant_model->getAllEmployee();
            $data['allconsultant']= $this->consultant_model->getAllConsultant();
            $data['page']='pages/consultant';
            $this->load->view('main', $data); 
	}
                
    public function selectConsultantById() {
            $id = $this->uri->segment(3);
            $data['single_consultant'] = $this->consultant_model->getConsultantDetailsById($id);
            $data['allvendors']= $this->consultant_model->getAllVendor();
            $data['allemployees']= $this->consultant_model->getAllEmployee();
            $data['allconsultant']= $this->consultant_model->getAllConsultant();
            $data['page']='pages/consultant';
            $this->load->view('main', $data); 
        }
}
?>

