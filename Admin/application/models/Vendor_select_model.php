<?php  
   class Vendor_select_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct(); 	
      }  
      //we will use the select function  
      function select()  
      {  
         //data is retrive from this query  
         $query = $this->db->get('tms_vendor');  
         return $query;  
      } 

      // Function to Delete selected record from table name students.
      function delete_vendor_id($id){
	 $this->db->where('vendor_id', $id);
	 $this->db->delete('tms_vendor');
      }	
   }  