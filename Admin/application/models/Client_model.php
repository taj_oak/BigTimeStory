<?php
class Client_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}
function createClient($data){
        $this->db->insert('tms_client', $data);
        
}
function getAllClient(){   
         $this->db->select('*');
	 $this->db->from('tms_client');	
         $this->db->where("cmp_id=$_SESSION[user_id]");
	 $query = $this->db->get();
	 return $query->result();
} 
function delete_client_id($id){
	 $this->db->where('client_id', $id);
	 $this->db->delete('tms_client');
}
function update_client($id,$data){
		$this->db->where('client_id', $id);
		$this->db->update('tms_client', $data);
	}	
function getClientDetailsById($id){
		$this->db->select('*');
		$this->db->from('tms_client');	
		$this->db->where('client_id =',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
}