<?php
class Project_detailView_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}
function getAllProjectforClient($id){   
         $this->db->select('*');
	 $this->db->from('tms_project');
         $this->db->join('tms_client','tms_project.client_id=tms_client.client_id');
         $this->db->join('users','tms_project.manager_id=users.id');
         $this->db->where('tms_project.project_id', $id);
	 $query = $this->db->get();
	 return $query->result();
} 
function getAllProjectforManager($id){   
         $this->db->select('*');
	 $this->db->from('tms_project');
         $this->db->join('users','tms_project.manager_id=users.id');
         $this->db->where('tms_project.project_id', $id);
	 $query = $this->db->get();
	 return $query->result();
}
function getAllProjectforEmployee($id){   
         $this->db->select('*');
	 $this->db->from('tms_project');
         $this->db->join('tms_project_employee','tms_project.project_id=tms_project_employee.project_id');
         $this->db->join('users','tms_project_employee.employee_id=users.id');
         $this->db->join('tms_employee','users.id=tms_employee.user_id');
         $this->db->where('tms_project.project_id', $id);
	 $query = $this->db->get();
	 return $query->result();
}
function getAllProjectforVendor($id){   
         $this->db->select('*');
	 $this->db->from('tms_project');
         $this->db->join('tms_project_employee','tms_project.project_id=tms_project_employee.project_id');
         $this->db->join('tms_employee','tms_project_employee.employee_id=tms_employee.user_id');
         $this->db->join('tms_vendor','tms_employee.vendor_id=tms_vendor.vendor_id');
         $this->db->where('tms_project.project_id', $id);
	 $query = $this->db->get();
	 return $query->result();
}

function getAllProjectCat($id){
    
    $query = $this->db->get_where('tms_categoris',array('project_id'=>$id));
    return $query->result();
}
function savecat($data) {
    $this->db->insert('tms_categoris',$data);
    return TRUE;
}

}