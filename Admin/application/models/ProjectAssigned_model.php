<?php

class ProjectAssigned_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAllEmployee() {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('tms_employee', 'tms_employee.user_id = users.id');
        $this->db->where(array('cmp_id' => $_SESSION['user_id'], 'role =' => 'user'));

        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

    function getAllProject() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->join('tms_client', 'tms_project.client_id=tms_client.client_id');
        $this->db->join('users', 'tms_project.manager_id=users.id');
        $this->db->where("tms_client.cmp_id=$_SESSION[user_id]");
        $query = $this->db->get();
        return $query->result();
    }

    function getAllProjectandEmployee() {
        $this->db->select(' tp.* , GROUP_CONCAT(tpm.employee_id separator ",") as employee');
        $this->db->from('tms_project_employee as tpm');
        $this->db->join('tms_project as tp', 'tp.project_id = tpm.project_id', 'left');
        $this->db->join('tms_client', 'tp.client_id=tms_client.client_id');
        $this->db->where("tms_client.cmp_id=$_SESSION[user_id]");
        $this->db->group_by('COALESCE(tpm.project_id)');
        $query = $this->db->get();
        return $query->result();
    }

    function addAssignedProjectEmployee($data) {
        $query = $this->db->get_where('tms_project_employee', $data);
        if ($query->num_rows() == 0) {
            $this->db->insert('tms_project_employee', $data);
        }
    }

    function getAllEmployeeAssignedProject() {
        $this->db->select('*');
        $this->db->from('tms_project_employee');
        $this->db->join('tms_project', 'tms_project_employee.project_id=tms_project.project_id');
        $query = $this->db->get();
        return $query->result();
    }

    function delete_employee_assigned_project_id($id, $project) {
        $this->db->where(array('employee_id' => $id, 'project_id' => $project));
        $this->db->delete('tms_project_employee');
    }

    function update_employee_assigned_project($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tms_project_employee', $data);
    }

    function getEmployeeAssignedProjectDetailsById($id) {
        $this->db->select('*');
        $this->db->from('tms_project_employee');
        $this->db->where('id =', $id);
        $query = $this->db->get();
        return $query->row();
    }

}