<?php

class Project_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAllManager() {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role =', 'manager');
        $this->db->where("cmp_id=$_SESSION[user_id]");
        $query = $this->db->get();
        return $query->result();
    }

    function getAllClient() {
        $this->db->select('*');
        $this->db->from('tms_client');
        $this->db->where("cmp_id=$_SESSION[user_id]");
        $query = $this->db->get();
        return $query->result();
    }

    function addProject($data) {
        $data['date_created'] = date('Y-m-d');
        $data['who_created'] = $_SESSION['user_id'];
        $this->db->insert('tms_project', $data);
    }

    function getAllProject() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->join('tms_client', 'tms_project.client_id=tms_client.client_id');
        $this->db->join('users', 'tms_project.manager_id=users.id');
        $this->db->where("tms_client.cmp_id=$_SESSION[user_id]");
        $query = $this->db->get();
        return $query->result();
    }

    function getAllLattestProject() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->join('tms_client', 'tms_project.client_id=tms_client.client_id');
        $this->db->join('users', 'tms_project.manager_id=users.id');
        $this->db->where("tms_client.cmp_id=$_SESSION[user_id]");
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }

    function delete_project_id($id) {
        $this->db->where('project_id', $id);
        $this->db->delete('tms_project');
        return TRUE;
    }

    function update_project($id, $data) {
        $data['date_updated'] = date('Y-m-d');
        $data['who_updated'] = $_SESSION['user_id'];
        $this->db->where('project_id', $id);
        $this->db->update('tms_project', $data);
    }

    function getProjectDetailsById($id) {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->where('project_id =', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getTotalProjectExpense($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(te.bill_amt) bill_amt from `tms_expense` te inner join `tms_project` tp on te.project_id = tp.project_id"
                    . " where tp.who_created = $company_id";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $rs_array = $query->row_array();
                return $rs_array['bill_amt'];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function getTotalProjectTimeSheet($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(ts.hours) hours from `tms_time_sheet` ts inner join `tms_project` tp on ts.project_id = tp.project_id"
                    . " where tp.who_created = $company_id";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $rs_array = $query->row_array();
                return $rs_array['hours'];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function getTimeSheetChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select DATE_FORMAT(ts.`start_time`,'%M') as month_name, sum(ts.hours) as count_hours"
                    . " from `tms_time_sheet` ts inner join `tms_project` tp on ts.project_id = tp.project_id"
                    . " where tp.who_created = $company_id group by MONTH(ts.start_time)";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $result = '';
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    
                    $result .= '["'.$val['month_name'].'",'.round($val['count_hours']).'],';
                }
                $res_final = substr($result, 0,-1);
                
               
                return $res_final;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function getExpenseChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select DATE_FORMAT(te.`bill_date`,'%M') as month_name, sum(te.bill_amt) as bill_amt"
                    . " from `tms_expense` te inner join `tms_project` tp on te.project_id = tp.project_id"
                    . " where tp.who_created = $company_id group by MONTH(te.bill_date)";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                $result = '';
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    
                    $result .= '["'.$val['month_name'].'",'.round($val['bill_amt']).'],';
                }
                $res_final = substr($result, 0,-1);
                
               
                return $res_final;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    
    function getTimeSheetPieChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(ts.hours) as project_hours, tp.project_name "
                    . " from `tms_time_sheet` ts inner join `tms_project` tp on ts.project_id = tp.project_id"
                    . " where tp.who_created = $company_id group by ts.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_hours = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
               
                foreach ($rs_array as $val) {
                    
                    $total_hours +=$val['project_hours'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['project_hours']/$total_hours)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    
    function getTimeSheetApprovedChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(ts.hours) as project_hours, tp.project_name "
                    . " from `tms_time_sheet` ts inner join `tms_project` tp on ts.project_id = tp.project_id"
                    . " where tp.who_created = $company_id and ts.status='Approved' group by ts.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_hours = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    $total_hours +=$val['project_hours'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['project_hours']/$total_hours)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function getTimeSheetRejectedChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(ts.hours) as project_hours, tp.project_name "
                    . " from `tms_time_sheet` ts inner join `tms_project` tp on ts.project_id = tp.project_id"
                    . " where tp.who_created = $company_id and ts.status='Rejected' group by ts.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_hours = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    $total_hours +=$val['project_hours'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['project_hours']/$total_hours)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function getTimeSheetPendingChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(ts.hours) as project_hours, tp.project_name "
                    . " from `tms_time_sheet` ts inner join `tms_project` tp on ts.project_id = tp.project_id"
                    . " where tp.who_created = $company_id and ts.status='Pending' group by ts.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_hours = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    $total_hours +=$val['project_hours'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['project_hours']/$total_hours)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    
    function getExpenseSheetPieChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(te.bill_amt) as bill_amt, tp.project_name "
                    . " from `tms_expense` te inner join `tms_project` tp on te.project_id = tp.project_id"
                    . " where tp.who_created = $company_id group by te.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_bill_amt = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
               
                foreach ($rs_array as $val) {
                    
                    $total_bill_amt +=$val['bill_amt'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['bill_amt']/$total_bill_amt)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function getExpenseSheetApprovedChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(te.bill_amt) as bill_amt, tp.project_name "
                    . " from `tms_expense` te inner join `tms_project` tp on te.project_id = tp.project_id"
                    . " where tp.who_created = $company_id and te.STATUS='Approved' group by te.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_bill_amt = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    $total_bill_amt +=$val['bill_amt'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['bill_amt']/$total_bill_amt)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    
    
    function getExpenseSheetRejectedChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(te.bill_amt) as bill_amt, tp.project_name "
                    . " from `tms_expense` te inner join `tms_project` tp on te.project_id = tp.project_id"
                    . " where tp.who_created = $company_id and te.STATUS='Rejected' group by te.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_bill_amt = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    $total_bill_amt +=$val['bill_amt'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['bill_amt']/$total_bill_amt)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    function getExpenseSheetPendingChart($company_id = '') {
        if ($company_id != '') {
            $sql = "select sum(te.bill_amt) as bill_amt, tp.project_name "
                    . " from `tms_expense` te inner join `tms_project` tp on te.project_id = tp.project_id"
                    . " where tp.who_created = $company_id and te.STATUS='Pending' group by te.project_id";
            $query = $this->db->query($sql);
// {label: "Series2", data: 30, color: "#3c8dbc"},
            if ($query->num_rows() > 0) {
                $total_bill_amt = 0.1;
                $result = array();
                $coller_array = array('#FF0F00','#FF6600','#FF9E01','#FCD202','#F8FF01','#B0DE09','#04D215','#0D8ECF','#0D52D1','#2A0CD0','#8A0CCF','#CD0D74');
                $rs_array = $query->result_array();
                
                foreach ($rs_array as $val) {
                    
                    $total_bill_amt +=$val['bill_amt'];
                }
                
                foreach ($rs_array as $val) {
                    
                    $result[]=array('label'=>$val['project_name'], 'data'=>round(($val['bill_amt']/$total_bill_amt)*100),  'color'=> $coller_array[rand(0, 11)]);
                }
                return json_encode($result);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

}
