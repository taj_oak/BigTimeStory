<?php
class Client_update_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}

function select(){  
//data is retrive from this query  
$query = $this->db->get('tms_client');  
return $query;  
} 
// Function To Fetch Selected Record
function show_client_id($data){
$this->db->select('*');
$this->db->from('tms_client');
$this->db->where('client_id', $data);
$query = $this->db->get();
$result = $query->result();
return $result;
}
// Update Query For Selected Record
function update_client_id($id,$data){
$this->db->where('client_id', $id);
$this->db->update('tms_client', $data);
}
}