<?php
class Vendor_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}
function createVendor($data){
        $this->db->insert('tms_vendor', $data);
        
}
function getAllVendor(){   
         $this->db->select('*');
	 $this->db->from('tms_vendor');	
         $this->db->where("cmp_id=$_SESSION[user_id]");
	 $query = $this->db->get();
	 return $query->result();
} 
function delete_vendor_id($id){
	 $this->db->where('vendor_id', $id);
	 $this->db->delete('tms_vendor');
}
function update_vendor($id,$data){
		$this->db->where('vendor_id', $id);
		$this->db->update('tms_vendor', $data);
	}	
function getVendorDetailsById($id){
		$this->db->select('*');
		$this->db->from('tms_vendor');	
		$this->db->where('vendor_id =',$id);
		$query = $this->db->get();
		return $query->row();
	}

}