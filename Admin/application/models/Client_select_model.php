<?php  
   class Client_select_model extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct(); 	
      }  
      //we will use the select function  
      function select()  
      {  
         //data is retrive from this query  
         $query = $this->db->get('tms_client');  
         return $query;  
      } 

      // Function to Delete selected record from table name students.
      function delete_client_id($id){
	 $this->db->where('client_id', $id);
	 $this->db->delete('tms_client');
      }	
   }  