<?php

class Expense_model extends CI_Model {

        function __construct() {
            parent::__construct();
            $this->load->database();
            $this->table_name='tms_expense_cat';
        }

        function addCategory($data){
		$data['date_created'] = date('Y-m-d H:i:s');
                $data['who_created']=$_SESSION['user_id'];
                $data['cmp_id']=$_SESSION['user_id'];
                //print_r($data);     
		$this->db->insert('tms_expense_cat', $data);
	}
	
	function getCategoryById($id){
		$this->db->select('*');
		$this->db->from($this->table_name);	
		$this->db->where('cat_id',$id);
		$query = $this->db->get();
		return $query->row();
                //echo $this->db->last_query();
                //die();
	}
	
	 function getAllCategory(){
		$this->db->select('*');
		$this->db->from($this->table_name);
                $this->db->where('cmp_id', $_SESSION['user_id']);
		$query = $this->db->get();               
		return $query->result();
               /* echo $this->db->last_query();
                die();*/
	}
        
        function deleteCategory($id){
            $this->db->where('cat_id', $id);
            $this->db->delete($this->table_name);
        }
        
        function updateCategory($id,$data){
                $data['date_updated'] = date('Y-m-d H:i:s');
                $data['who_updated']=$_SESSION['user_id'];      
		$this->db->where('cat_id', $id);
		$this->db->update($this->table_name, $data);
	}	
}

?>

