<?php
class Vendor_update_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}

function select(){  
//data is retrive from this query  
$query = $this->db->get('tms_vendor');  
return $query;  
} 
// Function To Fetch Selected Record
function show_vendor_id($data){
$this->db->select('*');
$this->db->from('tms_vendor');
$this->db->where('vendor_id', $data);
$query = $this->db->get();
$result = $query->result();
return $result;
}
// Update Query For Selected Record
function update_vendor_id($id,$data){
$this->db->where('vendor_id', $id);
$this->db->update('tms_vendor', $data);
}
}