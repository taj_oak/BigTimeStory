<?php
class Manager_detailView_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}
 
function getAllProjectforManager($id){   
         $this->db->select('*');
	 $this->db->from('tms_project');
         $this->db->join('users','tms_project.manager_id=users.id');
         $this->db->join('tms_client','tms_project.client_id=tms_client.client_id');
         $this->db->where('users.id', $id);
	 $query = $this->db->get();
	 return $query->result();
}
function getAllEmployeeforManager($id){   
         $this->db->select('*');
	 $this->db->from('users');
         $this->db->join('tms_project_employee','tms_project_employee.employee_id=users.id');
         $this->db->join('tms_project','tms_project.project_id=tms_project_employee.project_id');
         $this->db->where('tms_project.manager_id', $id);
	 $query = $this->db->get();
	 return $query->result();
}
}