<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */

class GroupModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table_name='users';
    }
    
    function invite_employee($data) {
        $this->db->insert('tms_client', $data);
    }
    function is_email_available($email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->or_where('LOWER(new_email)=', strtolower($email));

		$query = $this->db->get('users');
		return $query->num_rows() == 0;
	}
        
        function AddGroup($data,$data1)
	{
		
		if ($this->db->insert('tms_teams', $data)) {
			$team_id = $this->db->insert_id();
                        $data1['team_id']=$team_id;
                        foreach ($data1['user_id'] as $user_id) {
                            $data2['user_id']=$user_id;
                            $data2['team_id']=$team_id;
                             $this->db->insert('tms_team_member', $data2);
                        }
                       
                        
			return array('user_id' => $user_id);
		}
		return NULL;
	}
	
	function getteamsList()
	{
		$this->db->select('*');
		$this->db->from('tms_teams');	
		$this->db->where('team_mngr_id =', $_SESSION['user_id']);
		$query = $this->db->get();
		return $query->result();
	}
	
      function getAllUserByTeam($team_id)
	{
         
		$this->db->select('*');
		$this->db->from('tms_team_member');
                $this->db->join('tms_employee','tms_employee.user_id = tms_team_member.user_id');
		//$this->db->where(array('cmp_id'=>$_SESSION['user_id'] ,'role !='=> 'admin'));
              $this->db->where("tms_team_member.team_id = $team_id");
		$query = $this->db->get();
              return $query->result();
	}
	 function getAllUser()
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where('role !=', 'admin');
		$query = $this->db->get();
		return $query->result();
	}

}