<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class TimesheetModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('employeeModel');
        $this->table_name = 'users';
    }

    function getalluserbyManager($select = '*') {
       $u_id = $_SESSION['user_id'];
     $query=   $this->db->query("SELECT $select
FROM  `tms_project_employee` AS tpe
JOIN tms_project AS tp ON tpe.project_id = tp.project_id
WHERE tp.who_created = $u_id 
GROUP BY  employee_id");
        //$this->db->where('role !=', 'admin');
        //$query = $this->db->get();
        return $query->result_array();
    }

    function getallProjectbyManager() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->where('who_created =', $_SESSION['user_id']);
        $query = $this->db->get();
        return $query->result();
    }

    function getAllCategory() {

        $query = $this->db->query("select * from tms_categoris as tc join tms_project as tp on tp.project_id = tc.project_id where tp.who_created =$_SESSION[user_id]");
        return $query->result();
    }

    function getallExpensebyManager() {
        $this->db->select('*');
        $this->db->from('tms_time_sheet_approval');
        $this->db->join('tms_employee', 'tms_time_sheet_approval.submitted_by =tms_employee.user_id ');
        $this->db->where('action_by =', $_SESSION['user_id']);
        $query = $this->db->get();

        return $query->result();
        // echo $this->db->last_query();
    }

    function getallunapprovedExpensebyManager() {
        $this->db->select('*');
        $this->db->from('tms_time_sheet_approval');
        $this->db->join('tms_employee', 'tms_time_sheet_approval.submitted_by =tms_employee.user_id ');
        $this->db->where(array('action_by' => $_SESSION['user_id'], 'status' => 'Requested'));
        $query = $this->db->get();
        return $query->result();
    }

    function getexpenseDetails($approval_id) {
        //echo "SELECT * FROM `tms_time_sheet`  join tms_project on tms_project.project_id=tms_time_sheet.project_id  join tms_employee on tms_employee.user_id=tms_time_sheet.user_id where time_sheet_id in(select time_sheet_id from tms_time_sheet_approval where approval_id=$approval_id)";
        $query_timesheet = $this->db->query("select expense_id from tms_time_sheet_approval where approval_id=$approval_id");
        $timesheetIds = str_replace('#', ',', $query_timesheet->row()->expense_id);
        $query = $this->db->query("SELECT * FROM `tms_time_sheet`  join tms_project on tms_project.project_id=tms_time_sheet.project_id  join tms_employee on tms_employee.user_id=tms_time_sheet.submitted_by where time_sheet_id in($timesheetIds)");


        return $query->result();
    }

    function getAllUserById($id) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $this->db->where('id =', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAllUser() {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

    function getProjectwiseData() {
        $la_user = $this->getalluserbyManager("`employee_id`" );
       // print_r($la_user);
        $inuser='';
        foreach ($la_user as $user) {
            $inuser .= $user['employee_id'].',';
        }
        $inuser = trim($inuser, ",") ;

        $la_today = date('Y-m-d');
        $ls_30days = date('Y-m-d h:', strtotime("-30 days"));
        
       
        
        $sql = "SELECT "
                . "sum(hours+overtime ) as hours ,"
                . " tp.project_name , te.project_id , tp.color, "
                . "(select sum(hours+overtime) FROM tms_time_sheet where user_id in ($inuser) and start_time BETWEEN '$ls_30days' AND '$la_today') as totalhours "
                . "FROM tms_time_sheet as te JOIN tms_project as tp on tp.project_id = te.project_id  "
                . "where user_id in ($inuser) ";
         if (isset($_REQUEST['status']) && $_REQUEST['status'] != '' ) {
                $sat_str = stripslashes(implode($_REQUEST['status'], "','"));
                 $sql .= " and te.status in('$sat_str')";
            }
        if (isset($_REQUEST['get_projects']) && $_REQUEST['get_projects'] != '') {
            $sql .= " and te.project_id = ".$_REQUEST['get_projects'];
        }
        if (isset($_REQUEST['projects']) && $_REQUEST['projects'] != '') {
            $prj_str = stripslashes(implode($_REQUEST['projects'], "','"));
            $sql .= " and te.project_id in('$prj_str')";
        }
        if (isset($_REQUEST['category']) && $_REQUEST['category'] != '') {
            $cat_str = stripslashes(implode($_REQUEST['category'], "','"));
            $sql .= " and te.project_category_id in('$cat_str')";
        }
        
        $sql .= " and start_time BETWEEN '$ls_30days' AND '$la_today'  GROUP BY project_id";
        
        $la_result = $this->db->query($sql);
        //  echo $this->db->last_query();
        $la_data = array();
        $i = 0;
        foreach ($la_result->result() as $row) {
            $la_data[$i]['label'] = $row->project_name;
            $la_data[$i]['data'] = ($row->hours / $row->totalhours) * 100;
            $la_data[$i]['color'] = $row->color;
            $i++;
        }
        return ($la_data);
    }

    function getCatwiseData() {
        $la_user = $this->getalluserbyManager("`employee_id`" );
       // print_r($la_user);
        $inuser='';
        foreach ($la_user as $user) {
            $inuser .= $user['employee_id'].',';
        }
        $inuser = trim($inuser, ",") ;
        $la_today = date('Y-m-d');
        $ls_30days = date('Y-m-d', strtotime("-30 days"));
        
        $sql = "SELECT sum(hours+overtime) as hours ,tc.cat_name , tc.cat_id , te.color, "
                . "(select sum(hours+overtime) FROM tms_time_sheet where user_id in ($inuser) and start_time BETWEEN '$ls_30days' AND '$la_today') as totaHours "
                . "FROM tms_time_sheet as te JOIN tms_categoris as tc on tc.cat_id = te.project_category_id "
                . "where te.user_id in ($inuser) ";
        if (isset($_REQUEST['status']) && $_REQUEST['status'] != '' ) {
                $sat_str = stripslashes(implode($_REQUEST['status'], "','"));
                 $sql .= " and te.status in('$sat_str')";
            }
        if (isset($_REQUEST['get_projects']) && $_REQUEST['get_projects'] != '') {
            $sql .= " and te.project_id = ".$_REQUEST['get_projects'];
        }
        if (isset($_REQUEST['projects']) && $_REQUEST['projects'] != '') {
            $prj_str = stripslashes(implode($_REQUEST['projects'], "','"));
            $sql .= " and te.project_id in('$prj_str')";
        }
        if (isset($_REQUEST['category']) && $_REQUEST['category'] != '') {
            $cat_str = stripslashes(implode($_REQUEST['category'], "','"));
            $sql .= " and te.project_category_id in('$cat_str')";
        }
        $sql .= " and te.start_time BETWEEN '$ls_30days' AND '$la_today'  GROUP BY te.project_category_id";
        
        $la_result = $this->db->query($sql);

        $la_data = array();
        $i = 0;
        foreach ($la_result->result() as $row) {
            $la_data[$i]['label'] = $row->cat_name;
            $la_data[$i]['data'] = ($row->hours / $row->totaHours) * 100;
            $la_data[$i]['color'] = $row->color;
            $i++;
        }
        return ($la_data);
    }

    function getbarchartData() {
         $la_user = $this->getalluserbyManager("`employee_id`" );
       // print_r($la_user);
        $inuser='';
        foreach ($la_user as $user) {
            $inuser .= $user['employee_id'].',';
        }
        $inuser = trim($inuser, ",") ;
        $la_today = date('Y-m-d');
        $ls_30days = date('Y-m-d', strtotime("-30 days"));
        $sql = "SELECT sum(hours+overtime) as hours, weekNumber, start_time, project_name , te.project_id , tp.color, "
                . "(select sum(hours+overtime) FROM tms_time_sheet where user_id in ($inuser) and start_time BETWEEN '$ls_30days' AND '$la_today') as totalHours "
                . "FROM tms_time_sheet as te JOIN tms_project as tp on tp.project_id = te.project_id  "
                . "where user_id in ($inuser) ";
        if (isset($_REQUEST['status']) && $_REQUEST['status'] != '' ) {
                $sat_str = stripslashes(implode($_REQUEST['status'], "','"));
                 $sql .= " and te.status in('$sat_str')";
            }
        if (isset($_REQUEST['get_projects']) && $_REQUEST['get_projects'] != '') {
            $sql .= " and te.project_id = ".$_REQUEST['get_projects'];
        }
        if (isset($_REQUEST['projects']) && $_REQUEST['projects'] != '') {
            $prj_str = stripslashes(implode($_REQUEST['projects'], "','"));
            $sql .= " and te.project_id in('$prj_str')";
        }
        if (isset($_REQUEST['category']) && $_REQUEST['category'] != '') {
            $cat_str = stripslashes(implode($_REQUEST['category'], "','"));
            $sql .= " and te.project_category_id in('$cat_str')";
        }
        $sql .= " and start_time BETWEEN '$ls_30days' AND '$la_today'  GROUP BY weekNumber";
        $la_result = $this->db->query($sql);
        $la_data;
        $i = 0;
        $la_data['datasets'] = array();
        foreach ($la_result->result() as $row) {
            $la_data['labels'][$i] = "W $row->weekNumber " . implode($this->getStartAndEndDate($row->weekNumber, $row->start_time), ' to ');

            //$array[]=$row->hours;
            $la_data['datasets'][] = array(
                'label' => $row->project_name,
                'fillColor' => $row->color,
                'strokeColor' => $row->color,
                'pointColor' => $row->color,
                'pointStrokeColor' => $row->color,
                'pointHighlightFill' => $row->color,
                'pointHighlightStroke' => $row->color,
                'data' => array($row->hours),
            );
            $i++;
        }
        return ($la_data);
    }

//    function in_array_r($needle, $haystack, $strict = false) {
//    foreach ($haystack as $item) {
//        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
//            return true;
//        }
//    }
//
//    return false;
//}
    function getStartAndEndDate($week, $year) {

        $time = strtotime("1 January $year", time());
        $day = date('w', $time);
        $time += ((7 * $week) + 1 - $day) * 24 * 3600;
        $return[0] = date('d M ', $time);
        $time += 6 * 24 * 3600;
        $return[1] = date('d M ', $time);
        return $return;
    }

}