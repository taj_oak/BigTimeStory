<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class UserModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table_name = 'users';
        $this->tms_company = 'tms_company';
    }

    function create_company($data) {
        $this->db->where(array('user_id'=>$_SESSION['user_id']));
        $st =  $this->db->update($this->tms_company, $data);
     //   echo $this->db->last_query();die;
        if ($st) {
          
        $_SESSION['username'] =$data['company_name'];
           $_SESSION['logo']=$data['company_logo'];
            return array('user_id' => $_SESSION['user_id']);
        }
        return NULL;
    }

}