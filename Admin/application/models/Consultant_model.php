<?php
class Consultant_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}
function getAllEmployee(){
		$this->db->select('*');
		$this->db->from('users');	
		$this->db->where('role =', 'user');
		$query = $this->db->get();
		return $query->result();
}
        
function getAllVendor(){
		$this->db->select('*');
		$this->db->from('tms_vendor');	
		$query = $this->db->get();
		return $query->result();
}


function addConsultant($id,$data){
    $this->db->where('user_id', $id);
    $this->db->update('tms_employee', $data);

}
        
function getAllConsultant(){   
         $this->db->select('*');
	 $this->db->from('tms_employee');
         $this->db->join('users','tms_employee.user_id=users.id');
         $this->db->join('tms_vendor','tms_employee.vendor_id=tms_vendor.vendor_id');
	 $query = $this->db->get();
	 return $query->result();
} 

function delete_consultant($id,$data){
	 $this->db->where('user_id', $id);
	 $this->db->update('tms_employee', $data);
}

function getConsultantDetailsById($id){
		$this->db->select('*');
		$this->db->from('tms_employee');	
		$this->db->where('user_id =',$id);
		$query = $this->db->get();
		return $query->row();
}
        
}