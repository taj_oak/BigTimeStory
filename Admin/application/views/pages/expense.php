<script>
   function viewM(){
     if ($("#showM").prop('checked')) {
         $('#mileage').show();
     } else {
         $('#mileage').hide();
     }
   }
</script>
<?PHP
$chk='';
?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Expense Section 
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New Category</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?= base_url('expense_ctrl/createExpenseCategory') ?>" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Category Name</label><?php echo form_error('catname'); ?>
                      <input class="form-control" type="text" name="catname" value="<?php if(isset($selectedCat->cat_name)){ echo $selectedCat->cat_name; } else { echo ''; }?>" placeholder="Category Name">
                    </div>
                      
                    <div class="form-group">
                        <?php
                          if(isset($selectedCat->mileage) && $selectedCat->mileage>0){ 
                              $chk= "checked" ;
                              $style="display:block;";
                          }else{
                             $style="display:none;"; 
                          }
                        ?>
                        <label for="exampleInputEmail1"> <input type="checkbox" name="showM" id="showM" onclick="javascript:viewM();" <?=$chk?> > Mileage</label>
                      <input class="form-control" type="text" id="mileage" name="mileage" value="<?php if(isset($selectedCat->mileage)){ echo $selectedCat->mileage; } else { echo ''; }?>" placeholder="Mileage" style=<?=$style?>>
                    </div>
                      
                    <div class="form-group">
                    <label>Pick a Color</label>
                    <div class="input-group my-colorpicker2">
                      <input type="text" class="form-control" name="ccolor" value="<?php if(isset($selectedCat->color)){ echo $selectedCat->color; } else { echo ''; }?>">
                      <div class="input-group-addon"
                        <i></i>
                      </div>
                    </div>
                  </div>
                      
                  <div class="form-group">
                      <label>Status</label><?php echo form_error('cstatus'); ?>
                       <select name= "cstatus" class="form-control">
                          <option value=""></option>
                          <?php
                            $allstatus = array("Active","Inactive");                        
                            foreach($allstatus as $cstatus){
                                 if($cstatus == $selectedCat->status){
                                    $selected = " selected=selected";
                                }else{
                                   $selected = ""; 
                                }
                          ?>
                           <option <?=$selected?> value="<?php echo $cstatus; ?>"><?php echo $cstatus; ?></option>
                          <?php
                            }
                          ?>
                       </select>
                    </div>      
                                   
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <input type="hidden" id="hide" name="catid" value="<?php if(isset($selectedCat->cat_id)){ echo $selectedCat->cat_id; } else { echo ''; }?>">

                    </div>
                </form>
              </div><!-- /.box -->

            </div>
            </div>
            <!-- right column -->
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Category List</h3>
                 
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                      <?php 
                                   
                           foreach ($allcategory as $category) {  
                                      // print_r($client);
                      ?>               
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?php echo $category->cat_name?></span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"><?php echo $category->status?></i></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">  
                        <i class="fa fa-edit"><a href="<?= base_url('expense_ctrl/updateExpenseCategory/'.$category->cat_id) ?>"> Edit</a></i>
                        <i class="fa fa-trash-o"><a href="<?= base_url('expense_ctrl/deleteExpenseCategory/'.$category->cat_id) ?>"> Delete</a></i>
                      </div>
                    </li>      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
 