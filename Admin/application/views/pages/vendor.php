   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Vendor Section 
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new Vendors</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?= base_url('vendor_ctrl/createVendor') ?>" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label><?php echo form_error('dname'); ?>
                      <input class="form-control" type="text" name="dname" value="<?php if(isset($single_vendor->vendor_name)){ echo $single_vendor->vendor_name; } else { echo ''; }?>" placeholder="Name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Address</label><?php echo form_error('daddress'); ?>
                      <textarea class="form-control" name="daddress" placeholder="Address"><?php if(isset($single_vendor->vendor_address)){ echo $single_vendor->vendor_address; } else { echo ''; }?></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label><?php echo form_error('dname'); ?>
                      <input class="form-control" type="text" name="demail" value="<?php if(isset($single_vendor->vendor_email_id)){ echo $single_vendor->vendor_email_id; } else { echo ''; }?>" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Per Hour Billing</label><?php echo form_error('dperhr'); ?>
                      <input class="form-control" type="text" name="dperhr" value="<?php if(isset($single_vendor->per_hour)){ echo $single_vendor->per_hour; } else { echo ''; }?>" placeholder="Per Hour">
                    </div>
                       <div class="form-group">
                      <label for="exampleInputEmail1">Skype Id</label><?php echo form_error('dskype'); ?>
                      <input class="form-control" type="text" name="dskype" value="<?php if(isset($single_vendor->skype_id)){ echo $single_vendor->skype_id; } else { echo ''; } ?>" placeholder="Skype Id">
                    </div>
                       <div class="form-group">
                      <label for="exampleInputEmail1">Mobile</label><?php echo form_error('dmobile'); ?>
                      <input class="form-control" type="text" name="dmobile" value="<?php if(isset($single_vendor->phone_number)){ echo $single_vendor->phone_number; } else { echo ''; }?>" placeholder='10 Digit Mobile No'>
                    </div>
                      
                       <div class="form-group">
                      <label for="exampleInputEmail1">Payment Cycle</label><?php echo form_error('dpcycle'); ?>
                      <input class="form-control" type="text" name="dpcycle" value="<?php if(isset($single_vendor->payment_cycle)){ echo $single_vendor->payment_cycle; } else { echo ''; } ?>" placeholder='Payment Cycle'>
                    </div>
                      
                      
                     
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <input type="hidden" id="hide" name="did" value="<?php if(isset($single_vendor->vendor_id)){ echo $single_vendor->vendor_id; } else { echo ''; }?>">
                  </div>
                </form>
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Vendor List</h3>
                  <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
				   <?php 
                                   
                                   foreach ($allvendors as $vendor) {  
                                      // print_r($vendor);
                                       ?>
                          
                    
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?php echo $vendor->vendor_name?></span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> <?php echo $vendor->vendor_email_id; ?></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"><a href="<?= base_url('vendor_ctrl/select_vendor_id/'.$vendor->vendor_id) ?>"> Edit</a></i>
                        <i class="fa fa-trash-o"><a href="<?= base_url('vendor_ctrl/delete_vendor_id/'.$vendor->vendor_id) ?>"> Delete</a></i>
                      </div>
                    </li>
      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->