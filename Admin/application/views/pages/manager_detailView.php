   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manager Detail View
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title"> Project Details</h3>
                  
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                      <?php 
                                   
                                   foreach ($allprojects as $project) {  
                                      // print_r($client);
                                       ?>
                          
                    
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- todo text -->
                      <span class="text"><?php echo $project->project_name?></span>
                      <span class="text"><?php echo $project->client_name?></span>
                      <span class="text"><?php echo $project->client_address?></span>
                      <span class="text"><?php echo $project->client_email_id?></span>>
                    </li>
      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
            <!-- right column -->
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Employee Details</h3>
                  
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                      <?php 
                                   
                                   foreach ($allemployees as $employee) {  
                                      // print_r($client);
                                       ?>
                          
                    
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- todo text -->
                      <span class="text"><?php echo $employee->username?></span> 
                      <span class="text"><?php echo $employee->email?></span>
                    </li>
      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->