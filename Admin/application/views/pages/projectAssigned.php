<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Project Section 
            <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add new Assigned Projects</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="<?= base_url('projectAssigned_ctrl/createAssignedProject') ?>" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Project</label>
                                <select name="projectId" class="form-control select2">
                                    <?php
                                    foreach ($allprojects as $project) {
                                        if ($project->project_id == $single_projectAssigned->project_id) {
                                            $selected = " selected=selected";
                                        } else {
                                            $selected = "";
                                        }
                                        ?>
                                        <option <?= $selected ?> value="<?php echo $project->project_id; ?>"><?php echo $project->project_name; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>                    
                            <div class="form-group">
                                <label for="exampleInputEmail1">Employee</label>
                                <select class="form-control select2" multiple="multiple" data-placeholder="Select Employee/Counsult" name="pemployeeId[]">
                                    <option value=""></option>
                                    <?php
                                    foreach ($allemployees as $employee) {
                                        if ($employee->id == $single_projectAssigned->employee_id) {
                                            $selected = " selected=selected";
                                        } else {
                                            $selected = "";
                                        }
                                        ?>
                                        <option <?= $selected ?> value="<?php echo $employee->id; ?>"><?php echo $employee->username; ?> (<?php echo $employee->email; ?>)</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>


                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <input type="hidden" id="hide" name="did" value="<?php
                            if (isset($single_projectAssigned->id)) {
                                echo $single_projectAssigned->id;
                            } else {
                                echo '';
                            }
                            ?>">

                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title">Assigned Project List</h3>
                        <!--                  <div class="box-tools pull-right">
                                            <ul class="pagination pagination-sm inline">
                                              <li><a href="#">&laquo;</a></li>
                                              <li><a href="#">1</a></li>
                                              <li><a href="#">2</a></li>
                                              <li><a href="#">3</a></li>
                                              <li><a href="#">&raquo;</a></li>
                                            </ul>
                                          </div>-->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php $i = 0; foreach ($allprojects as $project) { 
                                
                            
                                ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?= $i ?>">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>" aria-expanded="<?php echo ($i == 0 ?  'true' :  'false'); ?>" aria-controls="collapse<?= $i ?>">
                                           <?php echo $project->project_name; ?>
                                        </a>
                                        <span class="label label-primary pull-right"><?= count($project->who_created) ?></span>
                                    </h4>
                                </div>
                                <div id="collapse<?=$i?>" class="panel-collapse <?php echo ($i == 0 ?  'collapsed in' :  'collapsed'); ?>" role="tabpanel" aria-labelledby="heading<?=$i?>">
                                    <div class="panel-body">
                                        <ul  class="todo-list">
                                           <?php foreach ($project->who_created as $emp) { ?>
                                                                                
                                                                           
                                            <li>
                                                <span><?= $emp->emp_name ?></span> <small class="alert-danger"><?= $emp->email ?></small> 
                                               
                                                <div class="tools">
                        <i class="fa fa-trash-o"><a href="<?= base_url('projectAssigned_ctrl/deleteAssignedProject').'/'.$emp->user_id.'/'.$emp->project_id  ?>"> Delete</a></i>
                      </div> 
                                            </li>
                                           <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php $i++ ; } ?>


                        </div>

                        <!--                   <ul class="todo-list" style="display:none;">
                        <?php
                        foreach ($allVendorAssignedProjects as $projectAssignedVendor) {
                            // print_r($client);
                            ?>
                                                          
                                                    <li class="box-title">For Vendor</li>
                                                    <li>
                                                       drag handle 
                                                      <span class="handle">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                        <i class="fa fa-ellipsis-v"></i>
                                                      </span>
                                                       checkbox 
                                                      <input type="checkbox" value="" name="">
                                                       todo text 
                                                      <span class="text"><?php echo $projectAssignedVendor->project_name ?></span>
                                                       Emphasis label 
                                                      <small class="label label-danger"><i class="fa fa-clock-o"><?php echo $projectAssignedVendor->current_status ?></i></small>
                                                       General tools such as edit or delete
                                                      <div class="tools">
                                                        <i class="fa fa-edit"><a href="<?= base_url('projectAssigned_ctrl/select_assigned_project_id/' . $projectAssignedVendor->id . '/vendor') ?>"> Edit</a></i>
                                                        <i class="fa fa-trash-o"><a href="<?= base_url('projectAssigned_ctrl/deleteAssignedProject/' . $projectAssignedVendor->id . '/vendor') ?>"> Delete</a></i>
                                                      </div>
                                                    </li>
                                      
<?php } ?>
                                          </ul> -->
                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->