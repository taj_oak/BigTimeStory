 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Employee
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Employee</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new Employee</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form action="<?= base_url('employee/invite') ?>" method="POST">
                    <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label>
                      <input required="" class="form-control" type="text" name="ename" value="<?php if(isset($single_project->project_name)){ echo $single_project->project_name; } else { echo ''; }?>" placeholder="employee name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email Id</label>
                        <input required="" class="form-control" type="email" placeholder="employee email id" name="email"  value='<?php if(isset($user->email)){ echo $user->email; } else { echo ''; }?>'/>
                        <p style="color: red"><?php if(isset($error)){ echo $error; } ?></p>
                    </div>
                    <div class="row">
                    <div class="col-lg-6">
                      <div class="input-group">
                        <span class="input-group-addon">
                            <input  type="radio"  value="user" name='role'>
                        </span>
                          <input type="text" readonly="" value="Employee" class="form-control">
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input type="radio" value="manager" name='role'>
                        </span>
                        <input type="text" readonly="" value="Manager"  class="form-control">
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                  </div>
                    
                    </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Employee Added List</h3>
                  <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
				   <?php foreach ($allusers as $user) {   ?>
                          
                    
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?= $user->email ?></span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> <?= $user->role ?></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                          <?php
                            if($user->role == "manager"){
                          ?>
                            <i class="fa fa-files-o"><a href="<?=base_url('manager_detailView_ctrl/showDeails/'.$user->id); ?>">View</a></i>
                          <?php
                            }
                          ?>
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div>
                    </li>
      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->