   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Project Section 
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new Projects</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?= base_url('project_ctrl/createProject') ?>" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label><?php echo form_error('pname'); ?>
                      <input class="form-control" type="text" name="pname" value="<?php if(isset($single_project->project_name)){ echo $single_project->project_name; } else { echo ''; }?>" placeholder="Name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Manager</label>
                      <select name="pmanagerId" class="form-control" required="">
                          <?php
                            foreach($allmanagers as $manager){
                                if($manager->id == $single_project->manager_id){
                                    $selected = " selected=selected";
                                }else{
                                   $selected = ""; 
                                }
                              ?>
                                <option <?=$selected?> value="<?php echo $manager->id; ?>"><?php echo $manager->username; ?> (<?php echo $manager->email; ?>)</option>
                          <?php
                            }
                          ?>
                      </select>
                    </div>
                      <div class="form-group">
                      <label for="exampleInputEmail1">Client</label>
                      <select name= "pclientId" class="form-control" required="">
                          <?php
                            foreach($allclients as $client){
                                if($client->client_id == $single_project->client_id){
                                    $selected = " selected=selected";
                                }else{
                                   $selected = ""; 
                                }
                              ?>
                                <option <?=$selected?> value="<?php echo $client->client_id; ?>"><?php echo $client->client_name; ?> (<?php echo $client->client_email_id; ?>)</option>
                          <?php
                            }
                          ?>
                      </select>
                    </div>
                        
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <input type="hidden" id="hide" name="did" value="<?php if(isset($single_project->project_id)){ echo $single_project->project_id; } else { echo ''; }?>">

                    </div>
                </form>
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Project List</h3>
                  <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                      <?php 
                                   
                                   foreach ($allprojects as $project) {  
                                      // print_r($client);
                                       ?>
                          
                    
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?php echo $project->project_name?></span>
                      <span class="text"><?php echo $project->client_name?>(<?php echo $project->client_email_id?>)</span>
                      <span class="text"><?php echo $project->username?>(<?php echo $project->email?>)</span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-files-o"><a href="<?=base_url('project_detailView_ctrl/showDeails/'.$project->project_id); ?>">View</a></i>  
                        <i class="fa fa-edit"><a href="<?= base_url('project_ctrl/select_project_id/'.$project->project_id) ?>"> Edit</a></i>
                        <i class="fa fa-trash-o"><a href="<?= base_url('project_ctrl/deleteProject/'.$project->project_id) ?>"> Delete</a></i>
                      </div>
                    </li>
      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->