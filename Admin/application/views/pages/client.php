   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Client Section 
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new Clients</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?= base_url('client_ctrl/createClient') ?>" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label><?php echo form_error('dname'); ?>
                      <input class="form-control" type="text" name="dname" value="<?php if(isset($single_client->client_name)){ echo $single_client->client_name; } else { echo ''; }?>" placeholder="Name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Address</label><?php echo form_error('daddress'); ?>
                      <textarea class="form-control" name="daddress" placeholder="Address"><?php if(isset($single_client->client_address)){ echo $single_client->client_address; } else { echo ''; }?></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label><?php echo form_error('dname'); ?>
                      <input class="form-control" type="text" name="demail" value="<?php if(isset($single_client->client_email_id)){ echo $single_client->client_email_id; } else { echo ''; }?>" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Per Hour Billing</label><?php echo form_error('dperhr'); ?>
                      <input class="form-control" type="text" name="dperhr" value="<?php if(isset($single_client->per_hour)){ echo $single_client->per_hour; } else { echo ''; }?>" placeholder="Per Hour">
                    </div>
                       <div class="form-group">
                      <label for="exampleInputEmail1">Skype Id</label><?php echo form_error('dskype'); ?>
                      <input class="form-control" type="text" name="dskype" value="<?php if(isset($single_client->skype_id)){ echo $single_client->skype_id; } else { echo ''; } ?>" placeholder="Skype Id">
                    </div>
                       <div class="form-group">
                      <label for="exampleInputEmail1">Mobile</label><?php echo form_error('dmobile'); ?>
                      <input class="form-control" type="text" name="dmobile" value="<?php if(isset($single_client->phone_number)){ echo $single_client->phone_number; } else { echo ''; }?>" placeholder='10 Digit Mobile No'>
                    </div>
                      
                       <div class="form-group">
                      <label for="exampleInputEmail1">Payment Cycle</label><?php echo form_error('dpcycle'); ?>
                      <input class="form-control" type="text" name="dpcycle" value="<?php if(isset($single_client->payment_cycle)){ echo $single_client->payment_cycle; } else { echo ''; } ?>" placeholder='Payment Cycle'>
                    </div>
                      
                      
                     
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <input type="hidden" id="hide" name="did" value="<?php if(isset($single_client->client_id)){ echo $single_client->client_id; } else { echo ''; }?>">
                  </div>
                </form>
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Client List</h3>
<!--                  <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div>-->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
				   <?php 
                                   
                                   foreach ($allclients as $client) {  
                                      // print_r($client);
                                       ?>
                          
                    
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?php echo $client->client_name?></span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i> <?php echo $client->client_email_id; ?></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"><a href="<?= base_url('client_ctrl/select_client_id/'.$client->client_id) ?>"> Edit</a></i>
                        <i class="fa fa-trash-o"><a href="<?= base_url('client_ctrl/delete_client_id/'.$client->client_id) ?>"> Delete</a></i>
                      </div>
                    </li>
      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->
             
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->