   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Project Section 
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Consultant</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?= base_url('consultant_ctrl/createConsultant') ?>" method="POST">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Vendor</label>
                      <select name= "pvendorId" class="form-control">
                          <option value=""></option>
                          <?php
                            foreach($allvendors as $vendor){
                                if($vendor->vendor_id == $single_consultant->vendor_id){
                                    $selected = " selected=selected";
                                }else{
                                   $selected = ""; 
                                }
                              ?>
                                <option <?=$selected?> value="<?php echo $vendor->vendor_id; ?>"><?php echo $vendor->vendor_name; ?> (<?php echo $vendor->vendor_email_id; ?>)</option>
                          <?php
                            }
                          ?>
                      </select>
                    </div>                                            
                    <div class="form-group">
                      <label for="exampleInputEmail1">Employee</label>
                      <select name="pemployeeId" class="form-control">
                          <option value=""></option>
                          <?php
                            foreach($allemployees as $employee){
                                if($employee->id == $single_consultant->user_id){
                                    $selected = " selected=selected";
                                }else{
                                   $selected = ""; 
                                }
                              ?>
                                <option <?=$selected?> value="<?php echo $employee->id; ?>"><?php echo $employee->username; ?> (<?php echo $employee->email; ?>)</option>
                          <?php
                            }
                          ?>
                      </select>
                    </div>  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Consultant List</h3>
                  <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                      <?php 
                                   
                                   foreach ($allconsultant as $consultant) {  
                                      // print_r($client);
                                       ?>
                          
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?php echo $consultant->username?></span>
                      <span class="text"><?php echo $consultant->email?></span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"><?php echo $consultant->vendor_name?></i></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                       <!--<i class="fa fa-edit"><a href="<?= base_url('consultant_ctrl/selectConsultantById/'.$consultant->user_id) ?>"> Edit</a></i>-->
                        <i class="fa fa-trash-o"><a href="<?= base_url('consultant_ctrl/deleteConsultant/'.$consultant->user_id) ?>"> Delete</a></i>
                      </div>
                    </li>
      
                    <?php } ?>
                  </ul>
                  
                   <ul class="todo-list" style="display:none;">
                      <?php 
                                   
                                   foreach ($allVendorAssignedProjects as $projectAssignedVendor) {  
                                      // print_r($client);
                                       ?>
                          
                    <li class="box-title">For Vendor</li>
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?php echo $projectAssignedVendor->project_name?></span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"><?php echo $projectAssignedVendor->current_status?></i></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <i class="fa fa-edit"><a href="<?= base_url('projectAssigned_ctrl/select_assigned_project_id/'.$projectAssignedVendor->id.'/vendor') ?>"> Edit</a></i>
                        <i class="fa fa-trash-o"><a href="<?= base_url('projectAssigned_ctrl/deleteAssignedProject/'.$projectAssignedVendor->id.'/vendor') ?>"> Delete</a></i>
                      </div>
                    </li>
      
                    <?php } ?>
                  </ul> 
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->