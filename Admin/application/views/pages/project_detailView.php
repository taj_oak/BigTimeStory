<?php
$this->load->helper("url");
echo $project_id = $this->uri->segment(3);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Project Detail View 
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title"> Client Details</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <ul class="todo-list">
                            <?php
                            foreach ($allclients as $client) {
                                // print_r($client);
                                ?>


                                <li>
                                    <!-- drag handle -->
                                    <span class="handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <!-- todo text -->
                                    <span class="text"><?php echo $client->client_name ?></span>
                                    <span class="text"><?php echo $client->client_address ?></span>
                                    <span class="text"><?php echo $client->client_email_id ?></span>>
                                </li>

<?php } ?>
                        </ul>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
            <!-- right column -->
            <div class="col-md-6">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title">Manager Details</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <ul class="todo-list">
<?php
foreach ($allmanagers as $manager) {
    // print_r($client);
    ?>


                                <li>
                                    <!-- drag handle -->
                                    <span class="handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <!-- todo text -->
                                    <span class="text"><?php echo $manager->username ?></span> 
                                    <span class="text"><?php echo $manager->email ?></span>
                                </li>

<?php } ?>
                        </ul>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->

        <div class="row">
            <div class="col-md-6">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title"> Vendor Details</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <ul class="todo-list">
<?php
foreach ($allvendors as $vendor) {
    // print_r($client);
    ?>


                                <li>
                                    <!-- drag handle -->
                                    <span class="handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <!-- todo text -->
                                    <span class="text"><?php echo $vendor->vendor_name ?></span>
                                    <span class="text"><?php echo $vendor->vendor_address ?></span>
                                    <span class="text"><?php echo $vendor->vendor_email_id ?></span>
                                </li>

<?php } ?>
                        </ul>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
            <!-- right column -->
            <div class="col-md-6">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title">Employee Details</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <ul class="todo-list">
<?php
foreach ($allemployees as $employee) {
    // print_r($client);
    ?>


                                <li>
                                    <!-- drag handle -->
                                    <span class="handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <!-- todo text -->
                                    <span class="text"><?php echo $employee->username ?></span> 
                                    <span class="text"><?php echo $employee->email ?></span>
                                    <span class="text"><?php echo $employee->current_status ?></span>
                                </li>

<?php } ?>
                        </ul>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
            <div class="col-md-6">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title">Category List</h3>
                        <i type="button" data-toggle="modal" data-target="#addcategory"class="pull-right fa fa-plus-square" title="Add category"></i>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <ul class="todo-list">
<?php
if (count($allcat > 0)) {
    foreach ($allcat as $cat) {
        // print_r($client);
        ?>


                                    <li>
                                        <!-- drag handle -->

                                        <!-- todo text -->
                                        <span class="text"><?php echo $cat->cat_name ?></span> 

                                    </li>

    <?php }
} else { ?>
                                <li>
                                    <!-- drag handle -->

                                    <!-- todo text -->
                                    <span class="text">No Category Added <i type="button" data-toggle="modal" data-target="#addcategory"class="pull-right fa fa-plus-square" title="Add category"></i> to add</span> 

                                </li>


<?php } ?>
                        </ul>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->