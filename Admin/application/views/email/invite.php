<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
       <div style="margin:0;background-color:white;color:#aaaaaa;font-family:Arial,Helvetica,sans-serif;font-size:12px">
    <br>
    
    <table cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td bgcolor="#ffffff" style="padding:20px 0">
            <table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;text-align:left;margin:0 auto;border-collapse:collapse;text-align:left;margin:0 auto" width="630">
              <tbody>
                <tr>
                  <td bgcolor="#ffffff" style="padding:5px;border:1px #aaaaaa solid">
                    
                    <table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;margin:0 auto;text-align:left" width="620">
                      <tbody>
                        <tr>
                          <td bgcolor="#ffffff" style="padding:20px 20px;font-family:Arial,Helvetica,sans-serif;font-size:30px;line-height:20pt;color:#444444;font-weight:lighter">
                            <img alt="Pyxists" src="" style="border:0;display:block" class="CToWUd">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    
                    <table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;margin:0 auto;text-align:left;color:#444" width="620">
                      <tbody>
                        <tr>
                          <td bgcolor="#ffffff" style="padding:20px;padding-top:10px;line-height:15pt">
                            
<h2>Dear <a href="mailto:" target="_blank"></a>, </h2>
<br>
 has invited you Pyxists.com.
<br>
To confirm this invitation, please click here:
<a href="" target="_blank"></a>
<br>
<br>
Pyxists – time tracking so easy you'll actually use it! Access it in your browser, your desktop and mobile. 
<br>
<br>
Best wishes,<br>
Pyxists team<br>
<a href="mailto:support@Pyxists.com" target="_blank">support@Pyxists.com</a>

                          </td>
                        </tr>
                      </tbody>
                    </table>
                    
                    <table align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;margin:0 auto;text-align:left" width="620">
                      <tbody>
                        <tr>
                          <td bgcolor="#444444" height="2" style="padding:0 0 15px 0;line-height:0">
                          <img alt="" height="2" src="https://ci6.googleusercontent.com/proxy/p6CiXpvR7nyONWXVD6Yg0IP1OdVVevgbYpwbIJuT_9467KOtgFq5-mf0QrZ5qAtaPQsmthbOtEJtXTPwbA4a0B2b=s0-d-e1-ft#https://assets.Pyxists.com/images/email/hr.png" width="620" style="border:0" class="CToWUd">
                          </td>
                        </tr>
                        <tr>
                          <td bgcolor="#444444" style="padding:0px 20px 15px 20px;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:15pt;color:#aaaaaa">
                            Copyright © 2015 Pyxists OÜ.<br>
                            <a href="http://www.Pyxists.com" style="color:#aaaaaa;text-decoration:underline" target="_blank">www.Pyxists.com</a> | &nbsp;Here’s how to get the quickest resolution for your support request: <a href="http://blog.Pyxists.com/2014/11/get-best-Pyxists-support/" style="color:#aaaaaa;text-decoration:underline" target="_blank">http://blog.Pyxists.com/2014/11/<wbr>get-best-Pyxists-support/</a><br>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table><div class="yj6qo"></div><div class="adL">
    <br>
  </div></div>
    </body>
</html>
