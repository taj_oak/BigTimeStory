<!-- jQuery 2.1.4 -->

<script src="<?= base_url() ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?= base_url() ?>js/admin.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?= base_url() ?>bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url() ?>plugins/select2/select2.full.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= base_url() ?>plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url() ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?= base_url() ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url() ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url() ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?= base_url() ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= base_url() ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url() ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= base_url() ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script>
    $().ready(function () {
        //alert(baseurl) 
    });
</script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
    $(".select2").select2();
</script>
<?php

if (trim($_SESSION['username']) == '') {
    ?>
    <script type="text/javascript">
        $(window).load(function () {
            $('#myModal').modal('show');
        });
    </script>
    
    <?php $this->load->view('common/popupwiz') ?>
    <?php
}
?> 
<!-- start Model -->

<!-- Category Modal -->
<div class="modal fade" id="addcategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Category</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?=  base_url().'project_detailView_ctrl/savecat'?>" role="form" method="POST" >
                    <?php if(isset( $_SESSION['error'])){ ?>
                    <div class="alert-danger"> <?= $_SESSION['error'] ?></div>
                    <?php unset($_SESSION['error']); } ?>
                   
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"
                                for="inputEmail3">Category Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="cat_name" class="form-control" required="" title="Please enter the category name"
                                    placeholder="Company name"/>
                            <input type="hidden" name="project_id" value="<?=$project_id?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
           
        </div>
    </div>
</div>
<!-- end Model -->
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>dist/js/demo.js"></script>
<script src="<?= base_url() ?>dist/js/pages/dashboard2.js"></script>
</body>
</html>