<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script>
    $(function () {
        
        /*
         * BAR CHART
         * ---------
         */
        
        <?php 
            if(isset($time_sheet_chart) && $time_sheet_chart != '') {
                $bar_data = $time_sheet_chart;
            } else {
                $bar_data = '';
            }
            
            if(isset($expense_chart) && $expense_chart != '') {
                $bar_data_expense = $expense_chart;
            } else {
                $bar_data_expense = '';
            }
            
            
        ?>
        
        var bar_data = {
            data: [<?=$bar_data?>],
            color: "#3c8dbc"
        };
        $.plot("#bar-chart", [bar_data], {
            grid: {
                borderWidth: 1,
                borderColor: "#f3f3f3",
                tickColor: "#f3f3f3"
            },
            series: {
                bars: {
                    show: true,
                    barWidth: 0.5,
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        });
        
        var bar_data_expense = {
            data: [<?=$bar_data_expense?>],
            color: "#3c8dbc"
        };
        $.plot("#bar-chart1", [bar_data_expense], {
            grid: {
                borderWidth: 1,
                borderColor: "#f3f3f3",
                tickColor: "#f3f3f3"
            },
            series: {
                bars: {
                    show: true,
                    barWidth: 0.5,
                    align: "center"
                }
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        });
        /* END BAR CHART */

        /*
         * DONUT CHART
         * -----------
         */
        <?php 
            if(isset($time_sheet_piechart) && $time_sheet_piechart != '') {
                $total_time_pie_data = $time_sheet_piechart;
                $total_time_pie_data = str_replace('"label"', 'label', $total_time_pie_data);
                $total_time_pie_data = str_replace('"data"', 'data', $total_time_pie_data);
                $total_time_pie_data = str_replace('"color"', 'color', $total_time_pie_data);
            } else {
                $total_time_pie_data = json_encode(array());
            }
        ?>
        
        var timepiedata = <?=$total_time_pie_data?>;
        $.plot("#donut-chart1", timepiedata, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        
        
        <?php 
            if(isset($time_sheet_approvedchart) && $time_sheet_approvedchart != '') {
                $timeApprovedData = $time_sheet_approvedchart;
                $timeApprovedData = str_replace('"label"', 'label', $timeApprovedData);
                $timeApprovedData = str_replace('"data"', 'data', $timeApprovedData);
                $timeApprovedData = str_replace('"color"', 'color', $timeApprovedData);
            } else {
                $timeApprovedData = json_encode(array());
            }
        ?>
        
         var timeApprovedData = <?=$timeApprovedData?>;
        $.plot("#donut-chart2", timeApprovedData, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        
          
        
        
        <?php 
            if(isset($expense_sheet_approvedchart) && $expense_sheet_approvedchart != '') {
                $expense_sheet_approvedchart = str_replace('"label"', 'label', $expense_sheet_approvedchart);
                $expense_sheet_approvedchart = str_replace('"data"', 'data', $expense_sheet_approvedchart);
                $expense_sheet_approvedchart = str_replace('"color"', 'color', $expense_sheet_approvedchart);
            } else {
                $expense_sheet_approvedchart = json_encode(array());
            }
        ?>
        var expense_sheet_approvedchart = <?=$expense_sheet_approvedchart?> ;
        $.plot("#donut-chart4", expense_sheet_approvedchart, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        
        
        
        
        <?php 
            if(isset($expense_sheet_piechart) && $expense_sheet_piechart != '') {
                $expense_sheet_piechart = str_replace('"label"', 'label', $expense_sheet_piechart);
                $expense_sheet_piechart = str_replace('"data"', 'data', $expense_sheet_piechart);
                $expense_sheet_piechart = str_replace('"color"', 'color', $expense_sheet_piechart);
            } else {
                $expense_sheet_piechart = json_encode(array());
            }
        ?>
        var expense_sheet_piechart = <?=$expense_sheet_piechart?> ;
        $.plot("#donut-chart3", expense_sheet_piechart, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        
        
        <?php 
            if(isset($time_sheet_pendingchart) && $time_sheet_pendingchart != '') {
                $time_sheet_pendingchart = str_replace('"label"', 'label', $time_sheet_pendingchart);
                $time_sheet_pendingchart = str_replace('"data"', 'data', $time_sheet_pendingchart);
                $time_sheet_pendingchart = str_replace('"color"', 'color', $time_sheet_pendingchart);
            } else {
                $time_sheet_pendingchart = json_encode(array());
            }
        ?>
        var time_sheet_pendingchart = <?=$time_sheet_pendingchart?> ;
        $.plot("#donut-chart11", time_sheet_pendingchart, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        
        
        
        <?php 
            if(isset($time_sheet_rejectedchart) && $time_sheet_rejectedchart != '') {
                $time_sheet_rejectedchart = str_replace('"label"', 'label', $time_sheet_rejectedchart);
                $time_sheet_rejectedchart = str_replace('"data"', 'data', $time_sheet_rejectedchart);
                $time_sheet_rejectedchart = str_replace('"color"', 'color', $time_sheet_rejectedchart);
            } else {
                $time_sheet_rejectedchart = json_encode(array());
            }
        ?>
        var time_sheet_rejectedchart = <?=$time_sheet_rejectedchart?>;
        $.plot("#donut-chart21", time_sheet_rejectedchart, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        
        
        <?php 
            if(isset($expense_sheet_rejectedchart) && $expense_sheet_rejectedchart != '') {
                $expense_sheet_rejectedchart = str_replace('"label"', 'label', $expense_sheet_rejectedchart);
                $expense_sheet_rejectedchart = str_replace('"data"', 'data', $expense_sheet_rejectedchart);
                $expense_sheet_rejectedchart = str_replace('"color"', 'color', $expense_sheet_rejectedchart);
            } else {
                $expense_sheet_rejectedchart = json_encode(array());
            }
        ?>
        var expense_sheet_rejectedchart = <?=$expense_sheet_rejectedchart?>;
        $.plot("#donut-chart41", expense_sheet_rejectedchart, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        
        
        <?php 
            if(isset($expense_sheet_pendingchart) && $expense_sheet_pendingchart != '') {
                $expense_sheet_pendingchart = str_replace('"label"', 'label', $expense_sheet_pendingchart);
                $expense_sheet_pendingchart = str_replace('"data"', 'data', $expense_sheet_pendingchart);
                $expense_sheet_pendingchart = str_replace('"color"', 'color', $expense_sheet_pendingchart);
            } else {
                $expense_sheet_pendingchart = json_encode(array());
            }
        ?>
        var expense_sheet_pendingchart = <?=$expense_sheet_pendingchart?> ;
        $.plot("#donut-chart31", expense_sheet_pendingchart, {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 2 / 3,
                formatter: labelFormatter,
                threshold: 0.1
              }

            }
          },
          legend: {
            show: false
          }
        });
        /*
         * END DONUT CHART
         */

    });

    /*
     * Custom Label formatter
     * ----------------------
     */
    function labelFormatter(label, series) {
        return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                + label
                + "<br>"
                + Math.round(series.percent) + "%</div>";
    }
</script>
<script>
    $().ready(function () {
        //alert(baseurl);
        $(".my-colorpicker2").colorpicker();
    });
</script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
    $(".select2").select2();
</script>
<?php
if (trim($_SESSION['username']) == '') {
    ?>
    <script type="text/javascript">
        $(window).load(function () {
            $('#myModal').modal('show');
        });
    </script>
    <?php
}
?> 
<!-- start Model -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">My Profile</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?= base_url() . 'userProfile/save' ?>" role="form" method="POST" enctype="multipart/form-data">
                    <?php if (isset($_SESSION['error'])) { ?>
                        <div class="alert-danger"> <?= $_SESSION['error'] ?></div>
                        <?php unset($_SESSION['error']);
                    } ?>

                    <div class="form-group">
                        <label   class="col-sm-3 control-label">Select File</label>
                        <div class="col-sm-9">
                            <input name="file" type="file" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"
                                for="inputEmail3">Company Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="company_name" class="form-control" 
                                   placeholder="Company name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                               for="inputPassword3" >Company Address</label>
                        <div class="col-sm-9">
                            <input type="text" name="company_address" class="form-control"
                                   placeholder="Address"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                               for="inputPassword3" >Company Tel</label>
                        <div class="col-sm-9">
                            <input type="tel" name="company_tel" class="form-control"
                                   id="inputPassword3" placeholder="Company Tel"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                               for="inputPassword3" >About Company</label>
                        <div class="col-sm-9">
                            <textarea type="text" name="company_desc" class="form-control"
                                      id="inputPassword3" placeholder="About Company "></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- end Model -->
<!-- Category Modal -->
<div class="modal fade" id="addcategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Category</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?= base_url() . 'project_detailView_ctrl/savecat' ?>" role="form" method="POST" >
                    <?php if (isset($_SESSION['error'])) { ?>
                        <div class="alert-danger"> <?= $_SESSION['error'] ?></div>
                        <?php unset($_SESSION['error']);
                    } ?>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"
                                for="inputEmail3">Category Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="cat_name" class="form-control" required="" title="Please enter the category name"
                                   placeholder="Company name"/>
                            <input type="hidden" name="project_id" value="<?//= $project_id ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- end Model -->
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>dist/js/demo.js"></script>
<script src="<?= base_url() ?>dist/js/pages/dashboard2.js"></script>
</body>
</html>