/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
   //console.log(chart);
  //  alert(chart);
 // var baseurl = 'http://localhost:8081/BigTimeStory/';
  var baseurl = 'http://dev.urplace.in/';
    $('#donut-chart').text('loading...');
    $.ajax({
        "url": baseurl+'Manager/expense/getallProjectExpanse/',
        "type": "POST",
        "data": JSON.parse(chart),
        success: function (result) {
            
             $('#donut-chart').text('');
             var data =  JSON.parse(result);
           var donutData = data.project;
           var catData =  data.cat;
           console.log(donutData);
           console.log(JSON.parse(result));
            $.plot("#donut-chart", donutData, {
                series: {
                    pie: {
                        show: true,
                        radius: 1,
                        innerRadius: 0,
                        label: {
                            show: true,
                            radius: 1 / 2,
                            formatter: labelFormatter,
                            // threshold: 0.1,
                            background: {
                                opacity: 0.8
                            }
                        }

                    }
                },
                legend: {
                    show: true
                }
            });
            $.plot("#donut-cat", catData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                innerRadius: 0,
                label: {
                    show: true,
                    radius: 1 / 2,
                    formatter: labelFormatter,
                    background: {
                        opacity: 0.8
                    }
                }

            }
        },
        legend: {
            show: true
        }
    });
            
        }
    });
$.ajax({
        url: baseurl+'Manager/expense/getallProjectExpanseBar/',
        type: "POST",
        data: {},
        success: function (result) {
            var areaChartData = JSON.parse(result)
//        var areaChartDataWeek = {
//        labels: ["Week1", "Week2", "Week3", "Week4"],
//        datasets: [
//            {
//                label: "Electronics",
//                fillColor: "blue",
//                strokeColor: "rgba(210, 214, 222, 1)",
//                pointColor: "rgba(210, 214, 222, 1)",
//                pointStrokeColor: "#c1c7d1",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(220,220,220,1)",
//                data: [65, 59, 80, 81]
//            },
//            {
//                label: "Digital Goods",
//                fillColor: "rgba(60,141,188,0.9)",
//                strokeColor: "rgba(60,141,188,0.8)",
//                pointColor: "#3b8bba",
//                pointStrokeColor: "rgba(60,141,188,1)",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(60,141,188,1)",
//                data: [28, 48, 40, 19]
//            },
//            {
//                label: "Timesheet",
//                fillColor: "rgba(255,0,0,0.9)",
//                strokeColor: "rgba(60,141,188,0.8)",
//                pointColor: "#3b8bba",
//                pointStrokeColor: "rgba(60,141,188,1)",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(60,141,188,1)",
//                data: [200, 300, 40, 19]
//            }
//        ]
//    };
//    var areaChartDataDay = {
//        labels: ["Mon", "Tue", "Wed", "Thu", 'Fri', 'Sat', 'Sun'],
//        datasets: [
//            {
//                label: "Electronics",
//                fillColor: "blue",
//                strokeColor: "rgba(210, 214, 222, 1)",
//                pointColor: "rgba(210, 214, 222, 1)",
//                pointStrokeColor: "#c1c7d1",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(220,220,220,1)",
//                data: [65, 59, 80, 81, 56, 55, 40]
//            },
//            {
//                label: "Digital Goods",
//                fillColor: "rgba(60,141,188,0.9)",
//                strokeColor: "rgba(60,141,188,0.8)",
//                pointColor: "#3b8bba",
//                pointStrokeColor: "rgba(60,141,188,1)",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(60,141,188,1)",
//                data: [28, 48, 40, 19, 86, 27, 90]
//            },
//            {
//                label: "Timesheet",
//                fillColor: "rgba(255,0,0,0.9)",
//                strokeColor: "rgba(60,141,188,0.8)",
//                pointColor: "#3b8bba",
//                pointStrokeColor: "rgba(60,141,188,1)",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(60,141,188,1)",
//                data: [200, 300, 40, 19, 400, 27, 90]
//            }
//        ]
//    };
//    var areaChartDataMonth = {
//        labels: ["January", "February", "March", "April", "May", "June", "July", 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
//        datasets: [
//            {
//                label: "Electronics",
//                fillColor: "blue",
//                strokeColor: "rgba(210, 214, 222, 1)",
//                pointColor: "rgba(210, 214, 222, 1)",
//                pointStrokeColor: "#c1c7d1",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(220,220,220,1)",
//                data: [65, 59, 80, 81, 56, 55, 40]
//            },
//            {
//                label: "Digital Goods",
//                fillColor: "rgba(60,141,188,0.9)",
//                strokeColor: "rgba(60,141,188,0.8)",
//                pointColor: "#3b8bba",
//                pointStrokeColor: "rgba(60,141,188,1)",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(60,141,188,1)",
//                data: [28, 48, 40, 19, 86, 27, 90]
//            },
//            {
//                label: "Timesheet",
//                fillColor: "rgba(255,0,0,0.9)",
//                strokeColor: "rgba(60,141,188,0.8)",
//                pointColor: "#3b8bba",
//                pointStrokeColor: "rgba(60,141,188,1)",
//                pointHighlightFill: "#fff",
//                pointHighlightStroke: "rgba(60,141,188,1)",
//                data: [200, 300, 40, 19, 400, 27, 90]
//            }
//        ]
//    };
    var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: false,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - Whether the line is curved between points
        bezierCurve: true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: false,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true
    };
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
   // var barChartData = areaChartDataMonth;
      var barChartData = areaChartData.project;
    //var barChartData = areaChartDataDay;
//    barChartData.datasets[1].fillColor = "#00a65a";
//    barChartData.datasets[1].strokeColor = "#00a65a";
//    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
        }
    });
    
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY')+'<input type="hidden" name="startdate" value="'+start.format('YYYY-MM-DD')+'"/>'+'<input type="hidden" name="enddate" value="'+end.format('YYYY-MM-DD')+'"/>');
        
    }
    cb(moment().subtract(29, 'days'), moment());

    $('#reportrange').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Last Three Month': [moment().subtract(0, 'month').startOf('month'), moment().subtract(3, 'month').endOf('month')]
        }
    }, cb);

    $("#project").select2({
        placeholder: "Select project"
    });
    $("#category").select2({
        placeholder: "Select Category"
    });
    $("#status").select2({
        placeholder: "Select status"
    });
    $('#showchart').click(function () {
        var button = $('#showchart').text();
        if (button == 'Hide Chart') {
            $('#showchart').text('Show Chart');
        } else {
            $('#showchart').text('Hide Chart');
        }
        var options = {};
        $("#chartlisting").toggle('clip', options, 500);
    })


    
});


function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
            + "<br>"
            + Math.round(series.percent) + "%</div>";
}