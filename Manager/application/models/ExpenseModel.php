<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class ExpenseModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('employeeModel');
        $this->table_name = 'users';
    }

    function getalluserbyManager($select = '*') {
       
     $query=   $this->db->query("SELECT $select
FROM  `tms_project_employee` AS tpe
JOIN tms_project AS tp ON tpe.project_id = tp.project_id
WHERE tp.manager_id = $_SESSION[user_id]
GROUP BY  employee_id");
        //$this->db->where('role !=', 'admin');
      //  $query = $this->db->get();
        return $query->result_array();
    }

    function getallProjectbyManager() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->where('manager_id =', $_SESSION['user_id']);
        $query = $this->db->get();
        return $query->result();
    }

    function getAllCategory() {

        $query = $this->db->query("select distinct cat_id,cat_name,color,mileage from tms_expense_cat,users where users.cmp_id=tms_expense_cat.cmp_id and tms_expense_cat.status='Active' and users.id=" . $_SESSION['user_id']);
        return $query->result();
    }

    function getallExpensebyManager() {
        $this->db->select('*');
        $this->db->from('tms_expense_approval');
        $this->db->join('tms_employee', 'tms_expense_approval.submitted_by =tms_employee.user_id ');
        $this->db->where('action_by =', $_SESSION['user_id']);
        $query = $this->db->get();

        return $query->result();
        // echo $this->db->last_query();
    }

    function getallunapprovedExpensebyManager() {
        $this->db->select('*');
        $this->db->from('tms_expense_approval');
        $this->db->join('tms_employee', 'tms_expense_approval.submitted_by =tms_employee.user_id ');
        $this->db->where(array('action_by' => $_SESSION['user_id'], 'status' => 'Requested'));
        $query = $this->db->get();
        return $query->result();
    }

    function getexpenseDetails($approval_id) {
        //echo "SELECT * FROM `tms_expense`  join tms_project on tms_project.project_id=tms_expense.project_id  join tms_employee on tms_employee.user_id=tms_expense.user_id where time_sheet_id in(select time_sheet_id from tms_expense_approval where approval_id=$approval_id)";
        $query_timesheet = $this->db->query("select expense_id from tms_expense_approval where approval_id=$approval_id");
        $timesheetIds = str_replace('#', ',', $query_timesheet->row()->expense_id);
        $query = $this->db->query("SELECT * FROM `tms_expense` join tms_expense_cat on tms_expense_cat.cat_id = `tms_expense`.exp_catid  join tms_project on tms_project.project_id=tms_expense.project_id  join tms_employee on tms_employee.user_id=tms_expense.submitted_by where exp_id in($timesheetIds)");


        return $query->result();
    }

    function getAllUserById($id) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $this->db->where('id =', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAllUser() {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

    function getProjectwiseData() {
        $la_user = $this->getalluserbyManager("`employee_id`" );
       // print_r($la_user);
        $inuser='';
        foreach ($la_user as $user) {
            $inuser .= $user['employee_id'].',';
        }
        $inuser = trim($inuser, ",") ;

        $la_today = date('Y-m-d');
        $ls_30days = date('Y-m-d', strtotime("-30 days"));
        $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by in ($inuser) and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by in ($inuser) and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY project_id");
        //  echo $this->db->last_query();
        $la_data = array();
        $i = 0;
        foreach ($la_result->result() as $row) {
            $la_data[$i]['label'] = $row->project_name;
            $la_data[$i]['data'] = ($row->billamount / $row->totalBillamount) * 100;
            $la_data[$i]['color'] = $row->color;
            $i++;
        }
        return ($la_data);
    }

    function getCatwiseData() {
        $la_user = $this->getalluserbyManager("`employee_id`" );
       // print_r($la_user);
        $inuser='';
        foreach ($la_user as $user) {
            $inuser .= $user['employee_id'].',';
        }
        $inuser = trim($inuser, ",") ;
        $la_today = date('Y-m-d');
        $ls_30days = date('Y-m-d', strtotime("-30 days"));
        $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount ,cat_name , tc.cat_id , tc.color,(select sum(bill_amt) FROM tms_expense where submitted_by in ($inuser) and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_expense_cat as tc on tc.cat_id = te.exp_catid  where submitted_by in ($inuser) and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY exp_catid");

        $la_data = array();
        $i = 0;
        foreach ($la_result->result() as $row) {
            $la_data[$i]['label'] = $row->cat_name;
            $la_data[$i]['data'] = ($row->billamount / $row->totalBillamount) * 100;
            $la_data[$i]['color'] = $row->color;
            $i++;
        }
        return ($la_data);
    }

    function getbarchartData() {
         $la_user = $this->getalluserbyManager("`employee_id`" );
       // print_r($la_user);
        $inuser='';
        foreach ($la_user as $user) {
            $inuser .= $user['employee_id'].',';
        }
        $inuser = trim($inuser, ",") ;
        $la_today = date('Y-m-d');
        $ls_30days = date('Y-m-d', strtotime("-30 days"));
        $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount,weekno,year ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by in ($inuser) and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by in ($inuser) and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY weekno");
        $la_data;
        $i = 0;
        $la_data['datasets'] = array();
        foreach ($la_result->result() as $row) {
            $la_data['labels'][$i] = "W $row->weekno " . implode($this->getStartAndEndDate($row->weekno, $row->year), ' to ');

            //$array[]=$row->billamount;
            $la_data['datasets'][] = array(
                'label' => $row->project_name,
                'fillColor' => $row->color,
                'strokeColor' => $row->color,
                'pointColor' => $row->color,
                'pointStrokeColor' => $row->color,
                'pointHighlightFill' => $row->color,
                'pointHighlightStroke' => $row->color,
                'data' => array($row->billamount),
            );
            $i++;
        }
        return ($la_data);
    }

//    function in_array_r($needle, $haystack, $strict = false) {
//    foreach ($haystack as $item) {
//        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
//            return true;
//        }
//    }
//
//    return false;
//}
    function getStartAndEndDate($week, $year) {

        $time = strtotime("1 January $year", time());
        $day = date('w', $time);
        $time += ((7 * $week) + 1 - $day) * 24 * 3600;
        $return[0] = date('d M ', $time);
        $time += 6 * 24 * 3600;
        $return[1] = date('d M ', $time);
        return $return;
    }

}

?>