<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of admin
 *
 * @author SKM
 */
class Admin extends CI_Controller {
    
//       public function createJob($details) {
//        $query = $this->db->insert('jobs_available', array('skills' => $details['skills'], 'name_of_post' => $details['name_of_post'], 'experience_required' => $details['experience_required'], 'job_location' => $details['job_location'] ,
//         'job_location' => $details['job_location'],'expected_salary' => $details['expected_salary'], 'job_category' => $details['job_category'],'job_description' => $details['job_description'],'date_of_job_posted' => date('Y-m-d') ));
//       // $postId = $this->db->get_where('posts', array('postTitle' => $details['postTitle'], 'postContent' => $details['postContent']));
//     
//    }
     public function createJob($details) {
        $query = $this->db->insert('jobs_available',$details);
       // $postId = $this->db->get_where('posts', array('postTitle' => $details['postTitle'], 'postContent' => $details['postContent']));
    }
    
     public function editJob($id ,$details) {
        $this->db->where('id', $id);
        $this->db->update('jobs_available', $details); 
//        $query = $this->db->update('jobs_available', array('skills' => $details['skills'], 'name_of_post' => $details['name_of_post'], 'experience_required' => $details['experience_required'], 'job_location' => $details['job_location'] ,
//         'job_location' => $details['job_location'],'expected_salary' => $details['expected_salary'], 'job_category' => $details['job_category'],'job_description' => $details['job_description'],'date_of_job_posted' => date('Y-m-d') ));
       // $postId = $this->db->get_where('posts', array('postTitle' => $details['postTitle'], 'postContent' => $details['postContent']));
     
    }
    public function deletedJob($id) {
	         $this->db->where('id', $id);
	         $this->db->update('jobs_available', array('status' => "1"));
	    }
    public function getAllcandidate() {
                    $this->db->select('*');
                    $this->db->distinct('lastName');
                    $this->db->from('candidate');
                    $this->db->join('jobs_available', 'jobs_available.id = candidate.openingId');
                    $query = $this->db->get();
         if($query->num_rows()>0){
             return $query->result();
         }else{
             return Null;
         }
    }
    public function createBlog($details) {
        $query = $this->db->insert('blog',$details);
           }
    
     public function editBlog($id ,$details) {
        $this->db->where('id', $id);
        $this->db->update('blog', $details); 
    }
    public function deletedBlog($id) {
	         $this->db->where('id', $id);
	         $this->db->update('blog', array('status' => "1"));
	    }
     public function allBlog(){
          $query =  $this->db->query('SELECT * FROM blog where status= 0 order by id DESC');
//         print_r($query->result());
        return $query->result();
    }
    
      public function allPages(){
          $query =  $this->db->query('SELECT * FROM pages where status= 0 order by id DESC');
//         print_r($query->result());
        return $query->result();
    }
}
