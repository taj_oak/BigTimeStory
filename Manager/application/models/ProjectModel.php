<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */

class ProjectModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table_name='users';
    }
    
    function invite_employee($data) {
        $this->db->insert('tms_client', $data);
    }
    function is_email_available($email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->or_where('LOWER(new_email)=', strtolower($email));

		$query = $this->db->get('users');
		return $query->num_rows() == 0;
	}
        
        function create_user($data, $activated = TRUE)
	{
		$data['created'] = date('Y-m-d H:i:s');
		$data['activated'] = $activated ? 1 : 0;

		if ($this->db->insert($this->table_name, $data)) {
			$user_id = $this->db->insert_id();
			return array('user_id' => $user_id);
		}
		return NULL;
	}
	   function update_user($id, $data, $activated = TRUE)
	{
		$data['created'] = date('Y-m-d H:i:s');
		$data['activated'] = $activated ? 1 : 0;
		$this->db->where('id', $id);
		$this->db->update($this->table_name, $data);
	}	
	function getAllUserById($id)
	{
		$this->db->select('*');
		$this->db->from($this->table_name);	
		$this->db->where('role !=', 'admin');
		$this->db->where('id =',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	 function getAllUser()
	{
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->where('role !=', 'admin');
		$query = $this->db->get();
		return $query->result();
	}

}

?>