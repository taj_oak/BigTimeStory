<?php
class Vendor_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
$this->tms_project_vendor='tms_project_vendor';
$this->tms_project='tms_project';
}
function createVendor($data){
        $this->db->insert('tms_vendor', $data);
        
}
function getAllVendor(){   
         $this->db->select('*');
	 $this->db->from('tms_vendor');	
          $this->db->join($this->tms_project_vendor, 'tms_project_vendor.vendor_id = tms_vendor.vendor_id');
         $this->db->join($this->tms_project, 'tms_project.project_id = tms_project_vendor.project_id');
     //    $this->db->join($this->tms_project_vendor, 'tms_project_vendor.vendor_id = tms_vendor.vendor_id');
         $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id']));
         $query = $this->db->get();
	 return $query->result();
} 
function getVendorDetailsById($id){
		$this->db->select('*');
		$this->db->from('tms_vendor');	
		$this->db->where('vendor_id =',$id);
		$query = $this->db->get();
		return $query->row();
	}

}
?>