<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class EmployeeModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table_name = 'users';
        $this->tms_employee = 'tms_employee';
        $this->tms_project ='tms_project';
        $this->tms_project_employee='tms_project_employee';
    }

    
    function getAllUserById($id) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $this->db->where('id =', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function getAllClient() {
        
    }
    function getAllUser() {
       $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->join($this->tms_employee, 'tms_employee.user_id = users.id');
         $this->db->join($this->tms_project_employee, 'tms_project_employee.employee_id = users.id');
        $this->db->join($this->tms_project, 'tms_project.project_id = tms_project_employee.project_id');
       
        $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id'], 'role !=' => 'admin'));
        $this->db->order_by("users.id", "desc");
       // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
       // echo $this->db->last_query();
        return $query->result();
    }

    function getAllLettestUser() {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->join($this->tms_employee, 'tms_employee.user_id = users.id');
         $this->db->join($this->tms_project_employee, 'tms_project_employee.employee_id = users.id');
        $this->db->join($this->tms_project, 'tms_project.project_id = tms_project_employee.project_id');
       
        $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id'], 'role !=' => 'admin'));
        $this->db->order_by("users.id", "desc");
        $this->db->limit(10);
        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

    function getAllUsersId() {
        $this->db->select("group_concat(`users`.`id` separator ',') as users");
        $this->db->from($this->table_name);
        $this->db->join($this->tms_employee, 'tms_employee.user_id = users.id');
         $this->db->join($this->tms_project_employee, 'tms_project_employee.employee_id = users.id');
        $this->db->join($this->tms_project, 'tms_project.project_id = tms_project_employee.project_id');
       
        $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id'], 'role !=' => 'admin'));
        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->row();
    }

    function getalltimesheet() {
        $ids = $this->getAllUsersId()->users;
        // print_r($ids);die;
        $this->db->select("* ,  (UNIX_TIMESTAMP(end_time)- UNIX_TIMESTAMP(start_time))  as timesq ");
        $this->db->from('tms_time_sheet');
        $this->db->join($this->tms_employee, 'tms_employee.user_id = tms_time_sheet.user_id');
        $this->db->where_in('tms_time_sheet.user_id', $ids);
        $this->db->order_by("time_sheet_id", "desc");
        $this->db->limit(10);
        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

    function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            $hours = 00;
            $minutes = (00 % 60);
            return sprintf($format, $hours, $minutes);
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    function time_elapsed_string($ptime) {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return '0 seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

}

?>