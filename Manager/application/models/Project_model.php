<?php

class Project_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table_name = 'users';
        $this->tms_employee = 'tms_employee';
        $this->tms_project = 'tms_project';
        $this->tms_project_employee = 'tms_project_employee';
         $this->tms_client='tms_client';
    }

    function getAllClient() {
        $this->db->select('*');
        $this->db->from('tms_client');
        $this->db->join($this->tms_project, 'tms_project.client_id = tms_client.client_id');
        $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id']));
        $query = $this->db->get();
        return $query->result();
    }

    function addProject($data) {
        $data['date_created'] = date('Y-m-d');
        $data['who_created'] = $_SESSION['user_id'];
        $this->db->insert('tms_project', $data);
    }

    function getAllProject() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->join('tms_client', 'tms_project.client_id=tms_client.client_id');
        $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id']));
        $query = $this->db->get();
        return $query->result();
    }

    function getAllLattestProject() {
        $this->db->select('*');
        $this->db->from('tms_project');
          $this->db->join($this->tms_client, 'tms_project.client_id = tms_client.client_id');
         $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id']));
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }
  function getProjectDetailsById($id) {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->where('project_id =', $id);
        $query = $this->db->get();
        return $query->row();
    }

}

?>