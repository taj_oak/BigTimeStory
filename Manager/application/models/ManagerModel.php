<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class ManagerModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
         $this->load->model('employeeModel');
        $this->table_name = 'users';
    }

    function getalluserbyManager() {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

    function getallProjectbyManager() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->where('manager_id =', $_SESSION['user_id']);
        $query = $this->db->get();
        return $query->result();
    }

    function getallTimesheetbyManager() {
        $this->db->select('*');
        $this->db->from('tms_time_sheet_approval');
        $this->db->join('tms_employee', 'tms_time_sheet_approval.submitted_by =tms_employee.user_id ');
        $this->db->where('action_by =', $_SESSION['user_id']);
        $query = $this->db->get();
      
        return $query->result();
       // echo $this->db->last_query();
    }
    
    function getallunapprovedTimesheetbyManager() {
        $this->db->select('*');
        $this->db->from('tms_time_sheet_approval');
        $this->db->join('tms_employee', 'tms_time_sheet_approval.submitted_by =tms_employee.user_id ');
        $this->db->where(array('action_by' => $_SESSION['user_id'],'status'=>'Requested'));
        $query = $this->db->get();
        return $query->result();
    }

    function gettimesheetDetails($approval_id) {
        //echo "SELECT * FROM `tms_time_sheet`  join tms_project on tms_project.project_id=tms_time_sheet.project_id  join tms_employee on tms_employee.user_id=tms_time_sheet.user_id where time_sheet_id in(select time_sheet_id from tms_time_sheet_approval where approval_id=$approval_id)";
      $query_timesheet = $this->db->query("select time_sheet_id from tms_time_sheet_approval where approval_id=$approval_id");
      $timesheetIds= str_replace('#', ',', $query_timesheet->row()->time_sheet_id) ;
        $query =$this->db->query("SELECT * , (UNIX_TIMESTAMP(end_time)- UNIX_TIMESTAMP(start_time))  as timesq FROM `tms_time_sheet`  join tms_project on tms_project.project_id=tms_time_sheet.project_id  join tms_employee on tms_employee.user_id=tms_time_sheet.user_id where time_sheet_id in($timesheetIds)");
        
       
       return $query->result();
        
    }

    function getAllUserById($id) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $this->db->where('id =', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAllUser() {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

}

?>