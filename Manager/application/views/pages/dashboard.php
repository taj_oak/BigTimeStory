<?php
$this->load->model("employeeModel");
?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Version 2.0</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-stats-bars"></i></span>
                <a class="info-box-content" href="<?= base_url('dashboard/showprojects') ?>">
                  <span class="info-box-text">Total Project</span>
                  <span class="info-box-number"><?=$totalcount['totalproject']?><small></small></span>
                </a><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-user-secret"></i></span>
                <a class="info-box-content" href="<?= base_url('dashboard/showclients') ?>">
                  <span class="info-box-text">Clients</span>
                  <span class="info-box-number"><?=$totalcount['totalclient']?></span>
                </a><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-building"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Vendor</span>
                  <span class="info-box-number"><?=$totalcount['totalvendor']?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Members</span>
                  <span class="info-box-number"><?=$totalcount['totalmembers']?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
              <!-- MAP & BOX PANE -->
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest TimeSheet Update</h3>
                 
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Employee Name</th>
                          <th>Task</th>
                          <th>Hours Worked</th>
                          <th>date</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($latestTime as $time) { ?>
                              
                         
                        <tr>
                          <td><?= $time->emp_name ?></td>
                          <td><?= $time->task_name ?></td>
                          <td><?=  $this->employeeModel->convertToHoursMins(($time->timesq/60), '%02d hours %02d minutes'); ?></td>
                          <td><?php  $time = strtotime($time->start_time);
                                   echo  date('l jS \of F Y',$time) ?></td>
                        </tr>
                          <?php }   ?>
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->

              </div>
              <!-- /.box -->
              <div class="row">

                <div class="col-md-6">
                  <!-- USERS LIST -->
                  <div class="box box-danger">
                    <div class="box-header with-border">
                      <h3 class="box-title">Latest Members</h3>
                      <div class="box-tools pull-right">
                          <span class="label label-danger"><?=  count($latestMembers) ?> New Members</span>
                       
                      </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">
                          <?php foreach ($latestMembers as $member) { ?>
                        <li>
                          <i class="fa fa-4x responsive fa-user"></i>
                          <a class="users-list-name"><?=$member->emp_name?></a>
                          <span class="users-list-date"><?= $this->employeeModel->time_elapsed_string(strtotime($member->created)) ?></span>
                        </li>
                          <?php } ?>
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                    <div class="box-footer text-center">
                      <!--<a href="javascript::" class="uppercase">View All Users</a>-->
                    </div><!-- /.box-footer -->
                  </div><!--/.box -->
                </div><!-- /.col -->
                <div class="col-md-6">
                    
                    <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Recently Added Projects</h3>
                  <div class="box-tools pull-right">
                        <span class="label label-danger"><?=  count($latestProjects) ?> New Projects</span>
                  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                        <?php foreach ($latestProjects as $project) { ?>
                      <li class="item" style="background-color: <?=$project->color?> ;">
                        
                          <div class="product-description">
                        <a href="javascript::;" class="product-title"><?= $project->project_name ?><span class="label label-warning pull-right"><?= $project->client_email_id ?></span></a>
                        <span class="product-description">
                         <?= $project->client_name .'<br /> <b> Address:</b> '.$project->client_address ?>
                        </span>
                      </div>
                    </li><!-- /.item -->
                        <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="javascript::;" class="uppercase">View All Products</a>
                </div><!-- /.box-footer -->
              </div>
                    
                </div>
              </div><!-- /.row -->

              <!-- TABLE: LATEST ORDERS -->
              <!-- /.box -->
            </div><!-- /.col -->

            <div class="col-md-4">
              <!-- Info Boxes Style 2 -->
              <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Inventory</span>
                  <span class="info-box-number">0</span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                    0% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Mentions</span>
                  <span class="info-box-number">0</span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                    20% Increase in 0 Days
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Downloads</span>
                  <span class="info-box-number">0</span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                    0% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Direct Messages</span>
                  <span class="info-box-number">0</span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 0%"></div>
                  </div>
                  <span class="progress-description">
                    0% Increase in 30 Days
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
              <!-- PRODUCT LIST -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
