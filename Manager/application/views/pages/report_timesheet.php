<!-- FLOT CHARTS -->
<?php
//print_r($_REQUEST);
if(isset($_REQUEST['startdate']) && isset($_REQUEST['enddate'])){
     $start = strtotime($_REQUEST['startdate']); // or your date as well
     $end_date = strtotime($_REQUEST['enddate']);
     $datediff = $end_date - $start;
      $ls_diffDays = floor($datediff/(60*60*24));
}else{
     $ls_diffDays='29';
}
$column="[]";
if(isset($_REQUEST['reporttype']) && $_REQUEST['reporttype']=='D') { 
    $column = " [
        { data: 'project_name' },
        { data: 'emp_name' },
        { data: 'start_time' },
        { data:  'task_name'},
        { data: 'hours' },
        { data: 'overtime' },
        { data: 'status' },
       
    ] ";
}else{
     $column = "[
        { data: 'project_name' },
        { data: 'weekNumber' },
        { data: 'totaltime' },
        { data: 'STATUS' }
    ] ";
}
//print_r($_REQUEST);die;
?>
<script type="text/javascript">
    <?php 
    if(isset($_REQUEST['reporttype']) && $_REQUEST['reporttype'] != '') {
        $chart_param = array(
            'projects' =>isset($_REQUEST['projects'])?$_REQUEST['projects'][0]:null,
            'weekno' =>isset($_REQUEST['weekno'])?$_REQUEST['weekno']:null,
            'reporttype' =>isset($_REQUEST['reporttype'])?$_REQUEST['reporttype']:null,
            'startdate' =>isset($_REQUEST['startdate'])?$_REQUEST['startdate']:null,
            'enddate' =>isset($_REQUEST['enddate'])?$_REQUEST['enddate']:null,
            'status' =>isset($_REQUEST['status'])?$_REQUEST['status']:null,
            'startdate' =>isset($_REQUEST['startdate'])?$_REQUEST['startdate']:null
        );
        $chart_param = json_encode($chart_param);
    } else {
        $chart_param = json_encode(array());
    }
?>
var chart = '<?=$chart_param?>';
//var chart = '<?//php echo (isset($_REQUEST['startdate']))?  json_encode($_REQUEST): '[]'; ?>';
//var chart = '<?//php echo json_encode($_REQUEST) ?>';
//chart =JSON.parse(chart);
console.log(chart);

var column = <?= $column ?>;
//alert(column);
//column =JSON.parse(column);
</script>
<script src="<?= base_url() ?>plugins/flot/jquery.flot.min.js"></script>
<script src="<?= base_url() ?>plugins/flot/jquery.flot.pie.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
<script src="<?= base_url() ?>plugins/chartjs/Chart.min.js"></script>

<script src="<?= base_url() ?>js/timesheet_report.js"></script>
<script src="<?= base_url() ?>js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.print.min.js"></script>
<link rel="<?= base_url() ?>css/style.css">
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
 <link rel="stylesheet"href="https://cdn.datatables.net/buttons/1.1.1/css/buttons.dataTables.min.css">
<script>
    $(document).ready(function () {
     //alert("Ss")
        $('#expansereport').DataTable({
        "processing": true,
        "serverSide": true,
        "dom": 'Bfrtip',
        "buttons": [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "ajax": {
               "url": '<?= base_url('timesheet/expanseReportTable'); ?>',
               "type": 'POST',
               "data": JSON.parse(chart)
           },
         "columns"  : column   
        
        });
        
    });
//     function getDetails(project_id,weekno,year){
//       var ajaxdata={"year":year,"weekno":weekno,"projects":[project_id],"reporttype":"D"};
//     }
//    function loaddatatables(data){
//    
//    $('#expansereport').DataTable({
//        "processing": true,
//        "serverSide": true,
//        "ajax": {
//               "url": '<?//= base_url('expanse/expanseReportTable'); ?>',
//               "type": 'POST',
//               "data": data;
//           },
//         "columns"  : column   
//        
//        });
//    }
//   
    

</script>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Time Sheet For Last 30 Days

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">expense</a></li>
            <li class="active">Report</li>
        </ol>
    </section>
    <section class="content">
        <div class="row" id="chartlisting">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-bar-chart-o"></i>
                        <h3 class="box-title">Time Sheet Chart</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div id="donut-chart" style="height: 200px;"></div>

                        </div>
                        <div class="col-md-6">
                            <div id="donut-cat" style="height: 200px;"></div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bar Chart</h3>
                        <!--                        <div class="box-tools pull-right">
                                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                                </div>-->
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="barChart" style="height:200px"></canvas>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <!--                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <i class="fa fa-angellist"></i>
                                        <h3 class="box-title">Short View</h3>
                                    </div>
                                    <div class="info-box col-lg-3">
                                        <span class="info-box-icon bg-blue"><i class="ion ion-speedometer"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Expense</span>
                                            <span class="info-box-number">
                                                $ 200.69
                                            </span></div><small></small>
                                         /.info-box-content 
                                    </div>
                                    <div class="info-box col-lg-3">
                                        <span class="info-box-icon bg-green"><i class="ion ion-cash"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Approve Expense</span>
                                            <span class="info-box-number">
                                                $ 160.69
                                            </span></div><small></small>
                                         /.info-box-content 
                                    </div>
                                    <div class="info-box col-lg-3">
                                        <span class="info-box-icon bg-red"><i class="ion ion-alert-circled"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Total Reject Expense</span>
                                            <span class="info-box-number">
                                                $ 40.00
                                            </span></div><small></small>
                                         /.info-box-content 
                                    </div>
                                </div>-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="<?=  base_url('timesheet/report')?>" method="POST">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="col-lg-3">

                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>

                        </div>
                        <div class="col-lg-3">
                            <div class="col-md-6">
                            <select class="select2 form-control" id="status"  name="status[]" multiple="">
                                <option value="Pending">Pending</option>
                                <option value="Approved">Approved</option>
                                <option value="Reject">Reject</option>
                            </select>
                            </div>
                            <div class="col-md-6">
                            <select class="select2 form-control" id="project"  name="projects[]" multiple="">
                               <?php foreach ($allprojects as $project) { ?>
                                <option  <?php if(isset($_REQUEST['projects'])){  echo (in_array($project->project_id, $_REQUEST['projects'])) ? 'selected':''; } ?> value="<?=$project->project_id?>"><?=$project->project_name?></option>
                                <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="col-lg-5">
                            <select class="select2 form-control" id="category" name="category[]" multiple="">
                              <?php foreach ($allcategory as $cat) { ?>
                              <option  <?php if(isset($_REQUEST['category'])){ echo (in_array($cat->cat_id, $_REQUEST['category'])) ? 'selected':''; }; ?>  value="<?=$cat->cat_id?>"><?=$cat->cat_name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-lg-7">
                            <label class="radio-inline"> <input type="radio" <?php   if(isset($_REQUEST['reporttype'])){  echo $_REQUEST['reporttype']=='D' ? 'checked' : '' ; } ?> name="reporttype"  value="D"/>Details </label>
                            <label class="radio-inline"> <input type="radio" name="reporttype"  <?php   if(isset($_REQUEST['reporttype'])){  echo $_REQUEST['reporttype']=='S' ? 'checked' : '' ; }else{ echo  'checked'; } ?>  value="S"/>Summary </label>

                        </div>
                    </div>
                        <div class="col-lg-2">
                            <button type="submit" class="btn btn-primary">Apply</button>
                            <span class="btn btn-primary" id="showchart">Hide Chart</span>
                        </div>
                    </div>
                    
                </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!--                    <div class="box-header">
                                            <h3 class="box-title">Expense View Details</h3>
                                                              <div class="box-tools">
                                                                <div class="input-group" style="width: 150px;">
                                                                  <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                                                                  <div class="input-group-btn">
                                                                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                                                  </div>
                                                                </div>
                                                              </div>
                                        </div> /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="expansereport" class="table table-hover table-bordered table-striped">
                            <thead>
                                <?php if(isset($_REQUEST['reporttype']) && $_REQUEST['reporttype']=='D') { ?>
                                <tr>
                                    <th>Project</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Task</th>
                                    <th>Hours</th>
                                    <th>Overtime</th>
                                    <th>Status</th>
                                   </tr>
                                <?php }else{ ?>
                                <tr>
                                    <th>Project</th>
                                    <th>Period</th>
                                    <th>Total Time</th>
                                    <th>Status</th>
                                </tr>
                              <?php  } ?>
                            </thead>
                             <tbody id="infoBody">
            </tbody>
                            <tfoot>
                                <tr>
                                     <?php if(isset($_REQUEST['reporttype']) && $_REQUEST['reporttype']=='D') {  ?>
                                <tr>
                                    <th>Project</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Task</th>
                                    <th>Hours</th>
                                    <th>Overtime</th>
                                    <th>Status</th>
                                   </tr>
                                <?php }else{ ?>
                                <tr>
                                    <th>Project</th>
                                    <th>Period</th>
                                    <th>Total Time</th>
                                    <th>Status</th>
                                </tr>
                              <?php  } ?>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>