<?php
$this->load->model("employeeModel");
?> 
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Projects
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Projects</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                    <?php $total=''; $overtime='';?>
                  <h3 class="box-title">List of Project </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <!--<?php foreach ($allprojects as $project) {   ?>
                <li><a href="<?= base_url().'project/'.$project->project_id ?>"><?= $project->project_name ?></a></li>
                <?php } ?>-->
                <?php if(isset($tsdetails)){ ?>
                 <table class="table table-hover">
                    <tr  class="info">
                      <th>Employee</th>
                      <th>Project</th>
                      <th>Task</th>
                      <th>Task Description</th>
                      <th>Worked</th>
                      <th>OverTime</th>
                      <th>Date</th>
                    </tr>
                    <?php foreach ($tsdetails as $tsd) { ?>
                    <tr class="table-striped">
                      <td><?= $tsd->emp_name ?></td>
                      <td><?= $tsd->project_name ?></td>
                      <td><?= $tsd->task_name ?></td>
                      <td><?= $tsd->task_desc ?></td>
                      <td><span class="label label-success"><?= $tsd->hours ?><?php $total = $total+$tsd->hours; ?></span></td>
                      <td><?php $overtime = $overtime+$tsd->overtime; ?><?php echo $tsd->overtime > 0 ? "<span class=\"label label-warning\"> $tsd->overtime </span>" :'0.0' ?></td>
                       <!--<td><span class="label label-success"><?= $this->employeeModel->convertToHoursMins(($tsd->timesq/60), '%02d hours %02d minutes'); ?></span></td>-->
                       <td><?php  $time = strtotime($tsd->start_time);
                                   echo  date('l jS \of F Y',$time) ?></td></td>
                    </tr>
                    <?php  } ?>
                    <tr class="danger">
                        <td colspan="4"><b>Total Hours worked</b></td>
                        <td><span class="label label-success"><?= $total .' Hours' ?></span></td>
                        <td><span class="label label-warning"><?=$overtime .' Hours'?></span></td>
                        <td><span class="label label-danger"><?=$overtime+$total .' Hours'?></span></td>
                    </tr>
                  </table>
                <?php }else{
                    echo "Select a time sheet to view details here";
                } ?>
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-4">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Recently Time Sheet Update  </h3>
                  <div class="box-tools pull-right">
                  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                <?php foreach ($alltiemsheet as $timesheet) {   ?>
                          
                     <?php if($timesheet->status =='Pending'){  ?>
                      <li class="label-warning">
                   
                      <!-- todo text -->
                      <span class="text"><?= $timesheet->emp_name ?></span>
                      <!-- Emphasis label -->
                      <small>Has Submited A Time Sheet </small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                            <a class="btn btn-primary" href="<?= base_url().'manager/timesheetDetails/'.$timesheet->approval_id ?>" >Details</a>
                          <a class="btn btn-success" href="<?= base_url().'manager/timesheetAprove/'.$timesheet->approval_id.'/Approved' ?>" >Approved</a>
                          <a class="btn btn-danger"  href="<?= base_url().'manager/timesheetAprove/'.$timesheet->approval_id.'/Rejected' ?>" >Rejected</a>
                      </div>
                    </li>
                        <?php } elseif($timesheet->status =='Approved'){ ?>
                    <li class="label-success">
                   
                      <!-- todo text -->
                      <span class="text"><?= $timesheet->emp_name ?></span>
                      <!-- Emphasis label -->
                      <small>'s Time Sheet approved</small>
                      <!-- General tools such as edit or delete-->
                       <div class="tools">
                            <a href="<?= base_url().'manager/timesheetDetails/'.$timesheet->approval_id ?>" >Details</a>
                      </div>
                    </li>
                        <?php }  elseif($timesheet->status =='Rejected'){ ?>
                    <li class="label-danger">
                   
                      <!-- todo text -->
                      <span class="text"><?= $timesheet->emp_name ?></span>
                      <!-- Emphasis label -->
                      <small>'s Time sheet Rejected</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                          <a href="<?= base_url().'manager/timesheetDetails/'.$timesheet->approval_id ?>" >Details</a>
                          <!--<a href="<?= base_url().'manager/timesheetAprove/'.$timesheet->approval_id.'/Rejected' ?>" ><i class="fa fa-arrow-right"></i></a>-->
                      
                      </div>
                    </li>
                        <?php } ?>
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->