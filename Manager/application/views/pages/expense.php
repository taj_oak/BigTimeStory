<?php
$this->load->model("employeeModel");
?> 
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Expense
            <small>Manage</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Expense</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                    <?php $total=''; $overtime='';?>
                  <h3 class="box-title">List of Project </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <!--<?php foreach ($allprojects as $project) {   ?>
                <li><a href="<?= base_url().'project/'.$project->project_id ?>"><?= $project->project_name ?></a></li>
                <?php } ?>-->
                <?php if(isset($tsdetails)){ ?>
                 <table class="table table-hover">
                    <tr  class="info">
                      <th>Employee</th>
                      <th>Project</th>
                      <th>Expense</th>
                      <th>Category</th>
                      <th>Merchant</th>
                      <th>Date</th>
                    </tr>
                    <?php foreach ($tsdetails as $tsd) { ?>
                    
                    <tr class="table-striped">
                      <td><?= $tsd->emp_name ?></td>
                      <td><?= $tsd->project_name ?></td>
                      <td><?= '$ ' .$tsd->bill_amt ?><?php $total = $total+$tsd->bill_amt ?></td>
                      <td><?= $tsd->cat_name ?></td>
                       <td><?= $tsd->merchant ?></td>
                       <td><?php  $time = strtotime($tsd->bill_date);
                                   echo $tsd->bill_date ?></td>
                    </tr>
                    <?php  } ?>
                    <tr class="danger">
                        <td colspan="2"><b>Total Expense worked</b></td>
                        <td><span class="label label-success"><?= '$ '. $total  ?></span></td>
                        <td colspan="3"></td>
                                        
                    </tr>
                  </table>
                <?php }else{
                    echo "Select a time sheet to view details here";
                } ?>
              </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-4">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Recently Time Sheet Update  </h3>
                  <div class="box-tools pull-right">
                  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                <?php foreach ($allexpense as $expense) {   ?>
                          
                     <?php if($expense->status =='Pending'){  ?>
                      <li class="label-warning">
                   
                      <!-- todo text -->
                      <span class="text"><?= $expense->emp_name ?></span>
                      <!-- Emphasis label -->
                      <small>Has Submited A Time Sheet </small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                          <a class="btn btn-primary" href="<?= base_url().'expense/timesheetDetails/'.$expense->approval_id ?>" >Details</a>
                          <a  class="btn btn-success" href="<?= base_url().'expense/timesheetAprove/'.$expense->approval_id.'/Approved' ?>" >Approved</a>
                          <a class="btn btn-danger"  href="<?= base_url().'expense/timesheetAprove/'.$expense->approval_id.'/Rejected' ?>" >Rejected</a>
                      </div>
                    </li>
                        <?php } elseif($expense->status =='Approved'){ ?>
                    <li class="label-success">
                   
                      <!-- todo text -->
                      <span class="text"><?= $expense->emp_name ?></span>
                      <!-- Emphasis label -->
                      <small>'s Time Sheet approved</small>
                      <!-- General tools such as edit or delete-->
                       <div class="tools">
                            <a href="<?= base_url().'expense/timesheetDetails/'.$expense->approval_id ?>" >Details</a>
                      </div>
                    </li>
                        <?php }  elseif($expense->status =='Rejected'){ ?>
                    <li class="label-danger">
                   
                      <!-- todo text -->
                      <span class="text"><?= $expense->emp_name ?></span>
                      <!-- Emphasis label -->
                      <small>'s Time sheet Rejected</small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                          <a href="<?= base_url().'expense/timesheetDetails/'.$expense->approval_id ?>" >Details</a>
                          <!--<a href="<?= base_url().'expense/timesheetAprove/'.$expense->approval_id.'/Rejected' ?>" ><i class="fa fa-arrow-right"></i></a>-->
                      
                      </div>
                    </li>
                        <?php } ?>
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->