 <script src="<?= base_url() ?>dist/js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?= base_url() ?>dist/js/demo.js"></script>
        <script>
            var myajax = function () {
                $.ajax({
                    url: '<?= base_url() . 'manager/getmanagerNotification'; ?>',
                    type: "GET",
                    data: ({
                        code: '<?= $_SESSION['user_id'] ?>'
                    }),
                    beforeSend: function () {
                    },
                    error: function (request) {
                    },
                    success: function (data) {

                        var data = JSON.parse(data);
                        var currentvalue = $('#countNotification').html();
                        $('#countNotification').html(data.count);

                        if (currentvalue == data.count) {
                            $.each(data.tsdetails, function (key, value) {

                                $('#notification').prepend("<li> <a href=\"#\">" + value.emp_name + " has submited a timesheet </a> </li>")
                            });
                        }
                    }
                });

                //if you need to run again every 10 seconds
                //setTimeout(myajax, 10000);

            };
            setInterval(myajax, 5000);
            setTimeout(myajax, 300);
        </script>
</body>
</html>