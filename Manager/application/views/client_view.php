<html>
<head>
<title>Client Details</title>
</head>
<body>
<br>    
<div style="padding-left:250px;"><h1>Add Client Details</h1></div>
<div id="container" style="padding-left:250px;">
<?php echo form_open('client_ctrl'); ?>
<?php if (isset($message)) { ?>
    <CENTER><h3 style="color:green;">Data inserted successfully</h3></CENTER><br>
<?php } ?>
<?php echo form_label('Name :'); ?> <?php echo form_error('dname'); ?><br>
<?php echo form_input(array('id' => 'dname', 'name' => 'dname', 'placeholder' => 'Name')); ?><br /><br>

<?php echo form_label('Address :'); ?> <?php echo form_error('daddress'); ?><br>
<?php echo form_textarea(array('id' => 'daddress', 'name' => 'daddress','placeholder' => 'Address')); ?><br /><br>

<?php echo form_label('Email :'); ?> <?php echo form_error('demail'); ?><br>
<?php echo form_input(array('id' => 'demail', 'name' => 'demail', 'placeholder' => 'Email')); ?><br /><br>

<?php echo form_label('Phone Number :'); ?> <?php echo form_error('dmobile'); ?><br>
<?php echo form_input(array('id' => 'dmobile', 'name' => 'dmobile', 'placeholder' => '10 Digit Mobile No')); ?><br /><br>

<?php echo form_label('Per Hour :'); ?> <?php echo form_error('dperhr'); ?><br>
<?php echo form_input(array('id' => 'dperhr', 'name' => 'dperhr', 'placeholder' => 'Per Hour')); ?><br /><br>

<?php echo form_label('Skype Id :'); ?> <?php echo form_error('dskype'); ?><br>
<?php echo form_input(array('id' => 'dskype', 'name' => 'dskype', 'placeholder' => 'Skype Id')); ?><br /><br>

<?php echo form_label('Payment Cycle :'); ?> <?php echo form_error('dpcycle'); ?><br>
<?php echo form_input(array('id' => 'dpcycle', 'name' => 'dpcycle', 'placeholder' => 'Payment Cycle')); ?><br /><br>

<?php echo form_submit(array('id' => 'submit', 'value' => 'Add')); ?>
<?php echo form_close(); ?><br/>
</div>   
</body>
</html>

