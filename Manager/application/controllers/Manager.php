<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author Sk Mamtajuddin
 */
class Manager extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('ManagerModel');
        $this->load->helper("url");
        if (!isset($_SESSION['role']) || $_SESSION['role']!='manager')
        {
          redirect(main_url());
        }
    }
    public function index() {
		$data['error']='';
		$data['allprojects']= $this->ManagerModel->getallProjectbyManager();
                $data['alltiemsheet']= $this->ManagerModel->getallTimesheetbyManager();
		$data['page']='pages/projects';
                $this->load->view('main', $data);
    }
   
    public function timesheetAprove($apv_id,$status) {
                   $data=array('status'=>$status);
		$this->db->where('approval_id', $apv_id);
		$this->db->update('tms_time_sheet_approval', $data);
                redirect(base_url());
    }
    public function timesheetDetails($approval_id) {
        $data['tsdetails']= $this->ManagerModel->gettimesheetDetails($approval_id);
        $data['error']='';
        $data['allprojects']= $this->ManagerModel->getallProjectbyManager();
        $data['alltiemsheet']= $this->ManagerModel->getallTimesheetbyManager();
        $data['page']='pages/projects';
        $this->load->view('main', $data);
        
    }
    public function getmanagerNotification() {
        $data['tsdetails']= $this->ManagerModel->getallunapprovedTimesheetbyManager();
        $data['count'] = count($data['tsdetails']);
        echo json_encode($data);
        
    }
    
}