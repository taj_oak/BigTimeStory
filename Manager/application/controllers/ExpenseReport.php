<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExpenseReport
 *
 * @author Sk Mamtajuddin
 */
class ExpenseReport extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('ExpenseModel');
        $this->load->helper("url");
        if (!isset($_SESSION['role']) || $_SESSION['role'] != 'manager') {
            redirect(main_url());
        }
    }
    
    public function index() {
        //  echo "sdsa";
        $data['allprojects'] = $this->ExpenseModel->getallProjectbyManager();
        $data['allcategory'] = $this->ExpenseModel->getAllCategory();
        $data['page'] = 'pages/report_expanse';
        $this->load->view('main', $data);
    }

    public function expanseReportTable() {
        //print_r($_REQUEST);


        if (isset($_REQUEST['reporttype']) && $_REQUEST['reporttype'] == 'D') {
            $where = '';
            if (isset($_REQUEST['startdate']) && isset($_REQUEST['enddate'])) {

                $la_today = date('Y-m-d');
                $ls_30days = date('Y-m-d', strtotime("-30 days"));
                $startdate = (isset($_REQUEST['startdate'])) ? $_REQUEST['startdate'] : $ls_30days;
                $enddate = (isset($_REQUEST['enddate'])) ? $_REQUEST['enddate'] : $la_today;
                $this->datatables->where("bill_date BETWEEN '$startdate' AND '$enddate' ");
            } elseif (isset($_REQUEST['weekno'])) {
                $la_date = $this->getStartAndEndDate($_REQUEST['weekno'], $_REQUEST['year']);

                $startdate = $la_date[0];
                $enddate = $la_date[1];
                $this->datatables->where("bill_date BETWEEN '$startdate' AND '$enddate' ");
            }
            if (isset($_REQUEST['status'])) {
                $this->datatables->where_in('tms_expense.status', stripslashes(implode($_REQUEST['status'], "','")));
            }
            if (isset($_REQUEST['projects'])) {
                $this->datatables->where_in('tms_expense.project_id', stripslashes(implode($_REQUEST['projects'], "','")));
            }
            if (isset($_REQUEST['category'])) {
                $this->datatables->where_in('tms_expense.exp_catid', stripslashes(implode($_REQUEST['category'], "','")));
            }
            $this->datatables->select('exp_id,project_name,bill_date,tms_expense.STATUS,cat_name,merchant,bill_amt,tms_expense.file_path');
            $this->datatables->from('tms_expense');
            $this->datatables->join('tms_project', 'tms_project.project_id=tms_expense.project_id');
            $this->datatables->join('tms_expense_cat', 'tms_expense_cat.cat_id=tms_expense.exp_catid');
            //  $this->datatables->filter();
            $this->datatables->add_column('STATUS', '<span class="btn $1">$1</span>', 'STATUS');
            $this->datatables->add_column('file_path', '<a target="_blank" href="http://localhost:8081/BigTimeStory/UserEnd/uploads/$1" class="btn approve">view</a>', 'file_path');
            // $this->datatables->add_column('file_path', '<a href="'+ base_url() +'upload/$1"  >View</a>', 'file_path');
            echo $this->datatables->generate('json', 'utf-8');
        } else {
            if (isset($_REQUEST['reporttype'])) {
                $startdate = (isset($_REQUEST['startdate'])) ? $_REQUEST['startdate'] : $ls_30days;
                $enddate = (isset($_REQUEST['enddate'])) ? $_REQUEST['enddate'] : $la_today;
                $this->datatables->where("bill_date BETWEEN '$startdate' AND '$enddate' ");

                if (isset($_REQUEST['status'])) {
                    $this->datatables->where_in('tms_expense.status', stripslashes(implode($_REQUEST['status'], "','")));
                }
                if (isset($_REQUEST['projects'])) {
                    $this->datatables->where_in('tms_expense.project_id', stripslashes(implode($_REQUEST['projects'], "','")));
                }
            }
            $this->datatables->select('GROUP_CONCAT(DISTINCT project_name SEPARATOR "<br>") as project_name,tms_expense.project_id,weekno,year,tms_expense.STATUS,SUM(bill_amt) as bill_amt');
            $this->datatables->from('tms_expense');
            $this->datatables->join('tms_project', 'tms_project.project_id=tms_expense.project_id');
            $this->datatables->join('tms_expense_cat', 'tms_expense_cat.cat_id=tms_expense.exp_catid');
            $this->datatables->group_by('tms_expense.weekno,project_name');
            //$this->datatables->where();
            $this->datatables->add_column('STATUS', '<span class="btn $1">$1</span>', 'STATUS'); //?projects[]=$3weekno=$1&year=$2&reporttype=D
            $this->datatables->add_column('weekno', '<a target="_blank" href="?projects[]=$3weekno=$1&year=$2&reporttype=D" class="btn btn-primary ">Week $1 of $2 </span>', 'weekno,year,project_id');
            // $this->datatables->add_column('weekno', '<span onclick="getdetailsView($1,$2,$3)" class="btn btn-primary ">Week $1 of $2 </span>', 'weekno,year,project_id');
            echo $this->datatables->generate('json', 'utf-8');
        }
        // echo $this->db->last_query();
    }

    function getStartAndEndDate($week, $year) {

        $time = strtotime("1 January $year", time());
        $day = date('w', $time);
        $time += ((7 * $week) + 1 - $day) * 24 * 3600;
        $return[0] = date('Y-m-d', $time);
        $time += 6 * 24 * 3600;
        $return[1] = date('Y-m-d', $time);
        return $return;
    }

    public function getallProjectExpanse() {

        $la_allexpase['project'] = $this->ExpenseModel->getProjectwiseData();
        $la_allexpase['cat'] = $this->ExpenseModel->getCatwiseData();

        echo json_encode($la_allexpase);
    }

    public function getallProjectExpanseBar() {
        $la_allexpase['project'] = $this->ExpenseModel->getbarchartData();
        echo json_encode($la_allexpase);
    }

    //put your code here
}
