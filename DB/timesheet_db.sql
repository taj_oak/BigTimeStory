-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2015 at 03:45 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `timesheet_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `ai` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`ai`),
  UNIQUE KEY `ci_sessions_id_ip` (`id`,`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `denied_access`
--

CREATE TABLE IF NOT EXISTS `denied_access` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IP_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  `reason_code` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ips_on_hold`
--

CREATE TABLE IF NOT EXISTS `ips_on_hold` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IP_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_errors`
--

CREATE TABLE IF NOT EXISTS `login_errors` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username_or_email` varchar(255) NOT NULL,
  `IP_address` varchar(45) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_categoris`
--

CREATE TABLE IF NOT EXISTS `tms_categoris` (
  `cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(50) NOT NULL,
  `enabled_flag` varchar(1) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `project_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_client`
--

CREATE TABLE IF NOT EXISTS `tms_client` (
  `client_id` int(10) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(500) NOT NULL,
  `client_address` varchar(2000) NOT NULL,
  `per_hour` float NOT NULL,
  `skype_id` varchar(500) DEFAULT NULL,
  `client_email_id` varchar(500) DEFAULT NULL,
  `phone_number` int(10) DEFAULT NULL,
  `payment_cycle` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_company`
--

CREATE TABLE IF NOT EXISTS `tms_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(45) DEFAULT NULL,
  `company_logo` varchar(205) DEFAULT NULL,
  `company_address` varchar(445) DEFAULT NULL,
  `company_tel` varchar(45) DEFAULT NULL,
  `company_desc` varchar(425) DEFAULT NULL,
  `database_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_consultant`
--

CREATE TABLE IF NOT EXISTS `tms_consultant` (
  `cons_id` int(10) NOT NULL,
  `cons_name` varchar(500) NOT NULL,
  `cons_address` varchar(2000) NOT NULL,
  `skype_id` varchar(500) DEFAULT NULL,
  `cons_email_id` varchar(500) DEFAULT NULL,
  `phone_number` int(10) DEFAULT NULL,
  `vendor_id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tms_database`
--

CREATE TABLE IF NOT EXISTS `tms_database` (
  `DATABASE_ID` int(10) NOT NULL,
  `DATABASE_Name` varchar(45) DEFAULT NULL,
  `DATABASE_host` varchar(45) DEFAULT NULL,
  `DATABASE_user` varchar(45) DEFAULT NULL,
  `DATABASE_password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`DATABASE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tms_employee`
--

CREATE TABLE IF NOT EXISTS `tms_employee` (
  `emp_id` int(10) NOT NULL,
  `emp_name` varchar(500) NOT NULL,
  `emp_address` varchar(2000) NOT NULL,
  `skype_id` varchar(500) DEFAULT NULL,
  `emp_email_id` varchar(500) DEFAULT NULL,
  `phone_number` int(10) DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `project_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tms_expances`
--

CREATE TABLE IF NOT EXISTS `tms_expances` (
  `exp_id` int(10) NOT NULL AUTO_INCREMENT,
  `exp_day` varchar(10) NOT NULL,
  `exp_date` date NOT NULL,
  `exp_billable` varchar(3) NOT NULL,
  `project_id` int(10) NOT NULL,
  `exp_reimbursable` varchar(3) NOT NULL,
  `exp_description` varchar(2000) NOT NULL,
  `exp_by` int(10) NOT NULL,
  `exp_type` varchar(50) NOT NULL,
  `exp_cat_id` int(10) NOT NULL,
  PRIMARY KEY (`exp_id`),
  KEY `cat_id_idx` (`exp_cat_id`),
  KEY `project_id_idx` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_expances_category`
--

CREATE TABLE IF NOT EXISTS `tms_expances_category` (
  `exp_cat_id` int(10) NOT NULL AUTO_INCREMENT,
  `exp_cat_name` int(11) NOT NULL,
  PRIMARY KEY (`exp_cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_invoices`
--

CREATE TABLE IF NOT EXISTS `tms_invoices` (
  `invoice_id` int(10) NOT NULL AUTO_INCREMENT,
  `time_sheet_id` int(10) NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_amount` float NOT NULL,
  `due_amount` float NOT NULL,
  `paid_amount` varchar(12) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `tms_project_project_id` int(10) NOT NULL,
  PRIMARY KEY (`invoice_id`,`tms_project_project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_invoice_lines`
--

CREATE TABLE IF NOT EXISTS `tms_invoice_lines` (
  `inv_line_id` int(10) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(10) NOT NULL,
  `line_qty` int(10) NOT NULL,
  `rate` int(10) NOT NULL,
  `due_date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `line_amt` int(10) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`inv_line_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_project`
--

CREATE TABLE IF NOT EXISTS `tms_project` (
  `project_id` int(10) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(50) NOT NULL,
  `project_cat` int(10) NOT NULL,
  `manager_id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_project_vendor`
--

CREATE TABLE IF NOT EXISTS `tms_project_vendor` (
  `vendor_id` int(10) DEFAULT NULL,
  `project_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tms_roles`
--

CREATE TABLE IF NOT EXISTS `tms_roles` (
  `role_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `p_role_id` int(10) NOT NULL,
  `enabled_flag` varchar(1) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_role_functions`
--

CREATE TABLE IF NOT EXISTS `tms_role_functions` (
  `role_fn_id` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `function_id` int(10) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`role_fn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_seed_functions`
--

CREATE TABLE IF NOT EXISTS `tms_seed_functions` (
  `function_id` int(10) NOT NULL AUTO_INCREMENT,
  `function_name` varchar(50) NOT NULL,
  `access_url` varchar(4000) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`function_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_teams`
--

CREATE TABLE IF NOT EXISTS `tms_teams` (
  `team_id` int(10) NOT NULL,
  `team_name` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `team_mngr_id` int(10) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tms_team_member`
--

CREATE TABLE IF NOT EXISTS `tms_team_member` (
  `tms_team_member_id` int(10) NOT NULL AUTO_INCREMENT,
  `team_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`tms_team_member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_time_sheet`
--

CREATE TABLE IF NOT EXISTS `tms_time_sheet` (
  `time_sheet_id` int(10) NOT NULL AUTO_INCREMENT,
  `project_id` int(10) NOT NULL,
  `project_category_id` int(10) NOT NULL,
  `manager_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `task_name` varchar(50) NOT NULL,
  `task_desc` varchar(1000) NOT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  PRIMARY KEY (`time_sheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_time_sheet_approval`
--

CREATE TABLE IF NOT EXISTS `tms_time_sheet_approval` (
  `approval_id` int(10) NOT NULL AUTO_INCREMENT,
  `time_sheet_id` int(10) NOT NULL,
  `submit_date` date DEFAULT NULL,
  `submitted_by` int(10) NOT NULL,
  `status` varchar(15) DEFAULT NULL,
  `action_by` int(10) NOT NULL,
  `action_date` date NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `who_created` int(10) NOT NULL,
  `who_updated` int(10) NOT NULL,
  PRIMARY KEY (`approval_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tms_users`
--

CREATE TABLE IF NOT EXISTS `tms_users` (
  `user_id` int(10) unsigned NOT NULL,
  `user_name` varchar(12) DEFAULT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_pass` varchar(60) NOT NULL,
  `user_salt` varchar(32) NOT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_login_time` datetime DEFAULT NULL,
  `user_session_id` varchar(40) DEFAULT NULL,
  `user_date` datetime NOT NULL,
  `user_modified` datetime NOT NULL,
  `user_agent_string` varchar(32) DEFAULT NULL,
  `user_level` tinyint(2) unsigned NOT NULL,
  `user_banned` enum('0','1') NOT NULL DEFAULT '0',
  `passwd_recovery_code` varchar(60) DEFAULT NULL,
  `passwd_recovery_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tms_users`
--

INSERT INTO `tms_users` (`user_id`, `user_name`, `user_email`, `user_pass`, `user_salt`, `user_last_login`, `user_login_time`, `user_session_id`, `user_date`, `user_modified`, `user_agent_string`, `user_level`, `user_banned`, `passwd_recovery_code`, `passwd_recovery_date`) VALUES
(2147484849, 'skunkbot', 'skunkbot@example.com', '$2a$09$c89fd572bb855e8857effOMV029ZFrQ6XXUIYqSa.tCi1pEbmSqr.', 'c89fd572bb855e8857effb2154aa72b6', NULL, NULL, NULL, '2015-10-29 18:58:49', '2015-10-29 18:58:49', NULL, 1, '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tms_vendor`
--

CREATE TABLE IF NOT EXISTS `tms_vendor` (
  `vendor_id` int(10) NOT NULL,
  `vendor_name` varchar(500) NOT NULL,
  `vendor_address` varchar(2000) NOT NULL,
  `per_hour` float NOT NULL,
  `skype_id` varchar(500) DEFAULT NULL,
  `vendor_email_id` varchar(500) DEFAULT NULL,
  `phone_number` int(10) DEFAULT NULL,
  `payment_cycle` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `username_or_email_on_hold`
--

CREATE TABLE IF NOT EXISTS `username_or_email_on_hold` (
  `ai` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username_or_email` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`ai`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` enum('admin','manager','user') COLLATE utf8_bin NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`, `role`) VALUES
(1, 'admin', '$2a$08$Z9baq.bYYg4se7L/T4Y6kOLF4J3Mcxk64MR55xYKmodsPhQ/FLila', 'admin@gmail.com', 1, 0, NULL, NULL, NULL, NULL, '', '::1', '2015-10-30 15:45:23', '2015-10-29 19:28:12', '2015-10-30 14:45:23', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
