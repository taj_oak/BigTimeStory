<!-- Content Wrapper. Contains page content -->
<link rel="<?= base_url() . 'css/style.css' ?>"/>      
<div class="content-wrapper">        
    <!-- Content Header (Page header) -->        
    <section class="content-header">          
        <h1>            Dashboard            <small>Version 2.0</small>          </h1>          
        <ol class="breadcrumb">            
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>            
            <li class="active">Dashboard</li>          
        </ol>        
    </section>        
    <!-- Main content -->        
    <section class="content">          
        <!-- Info boxes -->          
        <div class="row">            
            <div class="col-md-3 col-sm-6 col-xs-12">              
                <div class="info-box">                
                    <span class="info-box-icon bg-aqua"><i class="ion ion-stats-bars"></i></span>                
                    <a class="info-box-content" href="<?= base_url() . 'dashboard/showprojects' ?>">                  
                        <span class="info-box-text">Total Project</span>                  
                        <span class="info-box-number"><?= $totalcount['totalproject'] ?><small></small></span>                
                    </a><!-- /.info-box-content -->              
                </div><!-- /.info-box -->            
            </div><!-- /.col -->            
            <div class="col-md-3 col-sm-6 col-xs-12">              
                <div class="info-box">                
                    <span class="info-box-icon bg-red"><i class="fa fa-user-secret"></i></span>                
                    <a class="info-box-content">                  
                        <span class="info-box-text">Enterprise Portal</span>                  
                        <span class="info-box-number"><?= $totalcount['totalclient'] ?></span>                
                    </a><!-- /.info-box-content -->              
                </div><!-- /.info-box -->            
            </div><!-- /.col -->            
            <!-- fix for small devices only -->            
            <div class="clearfix visible-sm-block"></div>            
            <div class="col-md-3 col-sm-6 col-xs-12">              
                <div class="info-box">                
                    <span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>               
                    <a class="info-box-content" href="<?= base_url('expanse/report') ?>">                  
                        <span class="info-box-text">Total Expense</span>                  
                        <span class="info-box-number">USD <?= $totalcount['totalExpanse'] ?></span>                
                    </a><!-- /.info-box-content -->              
                </div><!-- /.info-box -->            
            </div><!-- /.col -->            
            <div class="col-md-3 col-sm-6 col-xs-12">              
                <div class="info-box">                
                    <span class="info-box-icon bg-yellow"><i class="fa fa-clock-o"></i></span>                
                    <a class="info-box-content" href="<?= base_url('timesheetreport') ?>">                  
                        <span class="info-box-text">Total Hours Worked</span>                  
                        <span class="info-box-number"><?= $totalcount['totalTime'] ?> Hours</span>                
                    </a><!-- /.info-box-content -->              
                </div><!-- /.info-box -->            
            </div><!-- /.col -->          
        </div><!-- /.row -->            
        <div class="row">            
            <!-- Left col -->            
            <div class="col-md-6">              
                <!-- MAP & BOX PANE -->              
                <div class="box box-info">                
                    <div class="box-header with-border">                  
                        <h3 class="box-title">Last 8 Week Time sheet</h3>                     
                    </div><!-- /.box-header -->                
                    <div class="box-body">                  
                        <div class="table-responsive">                    
                            <table class="table no-margin">                    
                                <thead>                        
                                    <tr>                          
                                        <th>Project Name</th>                          
                                        <th>Week</th>                          
                                        <th>Hours Worked</th>                          
                                        <th>Status</th>                        
                                    </tr>                      
                                </thead>                      
                                <tbody>        

                                    <?php foreach ($latestweeklyTime as $wtime) { ?>                                                                               
                                        <tr>                          
                                            <td><?= $wtime->project_name ?></td>                
                                            <td><?= $wtime->weekNumber ?></td>             
                                            <td><span class="label label-success"><?= ($wtime->thours); ?></span>
                                                <?php echo ($wtime->tovertime > 0.00) ? "<span class=\"label label-danger\" >$wtime->tovertime</span>" : '' ?></td>   
                                            <td><span class="btn  <?php
                                                if ($wtime->status == 'Approved') {
                                                    echo 'btn-success';
                                                } elseif ($wtime->status == 'Pending') {
                                                    echo 'btn-warning';
                                                } else {
                                                    echo 'btn-default';
                                                }
                                                ?>"><?= $wtime->status ?></span></td> 
                                        </tr> 
                                    <?php } ?>                      
                                </tbody> </table>                  
                        </div><!-- /.table-responsive -->                
                    </div><!-- /.box-body -->              
                </div>                         
            </div>      
            <!-- /.box -->
            <!-- TABLE: LATEST ORDERS -->             

            <div class="col-md-6">              
                <!-- MAP & BOX PANE -->              
                <div class="box box-info">                
                    <div class="box-header with-border">                  
                        <h3 class="box-title">Last 8 Week Expanse</h3>                     
                    </div><!-- /.box-header -->                
                    <div class="box-body">                  
                        <div class="table-responsive">                    
                            <table class="table no-margin">                    
                                <thead>                        
                                    <tr>                          
                                        <th>Project Name</th>                          
                                        <th>Week</th>                          
                                        <th>Expanse Amount</th>                          
                                        <th>Status</th>                        
                                    </tr>                      
                                </thead>                      
                                <tbody>        

                                    <?php 
                                        if(isset($latestweeklyExpanse) && !empty($latestweeklyExpanse)) {
                                        foreach ($latestweeklyExpanse as $wtime) { ?>                                                                               
                                        <tr>                          
                                            <td><?= $wtime->project_name ?></td>                
                                            <td><?= $wtime->weekno ?></td>             
                                            <td><span class="label label-success"><?= ($wtime->total_bill_amt); ?></span>
                                                <?php echo ($wtime->bill_amt > 0.00) ? "<span class=\"label label-danger\" >$wtime->bill_amt</span>" : 0.00 ?></td>   
                                            <td><span class="btn  <?php
                                                if ($wtime->STATUS == 'Approved') {
                                                    echo 'btn-success';
                                                } elseif ($wtime->STATUS == 'Pending') {
                                                    echo 'btn-warning';
                                                } else {
                                                    echo 'btn-default';
                                                }
                                                ?>"><?= $wtime->STATUS ?></span></td> 
                                        </tr> 
                                        <?php } } ?>                      
                                </tbody> </table>                  
                        </div><!-- /.table-responsive -->                
                    </div><!-- /.box-body -->              
                </div>                         
            </div> 
            
            
            
            
            
            
        </div>          <!-- Main row -->          
        <div class="row">  
            
            <div class="col-md-6">  
                <div class="box box-info">  
                    <div class="box-header with-border"> 
                        <h3 class="box-title">Latest Time Sheet Update</h3>                                 
                    </div><!-- /.box-header -->                
                    <div class="box-body">                  
                        <div class="table-responsive">                    
                            <table class="table no-margin">                      
                                <thead>                       
                                    <tr>                          
                                        <th>Project Name</th>
                                        <th>Task</th>
                                        <th>Hours Worked</th>   
                                        <th>Added</th>
                                    </tr>                      
                                </thead>
                                <tbody>
                                    <?php foreach ($latestTime as $time) { ?>
                                        <tr>      
                                            <td><?= $time->project_name ?></td>
                                            <td><?= $time->task_name ?></td> 
                                            <td><span class="label label-success"><?= ($time->hours); ?></span>
                                                <?php echo ($time->overtime > 0.00) ? "<span class=\"label label-danger\" >$time->overtime</span>" : '' ?></td>
                                            <td><?= date('l  h:i A d M  y', strtotime($time->start_time)) ?></td> 
                                        </tr>                          
                                    <?php } ?>   
                                </tbody> 
                            </table>               
                        </div>           
                    </div>   
                </div>  
            </div>   
            
            
            <div class="col-md-6"> 
                <div class="box box-info">   
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Expanse Update</h3>  
                        <!--<b><?//= array_sum(array_column($latestExpense, 'bill_amt')); ?></b>-->
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">                   
                                <thead>     
                                    <tr>               
                                        <th>Project Name</th>    
                                        <th>Expense on</th>            
                                        <th>Expense</th>        
                                        <th>Added</th>          
                                    </tr>              
                                </thead>              
                                <tbody>                        
                                    <?php foreach ($latestExpense as $expense) { ?>            

                                        <tr>                          
                                            <td><?= $expense->project_name ?></td> 
                                            <td><?= $expense->cat_name ?></td>  
                                            <td><span class="label label-success">USD <?= ($expense->bill_amt); ?></span></td> 
                                            <td><?= date('l  d M  y', strtotime($expense->bill_date)) ?></td>
                                        </tr>     
                                    <?php } ?> 
                                </tbody>
                            </table> 
                        </div>
                    </div>
                </div>
            </div>
            
        
        </div>
    </section>      </div> 