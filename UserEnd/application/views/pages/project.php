   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Project  
            <small>View</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Projects</a></li>
            <li class="active">view</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            
            <!-- right column -->
            <div class="col-md-12">
               <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Project List</h3>
                  
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                      <?php 
                                   
                                   foreach ($allprojects as $project) {  
                                       //print_r($project);
                                       ?>
                          
                    
                    <li>
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <!--<input type="checkbox" value="" name="">-->
                      <!-- todo text -->
                      <span class="text"><?php echo $project->project_name?></span>
                      <span class="text"><?php echo $project->client_name?>(<?php echo $project->client_email_id?>)</span>
                    
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i></small>
                      <!-- General tools such as edit or delete-->
                     
                    </li>
      
                    <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              
              </div><!-- /.box -->

            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->