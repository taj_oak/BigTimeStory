<?php
$this->load->model('GroupModel');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Project Section 
            <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add New Group & Members</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="<?= base_url('groupmanage/AddGroup') ?>" method="POST">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Group Name</label>
                                <input class="form-control" name="group" type="text" placeholder="Group Name" >
                            </div>                    
                            <div class="form-group">
                                <label for="exampleInputEmail1">Employee</label>
                                <select class="form-control select2" multiple="multiple" data-placeholder="Select Employee/Counsult" name="pemployeeId[]">
                                    <option value=""></option>
                                    <?php 
                                    foreach ($allemployees as $employee) { ?>
                                        <option  value="<?php echo $employee->id; ?>"><?php echo $employee->emp_name; ?> (<?php echo $employee->email; ?>)</option>
                                    <?php     }     ?>
                                </select>
                            </div>


                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title">Group List</h3>
                        
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php $i = 0; foreach ($allgroups as $group) { 
                                
                            
                                ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?= $i ?>">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$i?>" aria-expanded="<?php echo ($i == 0 ?  'true' :  'false'); ?>" aria-controls="collapse<?= $i ?>">
                                           <?php echo $group->team_name; ?>
                                        </a>
                                        <!--<span class="label label-primary pull-right"><?= count($project->who_created) ?></span>-->
                                    </h4>
                                </div>
                               <div id="collapse<?=$i?>" class="panel-collapse <?php echo ($i == 0 ?  'collapsed in' :  'collapsed'); ?>" role="tabpanel" aria-labelledby="heading<?=$i?>">
                                    <div class="panel-body">
                                        <ul  class="todo-list">
                                           <?php $emps = $this->GroupModel->getAllUserByTeam($group->team_id); foreach ($emps as $emp) { ?>
                                                                                
                                                                           
                                            <li>
                                                <span><?= $emp->emp_name ?></span> <small class="alert-danger"></small> 
                                               
                                                <div class="tools">
                        <i class="fa fa-trash-o"><a href="<?= base_url('projectAssigned_ctrl/deleteAssignedProject').'/'.$emp->user_id.'/'.$group->team_id  ?>"> Delete</a></i>
                                                </div> 
                                            </li>
                                           <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php $i++ ; } ?>


                        </div>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div><!--/.col (right) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->