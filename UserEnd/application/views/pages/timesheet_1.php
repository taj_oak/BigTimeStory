<?php $datearray=array();
 foreach ($daycount as $key => $value) {
    
     $datearray[]=$value['date'];
}
?>
<script>
var count ='<?php echo json_encode(array_values($daycount))?>';
count = JSON.parse(count);
var dates = '<?php echo json_encode(array_values($datearray))?>';
dates = JSON.parse(dates);
</script>
<style>.datepicker{z-index:1200 !important;}</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            TimeSheet <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
        </ol>
        <table  id="weekViewStatus" align="center" style="margin-right: 5px;">
        </table>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-10" id="monthViewFace">
                <div class="box box-primary">
                    <div class="box-body ">
                        <!-- THE CALENDAR -->
                        <div id="timesheetcalendar"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2" id="divView">
                <div class="box box-solid">
                    <div class="box-body">
                        <!-- the events -->
                        <table class="table table-bordered" id="statusView" style="margin-bottom: -10px;">
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
$this->load->view('common/timesheetModel')
?>
<script>
    var sessionProjectId = <?php echo isset($_SESSION['project_id']) ? $_SESSION['project_id'] : -11 ?>;
    var date = new Date();
    var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();
    $(document).ready(function () {
        // getTimeSheetDetails("" , "agendaWeek");
        var eventObject = [];
        loadCalender(eventObject);
    });
    function getTimeSheetDetails(startTime, view) {
        var totalWeeks = 6;//new Date(startTime).getWeekOfMonth();
        var weekNumber = getWeekNumber(startTime);
        var sTime = convert(startTime, "/");
        $.ajax({
            method: "POST",
            url: "timesheet/api?method=list",
            asyn: false,
            cache: true,
            data: 'showdate=' + sTime + '&viewtype=' + view,
            dataType: "html",
            success: function (data) {
                if (data != null) {
                    var eventItems = JSON.parse(data);
                    makeObjectArray(eventItems["events"], view);
                    if (view == "month") {
                        monthViewCreation(eventItems["eventview"], view, totalWeeks, weekNumber);
                    } else {
                        weekViewCreation(eventItems["eventview"], view, totalWeeks, weekNumber);
                    }
                } else {
                    loadCalender([]);
                }
            }
        });
    }
    function makeObjectArray(eventsArray, view) {//debugger;
        var eventObject = [];
        var TOTALHOURS = 0;
        var TOTALOVERTIME = 0;
        var ids = 0;
        if (eventsArray != null && eventsArray != undefined && eventsArray.length > 0) {
            for (var ea = 0; ea < eventsArray.length; ea++) {
                var eventArr = eventsArray[ea];//id#name#desc#sTime#eTime#pId#cId#mId#uId#color#isAllDayEvent#hours#overtime#weekNumber
                console.log(eventArr);
                var timesheetObject = new Object();
                timesheetObject.id = eventArr[0];
                timesheetObject.title = eventArr[1];
                timesheetObject.description = eventArr[2];
                timesheetObject.start = eventArr[3];
                timesheetObject.sTime = eventArr[3];
                timesheetObject.end = eventArr[4];
                timesheetObject.eTime = eventArr[4];
                timesheetObject.projectId = eventArr[5];
                timesheetObject.categoryId = eventArr[6];
                timesheetObject.managerId = eventArr[7];
                timesheetObject.userId = eventArr[8];
                timesheetObject.backgroundColor = eventArr[9];
                timesheetObject.borderColor = eventArr[9];
                timesheetObject.IsAllDayEvent = eventArr[10];
                timesheetObject.hours = eventArr[11];
                timesheetObject.overtime = eventArr[12];
                timesheetObject.weekNumber = eventArr[13];
                //timesheetObject.view 			= view.name;
                eventObject.push(timesheetObject);
            }
        }
        //loadCalender(eventObject)
        $('#timesheetcalendar').fullCalendar('removeEvents');
        $('#timesheetcalendar').fullCalendar('addEventSource', eventObject);
    }
    Date.prototype.getWeekOfMonth = function () {
        var firstDay = new Date(this.setDate(1)).getDay();
        var totalDays = new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate();
        return Math.ceil((firstDay + totalDays) / 7);
    }
    function monthViewCreation(eventsViewArr, view, totalWeeks, weekNumber) {
        $("#monthViewFace").removeClass("col-md-12").addClass("col-md-10");
        $("#divView").show();
        $("#statusView").empty();
        $("#statusView").show();
        $("#weekViewStatus").empty();
        $("#weekViewStatus").hide();

        $tr = $("<tr />");
        $th = $("<th />").attr({"height": "85px"}).html("Weekly View").css({"padding-top": "27px"});
        $tr.append($th);
        $("#statusView").append($tr);
        for (var eva = 0; eva < totalWeeks; eva++) {
            $tr = $("<tr />").attr({"height": "20px"});
            $td = $("<td />").attr({"height": "20px"});
            $br0 = $("<br />");
            $br1 = $("<br />");
            $br2 = $("<br />");
            $br3 = $("<br />");
            $button = $("<button />");
            $span0 = $("<span />");
            $span1 = $("<span />");
            $span2 = $("<span />");
            $span3 = $("<span />");
            var hours = "0.0";
            var overtime = "0.0";
            var totalHours = "0.0";
            var timeSheetId = 0;
            var approvalId = 0;
            var status = "Submit";
            if (eventsViewArr != null && eventsViewArr != undefined && eventsViewArr.length > 0) {
                for (var jk = 0; jk < eventsViewArr.length; jk++) {
                    var eventViewObj = eventsViewArr[jk];
                    if (eventViewObj != null && eventViewObj != undefined && eventViewObj.hasOwnProperty(weekNumber)) {
                        var viewObj = eventViewObj[weekNumber];
                        timeSheetId = viewObj[0];
                        hours = viewObj[1];
                        overtime = viewObj[2];
                        totalHours = viewObj[3];
                        approvalId = viewObj[4];
                        status = viewObj[5];
                    }
                }
            }
            $span0.html("<b>W" + weekNumber + "</b>");
            $span1.html("<b>Hours :</b> " + hours);
            $span2.html("<b>Over Time :</b> " + overtime);
            $span3.html("<b>Total : </b>" + totalHours);
            $td.append($span0);
            $td.append($br0);
            $td.append($span1);
            $td.append($br1);
            $td.append($span2);
            $td.append($br2);
            $td.append($span3);
            $td.append($br3);
            $button.attr({"type": "button", "id": "approvedId" + weekNumber, "totalHours": totalHours, "timeSheetIds": timeSheetId, "weekNumber": weekNumber, "approvalId": approvalId});
            if ((status != "Submit" && status != "Rejected") || totalHours == "0.0") {
                $button.attr({"disabled": true});
            }
            if (status == "Rejected") {
                status = "Re Submit";
            }
            $button.html(status).css({"float": "right"});
            $button.addClass("btn btn-primary btn-sm");
            $button.bind("click", function () {
                var _totalHrs = $(this).attr("totalHours");
                var _timeSheetIds = $(this).attr("timeSheetIds");
                var _weekNo = $(this).attr("weekNumber");
                var _approvalId = $(this).attr("approvalId");
                approvedTimeSheet(_totalHrs, _timeSheetIds, _weekNo, _approvalId);
            });
            $td.append($button);
            $tr.append($td);
            $("#statusView").append($tr);
            disabledWeekPanel(weekNumber, status);
            weekNumber++;
            if (weekNumber > 52) {
                weekNumber = 1;
            }
        }
    }
    function approvedTimeSheet(_totalHrs, _timeSheetIds, _weekNo, _approvalId) {
        if (_timeSheetIds != null && typeof _timeSheetIds != 'undefined' && _timeSheetIds != "" && _totalHrs != "0.0") {
            bootbox.prompt({
                title: "Your total working hours is " + _totalHrs + ". Are you sure want to submit it ? ",
                value: "",
                placeholder: 'Description',
                callback: function (result) {//alert(result);
                    if (result != null && result != "") {
                        $.ajax({
                            method: "POST",
                            url: "timesheet/api?method=approved",
                            asyn: true,
                            cache: true,
                            data: 'timeSheetIds=' + _timeSheetIds + "&weekNumber=" + _weekNo + "&comment=" + result,
                            dataType: "html",
                            success: function (data) {
                                if (data != null) {
                                    //alert( "Time Sheet Approval Submitted Successfully"+data);
                                    $("#approvedId" + _weekNo).attr({"disabled": true}).html("Pending");
                                }
                            }
                        });
                    } else {
                        alert("Please add description on text box..!");
                        return false;
                    }
                }
            });
        } else {
            alert("There is no Task to approved...Please add task to approved");
        }
    }
    function weekViewCreation(eventsViewArr, view, totalWeeks, weekNumber) {
        $("#monthViewFace").removeClass("col-md-10").addClass("col-md-12");
        $("#divView").hide();
        $("#statusView").empty();
        $("#statusView").hide();
        $("#weekViewStatus").empty();
        $("#weekViewStatus").show();
        $tr = $("<tr />");
        $td = $("<td />");
        $br = $("<br />");
        $button = $("<button />");
        $span0 = $("<span />");
        $span1 = $("<span />");
        $span2 = $("<span />");
        $span3 = $("<span />");
        var hours = "0.0";
        var overtime = "0.0";
        var totalHours = "0.0";
        var timeSheetId = 0;
        var approvalId = 0;
        var status = "Submit";
        if (weekNumber > 52) {
            weekNumber = 1;
        }
        if (eventsViewArr != null && eventsViewArr != undefined && eventsViewArr.length > 0) {
            for (var jk = 0; jk < eventsViewArr.length; jk++) {
                var eventViewObj = eventsViewArr[jk];
                if (eventViewObj != null && eventViewObj != undefined && eventViewObj.hasOwnProperty(weekNumber)) {
                    var viewObj = eventViewObj[weekNumber];
                    timeSheetId = viewObj[0];
                    hours = viewObj[1];
                    overtime = viewObj[2];
                    totalHours = viewObj[3];
                    approvalId = viewObj[4];
                    status = viewObj[5];
                }
            }
        }
        $span0.text("Week No. : " + weekNumber + "   ");
        $span1.text("Hours : " + hours + "   ");
        $span2.text("Overtime : " + overtime + "   ");
        $span3.text("Total : " + totalHours + "   ");
        $td.append($span0);
        $td.append($span1);
        $td.append($span2);
        $td.append($span3);
        $button.attr({"type": "button", "id": "approvedId" + weekNumber, "totalHours": totalHours, "timeSheetIds": timeSheetId, "weekNumber": weekNumber, "approvalId": approvalId});
        if (view == "agendaDay") {
            $button.attr({"disabled": true});
        } else if ((status != "Submit" && status == "Rejected") || totalHours == "0.0") {
            $button.attr({"disabled": true});
        }
        if (status == "Rejected") {
            status = "Re Submit";
        }
        $button.html(status);
        $button.addClass("btn btn-primary");
        $button.bind("click", function () {
            var _totalHrs = $(this).attr("totalHours");
            var _timeSheetIds = $(this).attr("timeSheetIds");
            var _weekNo = $(this).attr("weekNumber");
            var _approvalId = $(this).attr("approvalId");
            approvedTimeSheet(_totalHrs, _timeSheetIds, _weekNo, _approvalId);
        });
        $td.append($button);
        $tr.append($td);
        $("#weekViewStatus").append($tr);
        disabledDayWeekPanedl("W" + weekNumber, status, view);
    }
    function loadCalender(eventObject) {
        $('#timesheetcalendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            buttonText: {
                today: 'today',
                month: 'month',
                week: 'week',
                day: 'day'
            },
            height: "auto",
            //Random default events
            events: eventObject,
            //timezone: 'GMT',
            editable: true,
            droppable: true,
            weekNumbers: true,
          //  defaultView: 'agendaWeek',
            dayClick: function (date, jsEvent, view) {debugger;
                 console.log(date);
                var d=new Date(date);
                console.log(d);
                createNewTimeSheet(d, jsEvent, view);
            },
            eventClick: function (calEvent, jsEvent, view) {
                editTimeSheet(calEvent, jsEvent, view);
            },
            drop: function (date, allDay) {
                var originalEventObject = $(this).data('eventObject');

                // we need to copy it, so that multiple events don't have a reference to the same object
                var copiedEventObject = $.extend({}, originalEventObject);

                // assign it the date that was reported
                copiedEventObject.start = date;
                copiedEventObject.IsAllDayEvent = 0;
                if (allDay) {
                    copiedEventObject.IsAllDayEvent = 1;
                }

                var fullDate = copiedEventObject.start.format();
                var startDate = convert(fullDate, "/");
                var startTime = formatAMPM(fullDate);

                var timesheetObject = new Object();
                sessionProjectId = <?php echo isset($_SESSION['projectId']) ? $_SESSION['projectId'] : -11 ?>;
                timesheetObject.id = 0;
                timesheetObject.start = startDate;
                timesheetObject.sTime = startTime;
                timesheetObject.end = ""
                timesheetObject.eTime = "";
                timesheetObject.title = copiedEventObject.title;
                timesheetObject.description = "";
                timesheetObject.projectId = sessionProjectId;
                timesheetObject.categoryId = -11;
                timesheetObject.managerId = -11;
                timesheetObject.backgroundColor = $(this).css("background-color");
                timesheetObject.IsAllDayEvent = copiedEventObject.IsAllDayEvent;
                timesheetObject.borderColor = $(this).css("border-color");
                timesheetObject.hours = 0;
                timesheetObject.overtime = 0;
                timesheetObject.view = "";
                var weekNumber = getWeekNumber(startDate);
                checkStatus(weekNumber, "openModel", timesheetObject);

                // $('#timesheetcalendar').fullCalendar('renderEvent', copiedEventObject, true);

                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }

            },
            viewRender: function (view, element) {
                /*  var b = $('#timesheetcalendar').fullCalendar('getDate');
                 getTimeSheetDetails(b.format('L'),view.name); */
                if (view.name == "month") {
                    $('#timesheetcalendar').fullCalendar('option', 'height', 830);
                }
                loadCalenderFromOutSide();
            },
            eventAfterAllRender: function (view) {
                if (view.name == 'month') {
                    $('td.fc-day').each(function () {
                        var date = $(this).attr('data-date');
                        console.log(dates);
                        var index = $.inArray(date, dates);
                        if (index == -1) {
                            $(this).html('<span class="badge label-default">0</span>');
                        } else {
                            $(this).html('<span class="badge label-success">' + count[index].total + '</span>');
                        }
                    });
                }
            },
            eventDrop: function (calEvent, dayDelta, minuteDelta, allDay, revertFunc) {//debugger;

                var timesheetObject = new Object();

                timesheetObject.id = calEvent._id;
                timesheetObject.start = convert(calEvent.start.format(), "/");
                timesheetObject.sTime = formatAMPM(calEvent.start.format());
                timesheetObject.end = convert(calEvent.end.format(), "/");
                timesheetObject.eTime = formatAMPM(calEvent.end.format());
                timesheetObject.title = calEvent.title;
                timesheetObject.description = calEvent.description;
                timesheetObject.projectId = calEvent.projectId;
                timesheetObject.categoryId = calEvent.categoryId;
                timesheetObject.managerId = calEvent.managerId;
                timesheetObject.backgroundColor = calEvent.backgroundColor;
                timesheetObject.IsAllDayEvent = calEvent.IsAllDayEvent;
                timesheetObject.borderColor = calEvent.borderColor;
                timesheetObject.hours = calEvent.hours;
                timesheetObject.overtime = calEvent.overtime;
                timesheetObject.weekNumber = getWeekNumber(timesheetObject.start);
                checkStatus(timesheetObject.weekNumber, "saveModel", timesheetObject);
            },
            eventResize: function (calEvent, delta, revertFunc, jsEvent, ui, view) {
                console.log(calEvent);
                var timesheetObject = new Object();

                timesheetObject.id = calEvent._id;
                timesheetObject.start = convert(calEvent.start.format(), "/");
                timesheetObject.sTime = formatAMPM(calEvent.start.format());
                timesheetObject.end = convert(calEvent.end.format(), "/");
                timesheetObject.eTime = formatAMPM(calEvent.end.format());
                timesheetObject.title = calEvent.title;
                timesheetObject.description = calEvent.description;
                timesheetObject.projectId = calEvent.projectId;
                timesheetObject.categoryId = calEvent.categoryId;
                timesheetObject.managerId = calEvent.managerId;
                timesheetObject.backgroundColor = calEvent.backgroundColor;
                timesheetObject.IsAllDayEvent = calEvent.IsAllDayEvent;
                timesheetObject.borderColor = calEvent.borderColor;
                timesheetObject.hours = calEvent.hours;
                timesheetObject.overtime = calEvent.overtime;
                timesheetObject.weekNumber = getWeekNumber(timesheetObject.start);
                checkStatus(timesheetObject.weekNumber, "saveModel", timesheetObject);
            }
        })/* .on('click', '.fc-agendaWeek-button', function() {
         alert('Week button clicked');
         }).on('click', '.fc-agendaDay-button', function() {
         alert('day button clicked');
         }).on('click', '.fc-month-button', function() {
         alert('month button clicked');
         }).on('click', '.fc-today-button', function() {
         alert('today button clicked');
         }).on('click', '.fc-prev-button', function() {
         alert('Prev button clicked');
         }).on('click', '.fc-next-button', function() {
         alert('Next button clicked');
         }) */;
    }
    function createNewTimeSheet(date, jsEvent, view) {
    console.log(date);
        
       // var fullDate = date.format();
         var fullDate = date;
        var startDate = convert(fullDate, "/");
        var startTime = formatAMPM(fullDate);

        var timesheetObject = new Object();
        sessionProjectId = <?php echo isset($_SESSION['projectId']) ? $_SESSION['projectId'] : -11 ?>;
        timesheetObject.id = 0;
        timesheetObject.start = startDate;
        timesheetObject.sTime = startTime;
        ;
        timesheetObject.end = "";
        timesheetObject.eTime = "";
        timesheetObject.title = "New_Create";
        timesheetObject.description = "";
        timesheetObject.projectId = sessionProjectId;
        timesheetObject.categoryId = -11;
        timesheetObject.managerId = -11;
        timesheetObject.backgroundColor = "#f56954";
        timesheetObject.IsAllDayEvent = 0;
        timesheetObject.borderColor = "#f56954";
        timesheetObject.hours = 0;
        timesheetObject.overtime = 0;
        timesheetObject.view = view.name;
        var weekNumber = getWeekNumber(startDate);
        checkStatus(weekNumber, "openModel", timesheetObject);

    }
    function formatAMPM(date) {
        var now = new Date(date);
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + '' + ampm;
        return strTime;
    }
    function editTimeSheet(calEvent, jsEvent, view) {//debugger;

        var timesheetObject = new Object();
        var startDate = convert(calEvent.sTime, "/")

        timesheetObject.id = calEvent._id;
        timesheetObject.start = startDate;
        timesheetObject.sTime = formatAMPM(calEvent.sTime);
        timesheetObject.end = convert(calEvent.eTime, "/");
        ;
        timesheetObject.eTime = formatAMPM(calEvent.eTime);
        timesheetObject.title = calEvent.title;
        timesheetObject.description = calEvent.description;
        timesheetObject.projectId = calEvent.projectId;
        timesheetObject.categoryId = calEvent.categoryId;
        timesheetObject.managerId = calEvent.managerId;
        timesheetObject.backgroundColor = calEvent.backgroundColor;
        timesheetObject.IsAllDayEvent = calEvent.IsAllDayEvent;
        timesheetObject.borderColor = calEvent.borderColor;
        timesheetObject.hours = calEvent.hours;
        timesheetObject.overtime = calEvent.overtime;
        timesheetObject.view = view.name;
        var weekNumber = getWeekNumber(startDate);
        checkStatus(weekNumber, "openModel", timesheetObject);


        /* var originalEventObject = $(this).data('eventObject');
         var title = prompt('Event Title:', calEvent.title, { buttons: { Ok: true, Cancel: false} });
         if (title){
         calEvent.title = title;
         $('#timesheetcalendar').fullCalendar('renderEvent', calEvent, true);
         } */
    }
    function convert(str, joinOp) {
        console.log(str)
        var date = new Date();
        if (str != null && str != undefined && str != "") {
           /// date = new Date(str);
           date = str;
        }
         console.log("dateis"+date)
        var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
        var day = ("0" + date.getDate()).slice(-2);
        return [mnth, day, date.getFullYear()].join(joinOp);
    }
    function disabledWeekPanel(weekNumber, status) {
        $(".fc-week-number span").each(function () {
            if ($(this).text() == weekNumber && status != "Submit" && status != "Rejected" && status != "Re Submit") {
                var div = $(this).parents("table:first").attr("id", weekNumber);
                $("#" + weekNumber).parents('div:eq(0)').parent("div:eq(0)").css({"background-color": "#CCCCD6", "pointer-events": "none"});
            }
        });
    }
    function disabledDayWeekPanedl(weekNumber, status, view) {
        $(".fc-week-number span").each(function () {
            debugger;
            if ($(this).text() == weekNumber && status != "Submit" && status != "Rejected" && status != "Re Submit") {
                if (view == "agendaWeek") {
                    $(".fc-agendaWeek-view").css({"background-color": "#CCCCD6", "pointer-events": "none"});
                } else if (view == "agendaDay") {
                    $(".fc-agendaDay-view").css({"background-color": "#CCCCD6", "pointer-events": "none"});
                }
            } else {
                $(".fc-agendaWeek-view").css({"background-color": "#ffffff", "pointer-events": "auto"});
                $(".fc-agendaDay-view").css({"background-color": "#ffffff", "pointer-events": "auto"});
            }
        });
    }
    function loadCalenderFromOutSide() {
        var view = $('#timesheetcalendar').fullCalendar('getView');
        var b = $('#timesheetcalendar').fullCalendar('getDate');
        b = b.format('L');
        if (view.name == "month") {
            var date = new Date(b);
            var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
            var day = ("0" + date.getDate()).slice(-2);
            if (parseInt(day) > 1) {
                day = "01";
            }
            b = [mnth, day, date.getFullYear()].join("/");
        }
        getTimeSheetDetails(b, view.name);
    }
    function checkStatus(weekNumber, fcall, timesheetObject) {
        if (weekNumber != null && typeof weekNumber != 'undefined' && weekNumber != "" && weekNumber > 0) {
            $.ajax({
                method: "POST",
                url: "timesheet/api?method=checkApproval",
                asyn: false,
                cache: true,
                data: 'weekNumber=' + weekNumber,
                dataType: "html",
                success: function (data) {//debugger;
                    if (data != null) {
                        var text = JSON.parse(data);
                        if (text["Msg"] == "Pending" || text["Msg"] == "Approved") {
                            return false;
                        } else {
                            if (fcall != null && fcall != "undefined" && fcall != "" && fcall == "openModel") {
                                showTimeSheet(timesheetObject);
                            }
                        }
                    }
                }
            });
        }
    }
</script>
<style>

</style>
