<!-- Content Wrapper. Contains page content -->
 <?php $this->load->view('pages/AddExpense') ?>
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Expense </h1>
          <?php if(isset($_SESSION['project_id']) && $_SESSION['project_name']!='-11'){
              echo $_SESSION['project_name'];
          } ?>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Calendar</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
<!--            <div class="col-md-2">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Expense Category</h4>
                </div>
                <div class="box-body">
                    <div id="external-events">
                     <?php 
                                   
                           foreach ($allcategory as $category) {  
                                      // print_r($client);
                      ?>     
                   the events                  
                    <div class="external-event" style="background-color:<?=$category->color?>" id="<?=$category->cat_id?>"><?php echo $category->cat_name?></div>                   
                    <?php
                    }
                    ?>
                  </div>
                </div> /.box-body 
              </div> /. box 
            </div> /.col -->
            <div class="col-md-10">
              <div class="box box-primary">
                <div class="box-body ">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
            <div class="col-md-2" id="divView">
				<div class="box box-solid">
					<div class="box-body">
						<!-- the events -->
						<table class="table table-bordered" id="statusView" height ="250px;">
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /. box -->
	   </div>
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->