<?php $datearray=array();
 foreach ($daycount as $key => $value) {
    
     $datearray[]=$value['bill_date'];
}
?>
<script>
var count ='<?php echo json_encode(array_values($daycount))?>';
count = JSON.parse(count);
var dates = '<?php echo json_encode(array_values($datearray))?>';
dates = JSON.parse(dates);
</script>
<script src="<?= base_url() ?>js/expance.js"></script>
<script src="<?= base_url() ?>js/bootbox.min.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>chosen/chosen.css">
<script src="<?= base_url() ?>chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.css" />
<script>
    var currency = "$ ";
     var  sessionProjectId = <?php echo isset($_SESSION['project_id'])?$_SESSION['project_id']:-11 ?>;
        function selectClient(projectid){
                   //var projectid = $("#projectid").val();
                   $.ajax({
                        type: "POST",
                        url: "<?=base_url('Expanse/getClient')?>",
                        data:{
                            projectid :projectid
                        },
                        success: function(data){
                            //alert(data);
                            $("#clientid").html(data);
                            if($("#cid").val() != ""){
                                 $('#clientid option').each(function() {
                                     if($(this).val()== $("#cid").val()){
                                        $(this).attr('selected','selected');
                                     }
                                });
                            }
                        }
                    });
        }
        
        function getWeekNumber(d) {
            // Copy date so don't modify original
            d = new Date(d);
            d.setHours(0,0,0);
            // Set to nearest Thursday: current date + 4 - current day number
            // Make Sunday's day number 7
            d.setDate(d.getDate() + 5 - (d.getDay()||1));
            // Get first day of year
            var yearStart = new Date(d.getFullYear(),0,1);
            // Calculate full weeks to nearest Thursday
            var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
            // Return array of year and week number
            return weekNo;
        }
        
        function saveExpense(){
                   var projectid = $("#projectid").val();
                   var clientid = $("#clientid").val();
                   var billno = $("#billno").val();
                   var billdate = $("#billdate").val();
                   var billdesc = $("#billdesc").val();
                   var billamt = $("#billamt").val();
                   var expensecatid = $("#modalTitle").val();
                   var merchant = $("#merchant").val();
                   var mileage = $("#mileage").val();
                   var pk = $("#pk").val();
                   var filepath = $("#filepath").val();
                   var weekNumber = getWeekNumber(billdate);
                   //alert(weekNumber);
                   var file_data = $("#fileinput").prop("files")[0];   
                   var form_data = new FormData();                  
                   form_data.append("file", file_data);
                   form_data.append("projectid", projectid);
                   form_data.append("clientid", clientid);
                   form_data.append("billno", billno);
                   form_data.append("billdate", billdate);
                   form_data.append("billdesc", billdesc);
                   form_data.append("billamt", billamt);
                   form_data.append("expensecatid", expensecatid);
                   form_data.append("merchant", merchant);
                   form_data.append("mileage", mileage);
                   form_data.append("pk", pk);
                   form_data.append("weekno", weekNumber);
                   form_data.append("filepath",filepath);
                   
                   //alert(form_data);        
                   $.ajax({
                        type: "POST",
                        url: "<?=base_url('Expanse/saveExpense')?>",
                        dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        success: function(data){ 
                          //alert(data);  
                          $("#viewImage").html(data);
                          $('#calendar').fullCalendar("rerenderEvents");
                             monthViewCreation();
                           $('#fullCalModal').modal('hide')
                         window.location.reload();
                        }
                    });
        }
        function getExpenseEvent(){ 
                   $.ajax({
                        type: "GET",
                        url: "<?=base_url('Expanse/getExpenseEvent')?>",
                        dataType: "json",
                        data:{},
                        success: function(data){
                        //    alert(JSON.stringify(data));   

                            if(data != null && data.length > 0 && typeof data != "undefined"){                                
                                 for(var i=0;i<data.length;i++){
                                     var addArrEvent = [];
                                     var id = data[i].exp_id;
                                     var name = data[i].cat_name;
                                     var startDate = data[i].bill_date;
                                     var color = data[i].color;
                                     var bill_amt = data[i].bill_amt;
                                     var project_id = data[i].project_id;
                                     var client_id = data[i].client_id;
                                     var bill_no = data[i].bill_no;
                                     var bill_date = data[i].bill_date;
                                     var exp_desc = data[i].exp_desc;
                                     var file_path = data[i].file_path;
                                     var merchant = data[i].merchant;
                                     var tot_mileage = data[i].tot_mileage;
                                     var exp_catid = data[i].exp_catid;
                                     //addArrEvent.push({start:startDate,title:name});
                                     addArrEvent.id = id;
                                     addArrEvent.title = "$"+bill_amt+' '+name;
                                     addArrEvent.start = startDate;
                                     addArrEvent.backgroundColor = color;
                                     addArrEvent.borderColor = color;
                                     addArrEvent.description= bill_amt;
                                     addArrEvent.project_id = project_id;
                                     addArrEvent.client_id = client_id;
                                     addArrEvent.bill_no = bill_no;
                                     addArrEvent.bill_date = bill_date;
                                     addArrEvent.exp_desc = exp_desc;
                                     addArrEvent.merchant = merchant;
                                     addArrEvent.file_path = file_path;
                                     addArrEvent.tot_mileage = tot_mileage;
                                     addArrEvent.exp_catid = exp_catid;
                                     
                                     /*console.log(addArrEvent);
                                     var v = getWeekNumber(bill_date);
                                     $(".fc-week-number span").each(function(){
                                             if($(this).text() == v){
                                                 var div=$(this).parents("table:first").attr("id",v);
                                                 $("#"+v).children().attr("disabled","disabled");    
                                             }
                                             
                                     });*/
                                                    
                                     $('#calendar').fullCalendar('renderEvent',addArrEvent);
                                     
                                 }
                            }
                        }
                    });
        }
        
        function editExpense(obj){
            //console.log(obj);
            selectClient(obj.project_id);
            $('#fullCalModal').modal();
            var showD = obj.bill_date.split('-');
            //showD[1]+"/"+showD[2]+"/"+showD[0]
            $("#billdate").val(obj.bill_date);
            $("#billno").val(obj.bill_no);
            $("#merchant").val(obj.merchant);
            $('#projectid option').each(function() { 
                    if($(this).val()== obj.project_id){
                        $(this).attr('selected','selected');
                    }
            });
            $("#cid").val(obj.client_id);
            $("#billdesc").val(obj.exp_desc);
            $("#mileage").val(obj.tot_mileage);
            $("#billamt").val(obj.description);
            $("#modalTitle").val(obj.exp_catid);
            $("#filepath").val(obj.file_path);
            if(obj.file_path != ""){
                 $("#viewImage").empty();
                 $("#viewImage").append("<img style='margin-left:5px;' src='./uploads/"+obj.file_path+"' width='250px' height='200px'>");
            }
            $("#pk").val(obj.id);
            showDelete();
        }
         function reset(){
            //console.log(obj);
            selectClient();
            $('#fullCalModal').modal();
        
            //showD[1]+"/"+showD[2]+"/"+showD[0]
            $("#billdate").val('');
            $("#billno").val('');
            $("#merchant").val('');
            
            $("#cid").val('');
            $("#billdesc").val('');
            $("#mileage").val('');
            $("#billamt").val('');
            $("#modalTitle").val();
            $("#filepath").val();
            if(obj.file_path != ""){
                 $("#viewImage").empty();
                 $("#viewImage").append("");
            }
            $("#pk").val('');
           }
       
       function showDelete(){
        //alert($("#pk").val());
        if($("#pk").val() != "" && $("#pk").val() > 0){
           $("#delete").show();
        }else{
           $("#delete").show(); 
        }
      }
      function deleteExpense(){
                   var pk = $("#pk").val();
                   $.ajax({
                        type: "POST",
                        url: "<?=base_url('Expanse/deleteExpense')?>",
                        data:{
                            pk :pk
                        },
                        success: function(data){
                           $("#fullCalModal").modal('hide');
                           $('#calendar').fullCalendar('removeEvents', pk);
			   $('#calendar').fullCalendar("rerenderEvents");
                           window.location.reload();
                        }
                    });
      }
      
      function monthViewCreation(){
      //alert("aaa")
                   $.ajax({
                        type: "GET",
                        url: "<?=base_url('Expanse/monthView')?>",
                          dataType: "json",
                        data:{},
                        success: function(data){
                            //alert(JSON.stringify(data));               
                            if(data != null && data.length > 0 && typeof data != "undefined"){
                                 $("#divView").show();
                                 $("#statusView").empty();
                                 $("#statusView").show();
                                 $tr = $("<tr />");
                               $th = $("<th />").attr({"height" : "85px"}).html("Weekly View").css({"padding-top": "71px"});
                                 $tr.append($th);
                                 $("#statusView").append($tr);
                                 for(var i=0;i<data.length;i++){
                                        var totalAmt = data[i].totalAmt;
                                        var weekno = data[i].weekno;
                                        var expenseIds = data[i].expenseId;
                                        var status = data[i].status;
                                        $tr = $("<tr />");
	  			$td = $("<td />").css({"padding-top": "27px"});
	  			$br0 = $("<br />");
	  			$br1= $("<br />");
	  			$br2 = $("<br />");
	  			$br3 = $("<br />");
	  			$button =$("<button />");
	  			$span0 = $("<span />");
	  			$span1 = $("<span />");
	  			$span2 = $("<span />");
	  			$span3 = $("<span />");
                                        var approvalId = 0;
                                        if(status == "" || status == null){
                                         status = "Submit";
                                        }
                                        $span0.text("Week No. : "+weekno);
                                        $span1.text("Total : "+currency+totalAmt);
                                        $td.append($span0);
                                        $td.append($br0);
                                        $td.append($span1);
                                        $td.append($br1);
                                        $button.attr({"type" : "button" , "id" : "approvedId"+weekno ,  "totalAmt" :  totalAmt , "expenseIds" : expenseIds , "weekNo" : weekno , "approvalId" : approvalId});
                                        $button.html(status);
                                        $button.addClass("btn btn-primary");
                                        $button.bind("click" , function(){
                                                        var _totalAmt = $(this).attr("totalAmt");
                                                        var _expenseIds = $(this).attr("expenseIds");
                                                        var _weekNo = $(this).attr("weekNo");
                                                        var _approvalId = $(this).attr("approvalId");
                                                        approvedExpense(_totalAmt , _expenseIds , _weekNo , _approvalId);
                                        });
                                        if(status != "Submit" || totalAmt == "0.0"){
                                                $button.attr({"disabled" : true});
                                        }
                                        if(sessionProjectId=='-11'){
                                        $button.attr({"style" : "display:none"});
                                        }
                                        $td.append($br2);
                                        $td.append($button);
                                        $tr.append($td);
                                        disabledWeekPanel(weekno , status);
                                        $("#statusView").append($tr);
                                        }
                                }
                            }
                    });
      
      }
      
      function approvedExpense(_totalAmt , _expenseIds , _weekNo , _approvalId){
     
       if(sessionProjectId!=='-11'){
  		if(_expenseIds != null && typeof _expenseIds != 'undefined' && _expenseIds != "" && _totalAmt != "0.0"){
  			bootbox.prompt({
  			  	title: "Your total expense is $ "+_totalAmt+". Are you sure want to submit it ? ",
  			  	value: "",
                                placeholder: 'Description',
  			  	callback: function(result) {
                                        //alert(result);
		  			if (result != null && result != "") {
					    	$.ajax({
		  		  			  method: "POST",
		  		  			  url: "<?=base_url('Expanse/approvedExpense')?>",
		  		  			  asyn:true,
		  		  			  cache:true,
		  		  			  data: 'expenseIds='+_expenseIds+"&weekNumber="+_weekNo+"&comment="+result,
		  		  			  dataType: "html",
		  		  			  success: function(data){
		  		  					if(data != null){
		  		  				    	alert( "Expanse Approval Submitted Successfully"+data);
                                                                        
		  		  				    	$("#approvedId"+_weekNo).attr({"disabled" : true}).html("Pending");
		  		  					}
		  		  				}
		  		  			});
			      	 	}else{
			      	 		alert("Please add description on text box..!");
			      	 		return false;
			      	 	}
  			  	}
  			});
  		}else{
  			alert("There is no Expense to approved...Please add expense to approved");
  		}
  	}else{
        alert("Please Select a project to approve");
        }
        }
        function loadCalenderFromOutSide(){
    		window.location.reload();
    	}
        function disabledWeekPanel(weekNumber , status){
    		 $(".fc-week-number span").each(function(){
                    
                 if($(this).text() == weekNumber && status != "Submit" && status != "Rejected" && status != "Re Submit"){
                     var div = $(this).parents("table:first").attr("id",weekNumber);
                     $("#"+weekNumber).parents('div:eq(0)').parent("div:eq(0)").css({"background-color":"#CCCCD6" , "pointer-events":"none"});
                 }
         	});
    	}
   </script>     
<!-- start Model -->
<div id="fullCalModal" class="modal modal-wide  fade">
    <div class="modal-dialog">
       <form enctype="multipart/form-data" name='imageform' role="form" id="imageform" method="post">
        <div class="modal-content">
            <div class="modal-header">
               <button type="reset"  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <h4 id="categoryadding" class="modal-title"></h4>
               
                <input type="hidden" id="modalTitle">
            </div>
            <div id="modalBody" class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="fileinput">Upload Bill</label>
                            <input type="file" id="fileinput">
                        </div>
                        <div id="viewImage"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                             <div class="col-lg-6">
                                 <div class="form-group">
                                    <label for="billdate">Bill Date</label>
                                    <input  class="form-control clsDatePicker" name="billdate" id="billdate" placeholder="Date">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="billno">Bill No</label>
                                    <input type="text" class="form-control" name="billno" id="billno" placeholder="Bill No">
                                </div>
                            </div>                          
                        </div>
                        <div class="form-group">
                           <label for="merchant">Merchant</label>
                           <input type="text" class="form-control" name="merchant" id="merchant" placeholder="Merchant">
                        </div>
                         <div class="row">
                            <div class="col-lg-6">
                                 <div class="form-group">
                                    <label for="project">Project</label>
                                    <select id= "projectid" class="form-control" onchange="javascript:selectClient(this.value);">
                                      <option value="">Select Project..</option>  
                                     <?php
                                        foreach($allprojects as $project){
                                      ?>
                                      <option   value="<?php echo $project->project_id; ?>"  <?php echo (isset($_SESSION['project_id'])) ? (($_SESSION['project_id']==$project->project_id)? 'selected' : '') : '' ;?> ><?php echo $project->project_name; ?></option>
                                      <?php
                                       }
                                      ?>
                                    </select>
                                  </div>
                            </div>
                             <div class="col-lg-6">
                                <div class="form-group">
                                  <label for="client">Client</label>  
                                  <select id= "clientid" class="form-control" data-placeholder="Select Client..">
                                     <?php if(!isset($_SESSION['client_name'])) { ?> <option value="">Select Client..</option> <?php }else{?>
                                        <option value="<?=$_SESSION['client_id'];?>"><?=$_SESSION['client_name'];?></option> 
                                    <?php } ?>
                                  </select>
                                  <input type="hidden" id="cid">
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="notes">Description</label>
                            <textarea  class="form-control" name="billdesc" id="billdesc" placeholder="Description"></textarea>
                        </div>
                        <div class="row">
                             <div class="col-lg-6">
                                 <div class="form-group" id="totalmillege" >
                                     <label for="billno"  >Mileage <span id="rateinput" class="label label-info"></span></label>
                                     <input type="number"  class="form-control" name="mileage" id="mileage" placeholder="Total Mileage" value="">
                                </div>
                            </div>  
<!--                            <div class="col-lg-1">
                                 <div class="form-group" id="millagerate" style="display: none">
                                    <label for="billno">Rate</label>
                                    <span id="rateinput"></span>
                                </div>
                            </div> -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="amount">Amount(USD)</label>
                                    <input type="text" class="form-control" name="billamt" id="billamt">
                                    <input type="hidden" id="pk">
                                    <input type="hidden" id="filepath">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="reset" class="btn btn-default"   data-dismiss="modal">Close</button>
            <button id="delete" name="delete" type="button" onclick="javascript:deleteExpense();" class="btn btn-danger" style="display: none;">Delete</button>
            <button type="button" class="btn btn-primary" onClick="javascript:saveExpense();">Add</button>
        </div>
        </form>
    </div>
</div>