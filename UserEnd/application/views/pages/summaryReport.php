

    <script>
        function selectClient(){
                   var projectid = [];
                   $("#project option:selected").each(function(i, sel){ 
                      projectid.push( $(sel).val()); 
                   });     
                   $.ajax({
                        type: "POST",
                        url: "<?=base_url('summaryReport_ctrl/getClient')?>",
                        data:{
                            projectid :projectid
                        },
                        success: function(data){
                            $("#client").html(data);
                        }
                    });
        }
        
        function selectEmployee(){
                   var projectid = [];
                   $("#project option:selected").each(function(i, sel){ 
                      projectid.push( $(sel).val()); 
                   });    
                   $.ajax({
                        type: "POST",
                        url: "<?=base_url('summaryReport_ctrl/getEmployee')?>",
                        data:{
                            projectid :projectid
                        },
                        success: function(data){
                            $("#employee").html(data);
                        }
                    });
        }
        
       
        function getAssignedWorkingTimeReportforChart(){
                   var projectId = [];
                   $("#project option:selected").each(function(i, sel){ 
                      projectId.push( $(sel).val()); 
                   });
                   var clientId = [];
                   $("#client option:selected").each(function(i, sel){ 
                      clientId.push( $(sel).val()); 
                   });
                   var employeeId=[];
                   $("#employee option:selected").each(function(i, sel){ 
                      employeeId.push( $(sel).val()); 
                   });
                   var sheettime =$("#reservationtime").val();
                   
                   var reportType = $('input[name=reportType]:checked').val();
                   
                   var bar_data = {};
                   var sheetData = [];
                   
                   $("#csv_project_id").val(projectId);
                   $("#csv_client_id").val(clientId);
                   $("#csv_shtime").val(sheettime);      
                   $("#csv_employee_id").val(employeeId);
                   
                   $("#pdf_project_id").val(projectId);
                   $("#pdf_client_id").val(clientId);
                   $("#pdf_shtime").val(sheettime);      
                   $("#pdf_employee_id").val(employeeId);
                   
                   if(sheettime == "" || sheettime == null){
                       window.alert("Select Date");
                       return false;
                   }/*else if(projectId == "" || projectId == null){
                       window.alert("Select Project");
                       return false;
                   }else if(clientId == "" || clientId == null){
                       window.alert("Select Client");
                       return false;
                   }else if(employeeId == "" || employeeId == null){
                       window.alert("Select Employee");
                       return false;
                   }else{*/
                        $.ajax({
                             type: "POST",
                             url: "<?=base_url('summaryReport_ctrl/getAssignedWorkingTimeReportforChart')?>",
                             data:{
                                    projectId:projectId,
                                    clientId:clientId,
                                    employeeId:employeeId,
                                    sheettime:sheettime
                                  },
                             success: function(data){
                                //alert(JSON.stringify(data));
                                if(data != null && data.length > 0 && typeof data != "undefined"){
                                  $("#dispC").show();
                                 for(var i=0;i<data.length;i++){
                                     var start_time = data[i].start_time;
                                     var end_time = data[i].end_time;
                                     var matchDate = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
                                     var firstDateParsed = matchDate.exec(end_time);
                                     var secondDateParsed = matchDate.exec(start_time);

                                     var a = new Date(firstDateParsed[1], firstDateParsed[2], firstDateParsed[3], firstDateParsed[4], firstDateParsed[5], firstDateParsed[6], 0);
                                     var b = new Date(secondDateParsed[1], secondDateParsed[2], secondDateParsed[3], secondDateParsed[4], secondDateParsed[5], secondDateParsed[6], 0);
                                     var differenceInMilliseconds = a.getTime() - b.getTime();

                                     var hrs=differenceInMilliseconds / 1000 / 60 / 60;
                                   
                                    var arr = start_time.split(' ');
                                    var showD = arr[0].split('-');
                                     
                                     var stDate = showD[2]+"-"+showD[1]+"-"+showD[0];
                                     
                                     
                                     sheetData.push([stDate,hrs]);

                                 }  
                                 
                                
                                 console.log(sheetData);

                                 bar_data = {
                                         data: sheetData,
                                         color: "#3c8dbc"
                                     };

                                 console.log(bar_data);    

                                 $.plot("#bar-chart", [bar_data], {
                                   grid: {
                                     borderWidth: 1,
                                     borderColor: "#f3f3f3",
                                     tickColor: "#f3f3f3"
                                   },
                                   series: {
                                     bars: {
                                       show: true,
                                       barWidth: 0.5,
                                       align: "center"
                                     }
                                   },
                                   xaxis: {
                                     mode: "categories",
                                     tickLength: 0
                                   }
                                 });
                               }else{
                                    $("#dispC").hide();
                               }
                             }
                        });
                //}      
        }
       
       
       
        function getAssignedWorkingTimeReportforDetailedReport(){
                   var projectId = [];
                   $("#project option:selected").each(function(i, sel){ 
                      projectId.push( $(sel).val()); 
                   });
                   var clientId = [];
                   $("#client option:selected").each(function(i, sel){ 
                      clientId.push( $(sel).val()); 
                   });
                   var employeeId=[];
                   $("#employee option:selected").each(function(i, sel){ 
                      employeeId.push( $(sel).val()); 
                   });
                   var sheettime =$("#reservationtime").val();
                   
                   var reportType = $('input[name=reportType]:checked').val();
                  
                   var sum = 0;
                   var projectName = "";
                   var total =0;
                   
                   $("#project_id").val(projectId);
                   $("#client_id").val(clientId);
                   $("#shtime").val(sheettime);
                   $("#employee_id").val(employeeId);
                   
                   /*if(sheettime == "" || sheettime == null){
                       window.alert("Select Date");
                       return false;
                   }else if(projectId == "" || projectId == null){
                       window.alert("Select Project");
                       return false;
                   }else if(clientId == "" || clientId == null){
                       window.alert("Select Client");
                       return false;
                   }else if(employeeId == "" || employeeId == null){
                       window.alert("Select Employee");
                       return false;
                   }else{*/
                        $.ajax({
                             type: "POST",
                             url: "<?=base_url('summaryReport_ctrl/getAssignedWorkingTimeReportforDetailedReport')?>",
                             data:{
                                    projectId:projectId,
                                    clientId:clientId,
                                    employeeId:employeeId,
                                    sheettime:sheettime,
                                    reportType:reportType
                                  },
                             success: function(data){
                                //alert(JSON.stringify(data));
                                if(data != null && data.length > 0 && typeof data != "undefined"){
                                    if(reportType == "detailed"){
                                        $("#showSummaryResult").hide();
                                        $("#showSummaryResultTable").empty();
                                        $("#showDetailedResult").show();
                                        $("#showDetailedResultTable").empty();
                                    }
                                   
                                     var $tr=$("<tr\>");
                                     $tr.css({"font-weight":"bold"});
                                     var $td_1=$("<td\>");
                                     $td_1.attr({"width":"25%"});
                                     var $td_2=$("<td\>");
                                     $td_2.attr({"width":"25%"})
                                     var $td_3=$("<td\>");
                                     $td_3.attr({"width":"40%"});
                                     var $td_4=$("<td\>");
                                     $td_4.attr({"width":"10%"})
                                    
                                    if(reportType == "detailed"){
                                        $td_1.append("Task")
                                        $td_2.append("User");
                                        $td_3.append("Time");
                                        $td_4.append("Duration");  

                                        $tr.append($td_1);
                                        $td_2.insertAfter($td_1);
                                        $td_3.insertAfter($td_2);
                                        $td_4.insertAfter($td_3);
                                        $("#showDetailedResultTable").append($tr); 
                                    }
                                     
                                     var employeeName="";
                                    for(var i=0;i<data.length;i++){
                                     var start_time = data[i].start_time;
                                     var end_time = data[i].end_time;
                                     var task_name = data[i].task_name;
                                     var employeeid = data[i].user_id;
                                     projectName = data[i].project_name;
                                     
                                     $("#employee option").each(function() {
                                        if($(this).val() == employeeid) {
                                                employeeName = $(this).text();           
                                        }                        
                                     });
                                     
                                    
                                     //alert(start_time+end_time);
                                     var matchDate = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
                                     var firstDateParsed = matchDate.exec(end_time);
                                     var secondDateParsed = matchDate.exec(start_time);

                                     var a = new Date(firstDateParsed[1], firstDateParsed[2], firstDateParsed[3], firstDateParsed[4], firstDateParsed[5], firstDateParsed[6], 0);
                                     var b = new Date(secondDateParsed[1], secondDateParsed[2], secondDateParsed[3], secondDateParsed[4], secondDateParsed[5], secondDateParsed[6], 0);
                                     var differenceInMilliseconds = a.getTime() - b.getTime();

                                     var hrs=differenceInMilliseconds / 1000 / 60 / 60;
                                     
                                     sum += hrs;
                                      
                                     var $tr1=$("<tr\>");
                                     var $td1=$("<td\>");
                                     var $td2=$("<td\>");
                                     var $td11=$("<td\>");
                                     var $td22=$("<td\>");
                                    if(reportType == "detailed"){ 
                                        $td11.append(projectName+" | "+task_name);
                                        $td22.append(employeeName);
                                        $td1.append(start_time+"-"+end_time);
                                        $td2.append(hrs);  

                                        $tr1.append($td11);
                                        $td22.insertAfter($td11);
                                        $td1.insertAfter($td22);
                                        $td2.insertAfter($td1);
                                        $("#showDetailedResultTable").append($tr1);
                                     }
                                 }  
                                 
                                 if(reportType == "detailed"){
                                     var $tr_2=$("<tr\>");
                                     var $td_3=$("<td\>");
                                     $td_3.attr({"colspan":"4"}); 
                                     $td_3.append("<hr style='border-top: 1px solid #3c8dbc;'>");
                                     $tr_2.append($td_3);
                                     $("#showDetailedResultTable").append($tr_2);
                                 
                                     var $tr2=$("<tr\>");
                                     var $td3=$("<td\>");
                                     $td3.attr({"colspan":"4"});
                                     $td3.css({"padding-left":"84.5%"})
                                     $td3.append("<strong>Total</strong>: "+sum);
                                     $tr2.append($td3);
                                     $("#showDetailedResultTable").append($tr2);
                                 }
                               }else{
                                    $("#showSummaryResult").hide();
                                    $("#showDetailedResult").hide();
                               }
                             }
                        });
                //}      
        }
        
        function getAssignedWorkingTimeReportforSummaryReport(){
                   var projectId = [];
                   $("#project option:selected").each(function(i, sel){ 
                      projectId.push( $(sel).val()); 
                   });
                   var clientId = [];
                   $("#client option:selected").each(function(i, sel){ 
                      clientId.push( $(sel).val()); 
                   });
                   var employeeId=[];
                   $("#employee option:selected").each(function(i, sel){ 
                      employeeId.push( $(sel).val()); 
                   });
                   var sheettime =$("#reservationtime").val();
                   
                   var reportType = $('input[name=reportType]:checked').val();
                  
                   var sum = 0;
                   var projectName = "";
                   var total =0;
                   
                   if(reportType == "summary" && (sheettime == "" || sheettime == null)){
                       window.alert("Select Date");
                       return false;
                   }else if(reportType == "summary" && (projectId == "" || projectId == null)){
                       window.alert("Select Project for summary report");
                       return false;
                   }else if(reportType == "summary" && (clientId == "" || clientId == null)){
                       window.alert("Select Client for summary report");
                       return false;
                   }else if(reportType == "summary" && (employeeId == "" || employeeId == null)){
                       window.alert("Select Employee for summary report");
                       return false;
                   }else{
                        $.ajax({
                             type: "POST",
                             url: "<?=base_url('summaryReport_ctrl/getAssignedWorkingTimeReportforSReport')?>",
                             data:{
                                    projectId:projectId,
                                    clientId:clientId,
                                    employeeId:employeeId,
                                    sheettime:sheettime,
                                    reportType:reportType
                                  },
                             success: function(data){
                                //alert(JSON.stringify(data));
                                if(data != null && data.length > 0 && typeof data != "undefined"){
                                    if(reportType == "summary"){
                                        $("#showDetailedResult").hide();
                                        $("#showDetailedResultTable").empty();
                                        $("#showSummaryResult").show();
                                        $("#showSummaryResultTable").empty();
                                        $("#showSummaryResultTable").html(data);  
                                    }                                    
                               }else{
                                    $("#showSummaryResult").hide();
                                    $("#showDetailedResult").hide();
                               }
                             }
                        });
                }      
        }
        
        function sdReport(){
            getAssignedWorkingTimeReportforChart();
            getAssignedWorkingTimeReportforDetailedReport();
            getAssignedWorkingTimeReportforSummaryReport();
        }
        </script>
 
     </head>
 

<body>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <div class="container">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Summary Report
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                <div class="form-group">
                    <div class="input-group" style="width:60%;">
                      <div class="input-group-addon">
                        <i class="fa fa-clock-o"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="reservationtime">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  
                  <div class="form-group">
                      <select id= "project" class="form-control select2" multiple="multiple" style="width: 60%;" data-placeholder="Select Project.." onchange="javascript:selectClient(this.value);javascript:selectEmployee(this.value);">
                          <option value="">Select Project..</option>  
                          <?php
                            foreach($allprojects as $project){
                              ?>
                                <option value="<?php echo $project->project_id; ?>"><?php echo $project->project_name; ?></option>
                          <?php
                            }
                          ?>
                    </select>
                  </div><!-- /.form-group -->
                  
                  <div class="form-group">
                    <select id= "client" class="form-control select2" multiple="multiple" style="width: 60%;" data-placeholder="Select Client..">
                        <option value="">Select Client..</option>
                    </select>
                  </div><!-- /.form-group -->
                  
                   <div class="form-group">
                    <select id= "employee" class="form-control select2" multiple="multiple" style="width: 60%;" data-placeholder="Select Employee..">
                        <option value="">Select Employee..</option>
                    </select>
                  </div><!-- /.form-group -->
                  
                 <div class="row">
                     <div class="col-lg-4">
                      <div class="input-group" width="40%">
                        <span class="input-group-addon">
                          <input type="radio" value="detailed" name='reportType' checked>
                        </span>
                        <input type="text" readonly="" value="Detailed Report"  class="form-control">
                      </div><!-- /input-group -->
                    </div>
                    <div class="col-lg-4">
                      <div class="input-group" width="40%">
                        <span class="input-group-addon">
                          <input type="radio"  value="summary" name='reportType'>
                        </span>
                          <input type="text" readonly="" value="Summary Report" class="form-control">
                      </div><!-- /input-group -->
                    </div>       
                  </div>
                    
                  <div class="tools" style="padding-top:25px;">
                      <button onclick="javascript:sdReport();" class="btn btn-primary">Apply</button>
                  </div>
                  
             </div><!-- /.box -->
             
             <!-- Bar chart -->
              <div id="dispC" style="display:none;">
                  <div style="margin-right:5px;padding-left:70%;">
                      <table>
                          <tr>
                              <td>
                                    <form role="form" action="<?= base_url('summaryReport_ctrl/create_csv') ?>" method="POST">
                                         <input type="hidden" id="csv_project_id" name="csv_project_id">
                                         <input type="hidden" id="csv_client_id" name="csv_client_id">
                                         <input type="hidden" id="csv_employee_id" name="csv_employee_id">
                                         <input type="hidden" id="csv_shtime" name="csv_shtime">
                                         <button type="submit" class="btn btn-primary">Export to CSV</button>
                                    </form>
                      </td><td style="padding-left:10px;">
                                <form role="form" action="<?= base_url('summaryReport_ctrl/create_pdf') ?>" method="POST">
                                    <input type="hidden" id="pdf_project_id" name="pdf_project_id">
                                    <input type="hidden" id="pdf_client_id" name="pdf_client_id">
                                    <input type="hidden" id="pdf_employee_id" name="pdf_employee_id">
                                    <input type="hidden" id="pdf_shtime" name="pdf_shtime">
                                    <button type="submit" class="btn btn-primary">Export to PDF</button>
                               </form>
                          </td>
                          </tr>
                       </table>   
                   </div>
                <div class="box-body">
                  <div id="bar-chart" style="height: 300px;width:80%;"></div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
             
              <div id="showSummaryResult" style="display:none;padding-top: 30px;padding-bottom:40px;">
                  <table id="showSummaryResultTable" border="0" cellspacing="0" cellpadding="0" width="80%" style="margin-left:40px;">
                  </table>      
              </div>
              
               <div id="showDetailedResult" style="display:none;padding-top: 30px;padding-bottom:40px;">
                  <table id="showDetailedResultTable" border="0" cellspacing="0" cellpadding="0" width="80%" style="margin-left:40px;">
                  </table>      
              </div>
             
            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
        </div>
    <!-- jQuery 2.1.4 -->
    <script src="<?= base_url() ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?= base_url() ?>bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
     <!-- Select2 -->
    <script src="<?= base_url() ?>plugins/select2/select2.full.min.js"></script>
    <!-- Sparkline -->
    <script src="<?= base_url() ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?= base_url() ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?= base_url() ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?= base_url() ?>plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <!-- datepicker -->
    <!--<script src="<?= base_url() ?>plugins/datepicker/bootstrap-datepicker.js"></script>-->
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?= base_url() ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?= base_url() ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url() ?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url() ?>dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= base_url() ?>dist/js/demo.js"></script>
     <script src="<?= base_url() ?>plugins/daterangepicker/daterangepicker.js"></script>
     <!-- FLOT CHARTS -->
    <script src="<?= base_url() ?>plugins/flot/jquery.flot.min.js"></script>
    <script src="<?= base_url() ?>plugins/flot/jquery.flot.categories.min.js"></script>
    <script>
      $(function () {
         $("#project").select2();
         $("#client").select2();
         $("#employee").select2();
         selectClient($("#project").val());
         selectEmployee($("#project").val());
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
            
        var date = new Date();
        var firstday = new Date(date.getFullYear(), date.getMonth(), 1);
        var lastday = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        var fDate = (firstday.getMonth()+1)+"/"+"1"+"/"+firstday.getFullYear()+" 12:00 AM";
        var lDate = (lastday.getMonth()+1)+"/"+lastday.getDate()+"/"+lastday.getFullYear()+" 11:59 PM";
        
        var showDate=fDate+"-"+lDate;
        $('#reservationtime').val(showDate);
        
      });
    </script>
  </body>
</html>

  
 

