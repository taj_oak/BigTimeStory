
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">My Profile</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="<?=  base_url().'userProfile/save'?>" role="form" method="POST" enctype="multipart/form-data">
                    <?php if(isset( $_SESSION['error'])){ ?>
                    <div class="alert-danger"> <?= $_SESSION['error'] ?></div>
                    <?php  unset($_SESSION['error']); } ?>
                    
                    <div class="form-group">
                    <label   class="col-sm-3 control-label">Select File</label>
                      <div class="col-sm-9">
                          <input required="" name="file" type="file" class="form-control" >
                      </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"
                                for="inputEmail3">Company Name</label>
                        <div class="col-sm-9">
                            <input required="" type="text" name="company_name" class="form-control" 
                                    placeholder="Company name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                               for="inputPassword3" >Company Address</label>
                        <div class="col-sm-9">
                            <input type="text" name="company_address" class="form-control"
                                    placeholder="Address"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                               for="inputPassword3" >Company Tel</label>
                        <div class="col-sm-9">
                            <input required="" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" type="tel" name="company_tel" class="form-control"
                                   id="inputPassword3" placeholder="xxx-xxx-xxxx"/>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                               for="inputPassword3" >About Company</label>
                        <div class="col-sm-9">
                            <textarea type="text" name="company_desc" class="form-control"
                                      id="inputPassword3" placeholder="About Company "></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Next</button>
                        </div>
                    </div>
                </form>
            </div>
           
        </div>
    </div>
</div>
<!-- end Model -->