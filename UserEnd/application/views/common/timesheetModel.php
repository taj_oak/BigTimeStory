
<script type="text/javascript">
var gblProjectId = -11;
var gblCategoryId = -11;
function showTimeSheet(timesheetObject){
	  $('#modalTitle').html(timesheetObject.title);
	  if(timesheetObject.title == "New_Create"){
		  timesheetObject.title = "";
		  $('#modalTitle').html("Create New Timesheet");    
                  $("#endDate").val(timesheetObject.start);
                  $("#startTime").val('9:00am');
	  }else{
              $("#endDate").val(timesheetObject.end);
               $("#startTime").val(timesheetObject.sTime);
          }
          console.log(timesheetObject.sTime);
	  $("#timeSheetId").val(timesheetObject.id);
	  $("#managerId").val(timesheetObject.managerId);
	  $("#colorValue").val(timesheetObject.backgroundColor);
	  $("#projectId").val(timesheetObject.projectId);
	  $("#categoryId").val(timesheetObject.categoryId);
	  $("#taskName").val(timesheetObject.title);
	  $("#description").val(timesheetObject.description);
	  $("#startDate").val(timesheetObject.start);
	  
	  
	 
	  $("#hoursWorked").val(timesheetObject.hours);
	  $("#overtimeWorked").val(timesheetObject.overtime);
	// $("#IsAllDayEvent").val();
	  if(timesheetObject.id != null && timesheetObject.id != undefined && timesheetObject.id != "" && parseInt(timesheetObject.id) > 0){
		  $("#delete").show();
	  }else{
		  $("#delete").hide();
	  }
	  $('#timesheetAddEditModel').modal();
	  gblProjectId = timesheetObject.projectId;
	  gblCategoryId = timesheetObject.categoryId;
	  getProjectList(gblProjectId , gblCategoryId);
}
function getProjectList(prevPID,prevCID){
	try{
	  var $option = $("<option />");
	  	$("#projectId").empty();
			$.ajax({
				  method: "GET",
				  url: "timesheet/api?method=getProjectList",
				  asyn: false,
				  cache:true,
				  dataType: "html",
				  data : {},
				  success: function(data){//alert("2");debugger;
						if(data != null){
							var object = JSON.parse(data);
							var projectList = object.project;
							for(var pl = 0 ; pl < projectList.length;pl++){
								var project = projectList[pl];
								$option = $("<option />").attr({"value" : project[0]}).text(project[1]);
								if(project[0] == prevPID){
									$option.attr('selected','selected');
								}
                              $option.attr('color',project[2]);
                              $option.attr('managerId',project[3]);
								$("#projectId").append($option);
							}
		    				loadCategoryView(prevCID);
						}
					}
				});
	}catch(ex){}
}
function loadCategoryView(prevCID){
	try{
	  	var projectId =$("#projectId").val();
	  	var $option = $("<option />");
	    var color = $("#projectId option:selected").attr("color");
	    $("#colorValue").val("#f56954");
	    var managerId = $("#projectId option:selected").attr("managerId");
	    $("#managerId").val(managerId);
	  	$("#categoryId").empty();
	      if(projectId != null && projectId != undefined && projectId > 0){
	      	$.ajax({
					  method: "GET",
					  url: "timesheet/api?method=getProjectCategoryList",
					  asyn:false,
					  data : 'projectId='+projectId,
					  cache:true,
					  dataType: "html",
					  success: function(data){//debugger;
							if(data != null){
								var object = JSON.parse(data);
								var categoryList = object.category;
								if(categoryList != null && categoryList != undefined && categoryList.length > 0){
									for(var pl = 0 ; pl < categoryList.length;pl++){
	    								var category = categoryList[pl];
	    								$option = $("<option />").attr({"value" : category[0]}).text(category[1]);
	    								if(category[0] == prevCID){
	  									$option.attr('selected','selected');
	  								}
	    								$("#categoryId").append($option);
	    							}
								}else{
									$option = $("<option />").attr({"value" : "-11"}).text("---No Category Available---");
									$("#categoryId").append($option);
								}
							}
							loadChosen();
					        resizeChosen();
					        jQuery(window).on('resize', resizeChosen);
						}
					});
		    }else{
		    	$option = $("<option />").attr({"value" : "-11"}).text("---No Category Available---");
		    	$("#categoryId").append($option);
		    	
		    }
	}catch(e){}
}
function loadChosen(){
	var config = {
		      '.chosen-select'           : {},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"},
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
}
function fnSave(){//debugger;
	var timeSheetId = $("#timeSheetId").val();
	var managerId   = $("#managerId").val();
	var colorValue  = $("#colorValue").val();
	var projectId   = $("#projectId").val();
	var categoryId  = $("#categoryId").val();
	var taskName    = $("#taskName").val();
	var description = $("#description").val();
	var startDate   = $("#startDate").val();
	var startTime   = $("#startTime").val();
	var endDate     = $("#endDate").val();
	var endTime     = $("#endTime").val();
	var hours       = $("#hoursWorked").val();
	var overtime    = $("#overtimeWorked").val();
	var IsAllDayEvent = 0;
	var weekNumber = getWeekNumber(startDate);
	
	var timesheetObject           = new Object();
	timesheetObject.id          = timeSheetId;
	timesheetObject.start       = startDate;
    timesheetObject.sTime       = startTime;;
	timesheetObject.end         = endDate;
	timesheetObject.eTime       = endTime;
	timesheetObject.title       = taskName; 
	timesheetObject.description = description;
	timesheetObject.projectId       = projectId 
	timesheetObject.categoryId      = categoryId
	timesheetObject.managerId       = managerId;
	timesheetObject.backgroundColor = colorValue
	timesheetObject.IsAllDayEvent   = IsAllDayEvent;
	timesheetObject.borderColor     = colorValue;
	timesheetObject.hours           = hours;
	timesheetObject.overtime		= overtime;
	timesheetObject.weekNumber 		= weekNumber;
	saveOnDB(timesheetObject);
}
function saveOnDB(timesheetObject){
	try{
		if(timesheetObject != null && timesheetObject != undefined && timesheetObject != ""){
				$.ajax({
					  method: "POST",
					  url: "timesheet/api?method=addOrEditTimeSheet",
					  asyn: false,
					  cache:true,
					  data: {
						  		timeSheetId 	: timesheetObject.id,
						  		startDate   	: timesheetObject.start,
						  		startTime   	: withoutAMPMTime(timesheetObject.sTime),
						  		endDate     	: timesheetObject.end,
						  		endTime   		: withoutAMPMTime(timesheetObject.eTime),
						  		taskName    	: timesheetObject.title,
						  		description 	: timesheetObject.description,
						  		projectId 		: timesheetObject.projectId,
						  		categoryId		: timesheetObject.categoryId,
						  		managerId		: timesheetObject.managerId,
						  		colorvalue		: timesheetObject.backgroundColor,
						  		IsAllDayEvent 	: timesheetObject.IsAllDayEvent,
						  		hours 			: timesheetObject.hours,
						  		overtime		: timesheetObject.overtime,
						  		weekNumber 		: timesheetObject.weekNumber
					  		},
					  dataType: "html",
					  success: function(data){
							if(data != null){
								var successObject = JSON.parse(data);
								if(successObject.IsSuccess){
									alert(successObject.Msg);
									timesheetObject.id = successObject.id;
									$('#timesheetcalendar').fullCalendar('renderEvent', timesheetObject, true);
									$("#timesheetAddEditModel").modal('hide');
									loadCalenderFromOutSide();
								}else{
									alert("creation fialed try again!");
								}
							}
						}
					});
		}
	}catch(ex){}
}
function withoutAMPMTime(time){
	try{
		if(time.indexOf("am") > -1){
			return (time.replace("am" , ":00"));
		}else{
			var timeWithOutAMPM = time.replace("pm" , ":00");
			var timeArr = timeWithOutAMPM.split(":");
			var hours = parseInt(timeArr[0]) + 12;
			return (hours + ":" + parseInt(timeArr[1])+":00");
		}
	}catch(ex){}
}
function getWeekNumber(d) {debugger;
    // Copy date so don't modify original
    d = new Date(d);
    d.setHours(0,0,0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 5 - (d.getDay()||1));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(),0,1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return weekNo;
}
function fnDelete(){
	var timeSheetId = $("#timeSheetId").val();
	bootbox.confirm({ 
	    size: 'small',
	    message: "Are you sure to remove this task ?", 
	    callback: function(result){
		    if(result){
		    	$.ajax({
					  method: "POST",
					  url: "timesheet/api?method=remove",
					  asyn: false,
					  cache:true,
					  data: {
						  		timeSheetId 	: timeSheetId
					  		},
					  dataType: "html",
					  success: function(data){
							if(data != null){
								var successObject = JSON.parse(data);
								if(successObject.IsSuccess){
									//alert(successObject.Msg);
									$('#timesheetcalendar').fullCalendar('removeEvents', timeSheetId);
									$('#timesheetcalendar').fullCalendar("rerenderEvents");
									$("#timesheetAddEditModel").modal('hide');
									loadCalenderFromOutSide();
								}else{
									alert("Deletion fialed try again!");
								}
							}
						}
					});
		    }
		}
	})
}
</script>
<!-- start Model -->
		<div id="timesheetAddEditModel" class="modal fade">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
		                <h4 id="modalTitle" class="modal-title"></h4>
		            </div>
		            <div id="modalBody" class="modal-body">
		            	<form class="form-horizontal" role="form" method="post" action="">
                                    <div class="form-group" >
  									<label class="col-sm-2 control-label" for="project">Project</label>
  									<div class="col-sm-10"><!-- chosen-select -->
										  <select id="projectId" class="form-control " style="width:100%" onchange=javascript:loadCategoryView();>
										   </select> 
		  							</div>
								</div>
								<div class="form-group">
  									<label class="col-sm-2 control-label" for="category">Category</label>
  									<div class="col-sm-10">
										  <select id="categoryId" class="form-control chosen-select" style="width:100%;">
										   </select> 
		  							</div>
								</div>
							    <div class="form-group">
							        <label for="taskName" class="col-sm-2 control-label">Task Name</label>
							        <div class="col-sm-10">
							            <input type="text" class="form-control" id="taskName" name="taskName" placeholder="Enter Task Name" value="">
							        </div>
							    </div>
							    <div class="form-group">
							        <label for="message" class="col-sm-2 control-label">Description</label>
							        <div class="col-sm-10">
							            <textarea class="form-control" rows="4" id="description" name="description" placeholder="Say something about your task"></textarea>
							        </div>
							    </div>
							    <div class="form-group">
							            	<label for="startDate" class="col-sm-2 control-label">Start Date</label>
							        		<div class="col-sm-4">
								            		<input type="text" class="form-control"  id="startDate"/>
								            </div>
								            <label for="startTime" class="col-sm-2 control-label">Start time</label>
								            <div class="col-sm-4">
												    <input type="text" class="form-control" id="startTime"/>
											</div>
							    </div>
							    <div class="form-group">
							    		<label for="endDate" class="col-sm-2 control-label">End Date</label>
							   			<div class="col-sm-4">
												<input type="text" class="form-control" id="endDate"/>
										</div>
										<label for="endTime" class="col-sm-2 control-label">End time</label>
										<div class="col-sm-4">
									       <input type="text" class="time end form-control" id="endTime"/>
									   </div>
								</div>
							    <div class="form-group">
							        <label for="hoursWorked" class="col-sm-2 control-label">Hours</label>
							        <div class="col-sm-4">
							            <input type="text" class="form-control" id="hoursWorked" name="hoursWorked" placeholder="Hours worked" value="">
							        </div>
							        <label for="OvertimeWorked" class="col-sm-2 control-label">Overtime</label>
							        <div class="col-sm-4">
							            <input type="text" class="form-control" id="overtimeWorked" name="OvertimeWorked" placeholder="Overtime worked" value="">
							        </div>
							    </div>
							    <div class="form-group">
							        <div class="col-sm-10 col-sm-offset-2">
							            <! Will be used to display an alert to the user>
							        </div>
							    </div>
							    <!-- Declaration for hidden field -->
							    <input id="timeSheetId" name="timeSheetId" type="hidden" value="0" />
          						<input id="managerId" name="managerId" type="hidden" value="0" />
          						<input id="colorValue" name="colorValue" type="hidden" value="0" />
						</form>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button id="delete" name="delete" type="button" onclick="javascript:fnDelete();" class="btn btn-danger" style="display: none;">Delete</button>
		                <button id="save" name="save" type="button" onclick="javascript:fnSave();" class="btn btn-primary">Save</button>
		            </div>
		        </div>
		    </div>
		</div>
<!-- end Model -->
<link rel="stylesheet" href="<?= base_url() ?>chosen/chosen.css">
<script src="<?= base_url() ?>chosen/chosen.jquery.js" type="text/javascript"></script>
<script src="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.css" />
<script src="<?= base_url() ?>js/bootbox.min.js"></script>

<script>
$(document).ready(function() {
    // initialize input widgets first
    $("#startTime").timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $("#startDate").datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });
 // initialize input widgets first
    $("#endTime").timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $("#endDate").datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });

    $('#endTime').change(function () {
        try{
	    	var startDate   = $("#startDate").val();
	    	var startTime   = $("#startTime").val();
	    	var endDate     = $("#endDate").val();
	    	var endTime     = $("#endTime").val();
	    	if(startDate != null && startDate != undefined && startDate != "" && endDate != null && endDate != undefined && endDate != "" && startTime != null  && startTime != undefined && startTime != ""  && endTime != null && endTime != undefined  && endTime != ""){
	    		toCalculateWorks(startDate , startTime, endDate ,endTime );
	    	}
        }catch(ex){}
    });
    $('#endDate').change(function () {
        try{
	    	var startDate   = $("#startDate").val();
	    	var startTime   = $("#startTime").val();
	    	var endDate     = $("#endDate").val();
	    	var endTime     = $("#endTime").val();
	    	if(startDate != null && startDate != undefined && startDate != "" && endDate != null && endDate != undefined && endDate != "" && startTime != null  && startTime != undefined && startTime != ""  && endTime != null && endTime != undefined  && endTime != ""){
	    		toCalculateWorks(startDate , startTime, endDate ,endTime );
	    	}
	    }catch(ex){}
    });
    $('#startDate').change(function () {
    	try{
        	var startDate   = $("#startDate").val();
	    	var startTime   = $("#startTime").val();
	    	var endDate     = $("#endDate").val();
	    	var endTime     = $("#endTime").val();
	    	if(startDate != null && startDate != undefined && startDate != "" && endDate != null && endDate != undefined && endDate != "" && startTime != null  && startTime != undefined && startTime != ""  && endTime != null && endTime != undefined  && endTime != ""){
	    		toCalculateWorks(startDate , startTime, endDate ,endTime );
	    	}
    	}catch(ex){}
    });
    $('#startTime').change(function () {
        try{
	    	var startDate   = $("#startDate").val();
	    	var startTime   = $("#startTime").val();
	    	var endDate     = $("#endDate").val();
	    	var endTime     = $("#endTime").val();
	    	if(startDate != null && startDate != undefined && startDate != "" && endDate != null && endDate != undefined && endDate != "" && startTime != null  && startTime != undefined && startTime != ""  && endTime != null && endTime != undefined  && endTime != ""){
	    		toCalculateWorks(startDate , startTime, endDate ,endTime );
	    	}
        }catch(ex){}
    });
   
     loadChosen();
    resizeChosen();
    jQuery(window).on('resize', resizeChosen);
});
function toCalculateWorks(startDate , startTime, endDate ,endTime ){
	var totalHr = 0;
	var totalOver = 0;
	try{
		var hours       = 0;
		var overtime    = 0;
		var timeDiff = Math.abs(new Date(endDate).getTime() - new Date(startDate).getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		var sTime = withoutAMPMTime(startTime).split(":");
		var eTime = withoutAMPMTime(endTime).split(":");
		diffDays = parseInt(diffDays) + 1;
		if(parseInt(sTime[0]) == 12){
			sTime[0] = 0;
		}
		var defaultHr = 8*parseInt(diffDays);
		hours =  ((parseInt(eTime[0])*60 + parseInt(eTime[1])) -(parseInt(sTime[0])*60 + parseInt(sTime[1]))) *  diffDays;
		if((hours / 60) >= (8*parseInt(diffDays))){
			totalHr = defaultHr;
			totalOver = (Math.floor(hours / 60) - defaultHr) + "." + (hours % 60);
		}else{
			totalHr = Math.floor(hours / 60) +"." + (hours % 60);
		}
	}catch(ex){}
	$("#hoursWorked").val(totalHr);
    $("#overtimeWorked").val(totalOver);
}
function resizeChosen() {
   $(".chosen-container").each(function() {
       $(this).attr('style', 'width: 100%');
   });   
   $("#categoryId").trigger("chosen:updated")     
}
</script>
