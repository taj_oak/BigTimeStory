 <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
<!--          <div class="user-panel">
            <div class="pull-left image">
            
            </div>
            <div class="pull-left info">
              <p><?= $_SESSION['username']?></p>
             
            </div>
          </div>-->
          <div class="clearfix"></div>
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="<?php echo base_url('dashboard') ?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
<!--              <ul class="treeview-menu">
                  <li><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-circle-o"></i> Dashboard </a></li>
              </ul>-->
            </li>
             <li class="treeview">
              <a href="<?= base_url('timeshet'); ?>">
                <i class="fa fa-files-o"></i>
                <span>Timesheet</span>
                <span class="label label-primary pull-right">1</span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="<?= base_url('timesheet'); ?>"><i class="fa fa-circle-o"></i>Timesheet</a></li>
                   <li><a href="<?= base_url('timesheetreport'); ?>"><i class="fa fa-circle-o"></i>Timesheet Report</a></li>
              </ul>
            </li>
              <li class="treeview">
              <a href="<?= base_url('expanse'); ?>">
                <i class="fa fa-pie-chart"></i>
                <span>expanse</span>
                <span class="label label-primary pull-right">1</span>
              </a>
              <ul class="treeview-menu">
                  <li><a href="<?= base_url('expanse'); ?>"><i class="fa fa-circle-o"></i>expanse</a></li>
              </ul>
                  <ul class="treeview-menu">
                  <li><a href="<?= base_url('expanse/report'); ?>"><i class="fa fa-bars "></i>expanse report</a></li>
              </ul>
            </li>
            
            
          </ul>
<!--          <div>
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Tasks</h4>
                </div>
                <div class="box-body">
                   the events 
                  <div id="external-events">
                    <div class="external-event bg-green">Lunch</div>
                    <div class="external-event bg-yellow">Go home</div>
                    <div class="external-event bg-aqua">Do homework</div>
                    <div class="external-event bg-light-blue">Work on UI design</div>
                    <div class="external-event bg-red">Sleep tight</div>
                    <div class="checkbox">
                      <label for="drop-remove">
                        <input type="checkbox" id="drop-remove">
                        	remove after drop
                      </label>
                    </div>
                  </div>
                </div> /.box-body 
              </div> /. box 
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Task</h3>
                </div>
                <div class="box-body">
                  <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                    <button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>
                    <ul class="fc-color-picker" id="color-chooser">
                      <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                    </ul>
                  </div> /btn-group 
                  <div class="input-group" id="newTask">
                    <input id="new-event" type="text" class="form-control" placeholder="Task name" value="">
                    <div class="input-group-btn">
                      <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
                    </div> /btn-group 
                  </div> /input-group 
                </div>
              </div>
            </div> /.col -->
        </section>
        <!-- /.sidebar -->
      </aside>

