<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Sk Mamtajuddin
 */
class UserModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table_name = 'users';
    }
    function getProjectName($project_id) {
        $this->db->select('project_name');
        return $this->db->get_where('tms_project',array('project_id'=>$project_id))->row()->project_name;
    }
     function getCategoryName($cat_id) {
        $this->db->select('cat_name');
        return $this->db->get_where('tms_categoris',array('cat_id'=>$cat_id))->row()->cat_name;
    }
     function getClientName($client_id) {
        $this->db->select('client_name');
        return $this->db->get_where('tms_client',array('client_id'=>$client_id))->row()->client_name;
    }
     function getManagerName($manager_id) {
        $this->db->select('emp_name');
        return $this->db->get_where('tms_employee',array('user_id'=>$manager_id))->row()->emp_name;
    }

}