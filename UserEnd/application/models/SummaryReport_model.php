<?php
class SummaryReport_model extends CI_Model{
function __construct() {
parent::__construct();
$this->load->database();
}       
 function getAllProject(){
         $this->db->distinct();
         $this->db->select('*');
	 $this->db->from('tms_project');
         $this->db->where("tms_project.manager_id=$_SESSION[user_id]");
	 $query = $this->db->get();
	 return $query->result();
}
function getprojectwiseAllClient($id){
         $this->db->distinct();
         $this->db->select('*');
	 $this->db->from('tms_client');
         $this->db->join('tms_project','tms_project.client_id=tms_client.client_id');
         if($id >0){
            $this->db->where_in('tms_project.project_id', $id);
         }
	 $query = $this->db->get();
	 return $query;
}

function getprojectwiseAllEmployee($id){
         $this->db->distinct();   
         $this->db->select('*');
	 $this->db->from('tms_project');
         $this->db->join('tms_project_employee','tms_project.project_id=tms_project_employee.project_id');
         $this->db->join('users','tms_project_employee.employee_id=users.id');
         $this->db->join('tms_employee','tms_employee.user_id=users.id');
         if($id >0){
            $this->db->where_in('tms_project.project_id', $id);
         }
	 $query = $this->db->get();
	 return $query;
}

function getAssignedWorkingTimeReportforChart($projectid,$clientid,$employeeid,$startTime,$endTime){  
    if($employeeid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.")");

    }else if($projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved'  and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_client.client_id in (".$clientid.")");

    }else if($clientid > 0 && $employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else{
      $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' or u.id in (".$employeeid.") or tms_project.project_id in (".$projectid.") or tms_client.client_id in (".$clientid.")");
  
    }
        $taskid="";
    if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $taskid .= $row['time_sheet_id'].",";
            }
            if(substr($taskid, -1) == ","){
                $taskid = substr($taskid,0,-1);
            }
            $qry = $this->db->query('select tms_time_sheet.start_time,tms_time_sheet.end_time from tms_time_sheet where tms_time_sheet.start_time >"'.$startTime.'" and tms_time_sheet.end_time <"'.$endTime.'" and tms_time_sheet.time_sheet_id in('.$taskid.') order by tms_time_sheet.start_time');
            return $qry->result();
    }
    
}

function getAssignedWorkingTimeReportforDetailedReport($projectid,$clientid,$employeeid,$startTime,$endTime){
    if($employeeid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.")");

    }else if($projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved'  and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_client.client_id in (".$clientid.")");

    }else if($clientid > 0 && $employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else{
      $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' or u.id in (".$employeeid.") or tms_project.project_id in (".$projectid.") or tms_client.client_id in (".$clientid.")");
  
    }
    $taskid="";
    if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $taskid .= $row['time_sheet_id'].",";
            }
            if(substr($taskid, -1) == ","){
                $taskid = substr($taskid,0,-1);
            }
            
            $qry = $this->db->query('select tms_project.project_name,tms_time_sheet.start_time,tms_time_sheet.end_time,tms_time_sheet.task_name,tms_time_sheet.user_id from tms_time_sheet,tms_project where tms_project.project_id=tms_time_sheet.project_id and tms_time_sheet.start_time >"'.$startTime.'" and tms_time_sheet.end_time <"'.$endTime.'" and tms_time_sheet.time_sheet_id in('.$taskid.') order by tms_time_sheet.start_time');
            return $qry->result();
    }
    
}

function getAssignedWorkingTimeReportforSReport($projectid,$clientid,$employeeid,$startTime,$endTime){
    $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");
    $taskid="";
    if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $taskid .= $row['time_sheet_id'].",";
            }
            if(substr($taskid, -1) == ","){
                $taskid = substr($taskid,0,-1);
            }
            
            $qry = $this->db->query('select tms_project.project_name,tms_time_sheet.start_time,tms_time_sheet.end_time,tms_employee.emp_name from tms_time_sheet,tms_project,tms_employee where tms_project.project_id=tms_time_sheet.project_id and tms_time_sheet.user_id = tms_employee.user_id and tms_time_sheet.start_time >"'.$startTime.'" and tms_time_sheet.end_time <"'.$endTime.'" and tms_time_sheet.time_sheet_id in('.$taskid.') order by tms_time_sheet.start_time');
            return $qry;
    }
    
}

function exportToCSV($projectid,$clientid,$employeeid,$startTime,$endTime){
    if($employeeid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.")");

    }else if($projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved'  and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_client.client_id in (".$clientid.")");

    }else if($clientid > 0 && $employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else{
      $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' or u.id in (".$employeeid.") or tms_project.project_id in (".$projectid.") or tms_client.client_id in (".$clientid.")");
  
    }  
    $taskid="";
    if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $taskid .= $row['time_sheet_id'].",";
            }
            if(substr($taskid, -1) == ","){
                $taskid = substr($taskid,0,-1);
            }
            $qry = $this->db->query('select tms_project.project_name as Project_Name,tms_time_sheet.task_name as Task_Name,tms_time_sheet.start_time as Start_Time,tms_time_sheet.end_time as End_Time,tms_employee.emp_name as Employee_Name,time_format(timediff(tms_time_sheet.end_time,tms_time_sheet.start_time),"%H") as Duration from tms_time_sheet,tms_project,tms_employee where tms_project.project_id=tms_time_sheet.project_id and tms_time_sheet.user_id = tms_employee.user_id and tms_time_sheet.start_time >"'.$startTime.'" and tms_time_sheet.end_time <"'.$endTime.'" and tms_time_sheet.time_sheet_id in('.$taskid.') order by tms_time_sheet.start_time,tms_project.project_id');
            return $qry;
    }
    
}

function exportToPDF($projectid,$clientid,$employeeid,$startTime,$endTime){
    if($employeeid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.")");

    }else if($projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved'  and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.")");

    }else if($clientid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else if($employeeid > 0 && $clientid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_client.client_id in (".$clientid.")");

    }else if($clientid > 0 && $employeeid > 0 && $projectid > 0){
        $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved' and u.id in (".$employeeid.") and tms_project.project_id in (".$projectid.") and tms_client.client_id in (".$clientid.")");

    }else{
      $query = $this->db->query("select distinct time_sheet_id from tms_time_sheet_approval tsa,users u,tms_project,tms_client where tsa.submitted_by = u.id and tms_project.project_id=tsa.project_id and tms_project.client_id=tsa.client_id and tsa.status='Approved'");
  
    }  
    $taskid="";
    if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $taskid .= $row['time_sheet_id'].",";
            }
            if(substr($taskid, -1) == ","){
                $taskid = substr($taskid,0,-1);
            }
            $qry = $this->db->query('select tms_project.project_name,tms_time_sheet.task_name,tms_time_sheet.start_time,tms_time_sheet.end_time,tms_employee.emp_name from tms_time_sheet,tms_project,tms_employee where tms_project.project_id=tms_time_sheet.project_id and tms_time_sheet.user_id = tms_employee.user_id and tms_time_sheet.start_time >"'.$startTime.'" and tms_time_sheet.end_time <"'.$endTime.'" and tms_time_sheet.time_sheet_id in('.$taskid.') order by tms_time_sheet.start_time,tms_project.project_id');
            return $qry;
    }
    
}
}