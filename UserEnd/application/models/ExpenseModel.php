<?php

class ExpenseModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
//$this->table_name='tms_expense_cat';
    }

    function getAllCategory() {
        $query = $this->db->query("select distinct cat_id,cat_name,color,mileage from tms_expense_cat,users where users.cmp_id=tms_expense_cat.cmp_id and tms_expense_cat.status='Active' and users.id=" . $_SESSION['user_id']);
        return $query->result();
    }

    function getAllProject() {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->from('tms_project');
        //$this->db->join('users', 'users.cmp_id=tms_project.who_created');
        $this->db->join('tms_project_employee', 'tms_project_employee.project_id=tms_project.project_id');
        
        $this->db->where("tms_project_employee.employee_id=" . $_SESSION['user_id']);
        $query = $this->db->get();
        return $query->result();
    }

    function getprojectwiseAllClient($id) {
        $this->db->distinct();
        $this->db->select('*');
        $this->db->from('tms_client');
        $this->db->join('tms_project', 'tms_project.client_id=tms_client.client_id');
        $this->db->where("tms_project.project_id=" . $id);
        $query = $this->db->get();
        return $query;
    }

    function saveExpense($data) {
        $data['submitted_by'] = $_SESSION['user_id'];

        $this->db->insert('tms_expense', $data);
//     echo $this->db->last_query();
    }

    function getAllExpense() {
        $this->db->distinct();
        $this->db->select('exp_id,project_id,client_id,bill_no,bill_date,exp_desc,file_path,merchant,tot_mileage,exp_catid,cat_name,bill_date,color,bill_amt');
        $this->db->from('tms_expense');
        $this->db->join('tms_expense_cat', 'tms_expense_cat.cat_id=tms_expense.exp_catid');
        $this->db->where("submitted_by=" . $_SESSION['user_id']);
        if (isset($_SESSION['project_id']) && $_SESSION['project_id'] != '-11') {
            $this->db->where("tms_expense.project_id=" . $_SESSION['project_id']);
        }
        $query = $this->db->get();
        return $query->result();
        echo $this->db->last_query();
//die();
    }

    function updateExpense($id, $data) {
        $this->db->where('exp_id', $id);
        $this->db->update('tms_expense', $data);
    }

    function deleteExpense($id) {
        $this->db->where('exp_id', $id);
        $this->db->delete('tms_expense');
    }

    function monthView() {
        $where = '';
        if (isset($_SESSION['project_id']) && $_SESSION['project_id'] != '-11') {
            $where = "Where tms_expense.project_id=$_SESSION[project_id]";
        }

        $query = $this->db->query("SELECT GROUP_CONCAT(`exp_id` ORDER BY `exp_id` ASC SEPARATOR '#') as expenseId,SUM(bill_amt) as totalAmt, weekno,tms_expense_approval.status FROM tms_expense left join tms_expense_approval on tms_expense.weekno = tms_expense_approval.weekNumber $where GROUP BY weekno");
      // echo $this->db->last_query();
        return $query->result();
    }

    function approvedExpense($data) {
        $data['submitted_by'] = $_SESSION['user_id'];
        $now = date('Y-m-d H:i:s');
        $data['date_created'] = $now;
        $data['submit_date'] = $now;
        $data['action_by'] = $_SESSION['manager_id'];
        $data['project_id'] = $_SESSION['project_id'];
        $data['client_id'] = $_SESSION['client_id'];
        $data['who_created'] = $_SESSION['user_id'];
        $data['status'] = "Pending";
//print_r($data);     
        $this->db->insert('tms_expense_approval', $data);
    }

    function getProjectwiseData($post) {
        if (!isset($post['startdate'])) {
//   echo 'i m in 30 days';
            $la_today = date('Y-m-d');
            $ls_30days = date('Y-m-d', strtotime("-30 days"));
            $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY project_id");
//  echo $this->db->last_query();
            $la_data = array();
            $i = 0;
            foreach ($la_result->result() as $row) {
                $la_data[$i]['label'] = $row->project_name;
                $la_data[$i]['data'] = ($row->billamount / $row->totalBillamount) * 100;
                $la_data[$i]['color'] = $row->color;
                $i++;
            }
            return ($la_data);
        } else {
            $la_today = $post['enddate'];
            $ls_30days = $post['startdate'];
            $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY project_id");
// echo $this->db->last_query();
            $la_data = array();
            $i = 0;
            foreach ($la_result->result() as $row) {
                $la_data[$i]['label'] = $row->project_name;
                $la_data[$i]['data'] = ($row->billamount / $row->totalBillamount) * 100;
                $la_data[$i]['color'] = $row->color;
                $i++;
            }
            return ($la_data);
        }
    }

    function getCatwiseData($post) {
        if (!isset($post['startdate'])) {
            $la_today = date('Y-m-d');
            $ls_30days = date('Y-m-d', strtotime("-30 days"));
            $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount ,cat_name , tc.cat_id , tc.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_expense_cat as tc on tc.cat_id = te.exp_catid  where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY exp_catid");

            $la_data = array();
            $i = 0;
            foreach ($la_result->result() as $row) {
                $la_data[$i]['label'] = $row->cat_name;
                $la_data[$i]['data'] = ($row->billamount / $row->totalBillamount) * 100;
                $la_data[$i]['color'] = $row->color;
                $i++;
            }
            return ($la_data);
        } else {
            $la_today = $post['enddate'];
            $ls_30days = $post['startdate'];
            $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount ,cat_name , tc.cat_id , tc.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_expense_cat as tc on tc.cat_id = te.exp_catid  where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY exp_catid");

            $la_data = array();
            $i = 0;
            foreach ($la_result->result() as $row) {
                $la_data[$i]['label'] = $row->cat_name;
                $la_data[$i]['data'] = ($row->billamount / $row->totalBillamount) * 100;
                $la_data[$i]['color'] = $row->color;
                $i++;
            }
            return ($la_data);
        }
    }

    function getbarchartData($post) {
       // print_r($post);
        if (!isset($post['startdate'])) {
          
            $la_today = date('Y-m-d');
            $ls_30days = date('Y-m-d', strtotime("-30 days"));
            $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount,weekno,year ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY weekno");
//  echo "SELECT sum( bill_amt ) as billamount,weekno,year ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY weekno";
            $la_data;
            $i = 0;
            $la_data['datasetss'] = array();
            foreach ($la_result->result() as $row) {
// $la_data['labels'][$i] = "W $row->weekno " . implode($this->getStartAndEndDate($row->weekno, $row->year), ' to ');
                $la_data['labels'][$i] = "W $row->weekno";

//$array[]=$row->billamount;
                $data[] = $row->billamount;
                $la_data['datasetss'][] = array(
                    'label' => "W $row->weekno ",
                    'fillColor' => $row->color,
                    'strokeColor' => $row->color,
                    'pointColor' => $row->color,
                    'pointStrokeColor' => $row->color,
                    'pointHighlightFill' => $row->color,
                    'pointHighlightStroke' => $row->color,
                    'data' => $data,
                );
                $i++;
            }
             
            if(!empty($la_data['datasetss'])) {
                $index = count($la_data['datasetss']) - 1;
                $la_data['datasets'][] = $la_data['datasetss'][$index];
            }
            
            
            
            return ($la_data);
        } else {
           
            $la_today = $post['enddate'];
            $ls_30days = $post['startdate'];
            $where='';
            if(isset($post['projects'])){
                $ids = implode($post['projects'], ',');
                $where = "And te.project_id in ($ids)";
            }
            $la_result = $this->db->query("SELECT sum( bill_amt ) as billamount,weekno,year ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by=$_SESSION[user_id] $where and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY weekno");
 // echo "SELECT sum( bill_amt ) as billamount,weekno,year ,project_name , te.project_id , tp.color,(select sum(bill_amt) FROM tms_expense where submitted_by=$_SESSION[user_id] and bill_date BETWEEN '$ls_30days' AND '$la_today') as totalBillamount   FROM tms_expense as te JOIN tms_project as tp on tp.project_id = te.project_id  where submitted_by=$_SESSION[user_id] $where and bill_date BETWEEN '$ls_30days' AND '$la_today'  GROUP BY weekno";
           // echo $this->db->last_query();
            $la_data;
            $i = 0;
            $la_data['datasetss'] = array();
            foreach ($la_result->result() as $row) {
// $la_data['labels'][$i] = "W $row->weekno " . implode($this->getStartAndEndDate($row->weekno, $row->year), ' to ');
                $la_data['labels'][$i] = "W $row->weekno";

//$array[]=$row->billamount;
                $data[] = $row->billamount;
                $la_data['datasetss'][] = array(
                    'label' => "W $row->weekno ",
                    'fillColor' => $row->color,
                    'strokeColor' => $row->color,
                    'pointColor' => $row->color,
                    'pointStrokeColor' => $row->color,
                    'pointHighlightFill' => $row->color,
                    'pointHighlightStroke' => $row->color,
                    'data' => $data,
                );
                $i++;
            }
            $index = count($la_data['datasetss']) - 1;
            $la_data['datasets'][] = $la_data['datasetss'][$index];
            return ($la_data);
        }
    }

//    function in_array_r($needle, $haystack, $strict = false) {
//    foreach ($haystack as $item) {
//        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
//            return true;
//        }
//    }
//
//    return false;
//}
    function getStartAndEndDate($week, $year) {

        $time = strtotime("1 January $year", time());
        $day = date('w', $time);
        $time += ((7 * $week) + 1 - $day) * 24 * 3600;
        $return[0] = date('d M ', $time);
        $time += 6 * 24 * 3600;
        $return[1] = date('d M ', $time);
        return $return;
    }

}