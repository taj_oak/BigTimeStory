<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DashboardModel
 *
 * @author Sk Mamtajuddin
 */
class DashboardModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->table_name = 'users';
    }

     function getAllClient() {
        $this->db->select('*');
        $this->db->from('tms_client');
        $this->db->join('tms_project', 'tms_project.client_id = tms_client.client_id');
        
        $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id']));
        $query = $this->db->get();
        return $query->result();
    }

    function addProject($data) {
        $data['date_created'] = date('Y-m-d');
        $data['who_created'] = $_SESSION['user_id'];
        $this->db->insert('tms_project', $data);
    }

    function getAllProject() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->join('tms_project_employee', 'tms_project.project_id = tms_project_employee.project_id');
         $this->db->join('tms_client', 'tms_project.client_id=tms_client.client_id');
        $this->db->where(array('tms_project_employee.employee_id' => $_SESSION['user_id']));
        $query = $this->db->get();
        return $query->result();
    }

    function getAllLattestProject() {
        $this->db->select('*');
        $this->db->from('tms_project');
        $this->db->join('tms_client', 'tms_project.client_id = tms_client.client_id');
        $this->db->where(array('tms_project.manager_id' => $_SESSION['user_id']));
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }
    
    function getTotalExpanse() {
   
        $this->db->select('sum(bill_amt) as totalexp');
        $this->db->from('tms_expense');
        $this->db->where(array('tms_expense.submitted_by' => $_SESSION['user_id']));
        $query = $this->db->get();
        return $query->row();
        
    }
 function getTotalTime($status='') {
   
        $this->db->select(' (sum(hours)+sum(overtime) )as totaltime');
        $this->db->from('tms_time_sheet');
        $this->db->where(array('tms_time_sheet.user_id' => $_SESSION['user_id']));
        $query = $this->db->get();
        return $query->row();
        
    }
    function getalltimesheet() {
       
        // print_r($ids);die;
        $this->db->select("*");
        $this->db->from('tms_time_sheet');
        $this->db->join('tms_project', 'tms_project.project_id = tms_project.project_id');
        $this->db->where('tms_time_sheet.user_id =', $_SESSION['user_id']);
        $this->db->order_by("time_sheet_id", "desc");
        $this->db->limit(10);
        //echo $this->db->last_query()
        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }
    function getLetestWeelytimesheet() {
       
        // print_r($ids);die;
        $this->db->select("* ");
        $this->db->from('(Select *,sum(Hours) as thours , sum(overtime) as tovertime from tms_time_sheet Group by weekNumber , project_id ) as q');
        $this->db->join('tms_project', 'tms_project.project_id = q.project_id');

        $this->db->where('q.user_id =', $_SESSION['user_id']);
        $this->db->order_by("q.weekNumber", "desc");
        $this->db->limit(8);
       
        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        foreach ($query->result() as $row) {
           $row->status = $this->getweekstatus($row->weekNumber,$row->project_id) ;
        }
    
        return $query->result();
    }
    function getweekstatus($week,$projectId) {
       $data = $this->db->get_where('tms_time_sheet_approval',array('weekNumber'=>$week,'project_id'=>$projectId));
       if($data->num_rows()>0){
           return $data->row()->status;
       }  else {
           return 'Not Submited';    
       }
    }
     function getallExpanse() {
       
        // print_r($ids);die;
        $this->db->select("*");
        $this->db->from('tms_expense');
        $this->db->join('tms_project', 'tms_project.project_id = tms_project.project_id');
         $this->db->join('`tms_expense_cat`', 'tms_expense.exp_catid = `tms_expense_cat`.`cat_id`');
        $this->db->where('tms_expense.submitted_by =', $_SESSION['user_id']);
        $this->db->group_by("tms_expense.weekno");
        $this->db->order_by("tms_expense.exp_id", "desc");
        $this->db->limit(8);
        //echo $this->db->last_query()
        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }
    function time_elapsed_string($ptime) {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return '0 seconds';
        }

        $a = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }
    
    
      function getLetestWeelytExpanse() {
       
        // print_r($ids);die;
        $this->db->select("* ");
        $this->db->from('(Select *,sum(bill_amt) as total_bill_amt from tms_expense Group by weekno , project_id ) as q');
        $this->db->join('tms_project', 'tms_project.project_id = q.project_id');

        $this->db->where('q.submitted_by =', $_SESSION['user_id']);
        $this->db->order_by("q.weekno", "desc");
        $this->db->limit(8);
       
        // $this->db->where('role !=', 'admin');
        $query = $this->db->get();
        return $query->result();
    }

}