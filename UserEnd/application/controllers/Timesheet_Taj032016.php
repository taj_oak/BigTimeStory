<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashbord
 *
 * @author SKM
 */
class Timesheet extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('form_validation');
        $this->load->model('userModel');
        if (!isset($_SESSION['role']) || $_SESSION['role'] != 'user') {
            redirect(main_url());
        }
//        if(!isset($_SESSION['project_id'])){
//        $_SESSION['project_id'] = -11;
//        $_SESSION['category_id'] = -11;
//        $_SESSION['manager_id'] = -11;
//        $_SESSION['client_id'] = -11;
//        }
      
    }

    public function index() {
        $data['page'] = 'pages/timesheet';
          $data['daycount'] = $this->getTimesheetEventcountday();
        $this->load->view('main', $data);
    }
    
    public function api() {
    	$ret = array(); 
        header('Content-type:text/javascript;charset=UTF-8');
        $method = $_GET["method"];
	switch ($method) {
		    case "add":
		        		$ret = $this->addCalendar($_POST["CalendarStartTime"], $_POST["CalendarEndTime"], $_POST["CalendarTitle"], $_POST["IsAllDayEvent"]);;
		        break;
		    case "list":
		       			 $ret = $this->listCalendar($_POST["showdate"], $_POST["viewtype"]);
		        break;
		    case "update":
		        		  $ret = $this->updateCalendar($_POST["calendarId"], $_POST["CalendarStartTime"], $_POST["CalendarEndTime"]);
		        break; 
		    case "remove":
		        		  $ret = $this->removeCalendar( $_POST["timeSheetId"]);
		        break;
		    case "getTimeSheetDetails":
		    						   $ret = $this->getTimeSheetDetails($_REQUEST["startTime"] , $_REQUEST["endTime"]);
		    	break;
		    case "approved":
		    				$ret = $this->approvedTimeSheet($_REQUEST["timeSheetIds"],$_REQUEST["weekNumber"],$_REQUEST["comment"]);
		    	break;
		    case "checkApproval":
		    					 $ret = $this->checkApprovalStatus($_REQUEST["weekNumber"]);
		    	break;
		    case "setProjectId" :
		    					 $ret = $this->setProjectIdInSession($_REQUEST["projectId"] , $_REQUEST["categoryId"],$_REQUEST["managerId"] ,$_REQUEST["clientId"]);
		    	break;
		    case "getProjectList" : 
		    					   $ret = $this->getProjectList();
		    	break;
		    case "getGlobalProjectList" :
		    							 $ret = $this->getProjectList();
		    	break;
		    case "getProjectCategoryList":
		    							  $ret = $this->getProjectCategoryList($_REQUEST["projectId"]);
		    	break;
		    case "addOrEditTimeSheet":
								        $st = $_REQUEST["startDate"] . " " . $_REQUEST["startTime"];
								        $et = $_REQUEST["endDate"] . " " . $_REQUEST["endTime"];
								        $timeSheetId = isset($_REQUEST["timeSheetId"])? $_REQUEST["timeSheetId"]: 0;
								        if($timeSheetId != null && $timeSheetId != "" && $timeSheetId > 0){
								            $ret = $this->updateDetailedCalendar($_REQUEST["timeSheetId"], $st, $et, 
								                 $_REQUEST["taskName"], isset($_REQUEST["IsAllDayEvent"])?1:0, $_REQUEST["description"], 
								                 $_REQUEST["colorvalue"],$_REQUEST["projectId"] , $_REQUEST["categoryId"],$_REQUEST["managerId"] , $_REQUEST["hours"] , $_REQUEST["overtime"], $_REQUEST["weekNumber"]);
								        }else{
								            $ret = $this->addDetailedCalendar($st, $et,                    
								                $_REQUEST["taskName"], isset($_REQUEST["IsAllDayEvent"])?1:0, $_REQUEST["description"], 
								                $_REQUEST["colorvalue"],$_REQUEST["projectId"] , $_REQUEST["categoryId"] ,$_REQUEST["managerId"] , $_REQUEST["hours"] , $_REQUEST["overtime"], $_REQUEST["weekNumber"]);
								        }        
		        break;
		    case  "addUserDefinedTask" :
		    							$ret = 	$this->AddUserDefinedTask($_REQUEST["taskName"] , $_REQUEST["colorTask"]);
		    	break;
		    case  "getUserDefinedTask" :
		    							$ret = 	$this->getUserDefinedTasks();
		    	break;
	
	
	}
        echo json_encode($ret);
    }
    /* Add New Time sheet */

    function setProjectIdInSession($projectId, $projectCatId, $managerId, $clientId) {
        $_SESSION['project_id'] = -11;
        $_SESSION['category_id'] = -11;
        $_SESSION['manager_id'] = -11;
        $_SESSION['client_id'] = -11;
         $_SESSION['project_name'] ='';
        $_SESSION['category_name'] = '';
        $_SESSION['manager_name'] = '';
        $_SESSION['client_name'] = '';
        if ($projectId > 0) {
            $_SESSION['project_id'] = $projectId;
            $_SESSION['category_id'] = $projectCatId;
            $_SESSION['manager_id'] = $managerId;
            $_SESSION['client_id'] = $clientId;
            $_SESSION['project_name'] = $this->userModel->getProjectName($projectId);
            //$_SESSION['category_name'] = $this->userModel->getCategoryName($projectCatId);
            $_SESSION['manager_name'] = $this->userModel->getManagerName($managerId);
            $_SESSION['client_name'] = $this->userModel->getClientName($clientId);
        }
       // print_r($_SESSION);
        return base64_encode(serialize($_SESSION));
    }

    function addCalendar($st, $et, $sub, $ade) {
        $ret = array();
        try {
            $projectId = $_SESSION['project_id'];
            $projectCatId = $_SESSION['category_id'];
            $managerId = $_SESSION['manager_id'];
            $userId = $_SESSION['user_id'];
            if ($projectId > 0 && $projectId != -11) {
               $sql = "insert into `tms_time_sheet` (`task_name`, `start_time`, `end_time`, `isalldayevent`, `project_id` , `project_category_id` , `manager_id` ,`user_id` ) values ('"
                        . $this->db->real_escape_string($sub) . "', '"
                        . $this->php2MySqlTime($this->js2PhpTime($st)) . "', '"
                        . $this->php2MySqlTime($this->js2PhpTime($et)) . "', '"
                        . $this->db->real_escape_string($ade) . "',"
                        . $projectId . ","
                        . $projectCatId . ","
                        . $managerId . ","
                        . $userId . ")";
            } else if ($projectId == -11) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = "Please select any one project,except `All project` !";
            }
            if ($this->db->query($sql) == false) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = $this->db->_error_message();
            } else {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'add success';
                $ret['Data'] = $this->db->insert_id();
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    /* Add New Task Details */

    function addDetailedCalendar($st, $et, $name, $ade, $dscr, $color,  $projectId, $projectCatId, $managerId ,$hours , $overtime ,$weekNumber) {
        $ret = array();
        try {
            $userId = $_SESSION['user_id'];
          
           
            if ($projectId > 0 && $projectId != -11) {
                $sql = "insert into `tms_time_sheet` (`task_name`, `start_time`, `end_time`, `isalldayevent`, `task_desc`, `color`, `project_id` , `project_category_id` , `manager_id` ,`user_id` , `hours` , `overtime` ,`weekNumber`) values ('"
                        . $name. "', '"
                        . $this->php2MySqlTime($this->js2PhpTime($st)) . "', '"
                        . $this->php2MySqlTime($this->js2PhpTime($et)) . "', '"
                        . $ade. "', '"
                        . $dscr . "', '"
                        . $color . "',"
                        . $projectId . ","
                        . $projectCatId . ","
                        . $managerId . ","
                        . $userId . ","
                        . $hours . ","
                        . $overtime	. ","
                        . $weekNumber			 
                        . " )";
            } else if ($projectId == -11) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = "Please select any one project,except `All project` !";
            }
            $ret["sql"] = $sql;
            if ($this->db->query($sql) == false) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = $this->db->_error_message();
            } else {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'Time sheet created successfully!';
                $ret['id'] = $this->db->insert_id();
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    /* Get All Taks Deatils based on start time and end time */

    function listCalendarByRange($sd, $ed) {
        $ret = array();
        $ret['events'] = array();
        $ret["issort"] = true;
        $ret["start"] = $this->php2JsTime($sd);
        $ret["end"] = $this->php2JsTime($ed);
        $ret['error'] = null;
        try {

            $projectId = (isset($_SESSION['project_id'])) ? $_SESSION['project_id'] :'-11';
            $projectCatId = (isset($_SESSION['category_id'])) ? $_SESSION['category_id'] :'-11' ;
            $managerId = (isset($_SESSION['manager_id'])) ? $_SESSION['manager_id'] :'-11' ;
            $userId = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] :'-11';
          
           
            if ($projectId > 0 && $projectId != -11) {
                $sql = "select * from `tms_time_sheet` where `start_time` between '"
                        . $this->php2MySqlTime($sd) . "' and '" . $this->php2MySqlTime($ed) . "'"
                        . " and `project_id` = " . $projectId . "  and `manager_id` = " . $managerId . " and `user_id` = " . $userId;
            } else if ($projectId == -11) {
                $sql = "select * from `tms_time_sheet` where `start_time` between '"
                        . $this->php2MySqlTime($sd) . "' and '" . $this->php2MySqlTime($ed) . "'"
                        . " and `user_id` = " . $userId;
                //." and `manager_id` = ".$managerId." and `user_id` = ".$userId;
            }
            $ret['sql'] = $sql;
            $handle = $this->db->query($sql);//id#name#desc#sTime#eTime#pId#cId#mId#uId#color#isAllDayEvent#hours#overtime
            foreach ($handle->result() as $row){
                $ret['events'][] = array(
                    $row->time_sheet_id, 
                    $row->task_name,
                	$row->task_desc,
                    $this->php2JsTime($this->mySql2PhpTime($row->start_time)),
                    $this->php2JsTime($this->mySql2PhpTime($row->end_time)),
                    $row->project_id,
                    $row->project_category_id,
                    $row->manager_id,
                    $row->user_id,
                    $row->color,
                	$row->IsAllDayEvent,
                	$row->hours,
                	$row->overtime,
                	$row->weekNumber	
                );
            }
            
            //call view status
            $ret = $this->getViewRecordOnWeekNumber($ret, $sd, $ed);
        } catch (Exception $e) {
            $ret['error'] = $e->getMessage();
        }
        return $ret;
    }
    
    /*Get View status on calender by week number based on start date and end date */
    function  getViewRecordOnWeekNumber($ret, $sd, $ed){
    	//All values from session
    	 $projectId = (isset($_SESSION['project_id'])) ? $_SESSION['project_id'] :'-11';
         $projectCatId = (isset($_SESSION['category_id'])) ? $_SESSION['category_id'] :'-11' ;
         $managerId = (isset($_SESSION['manager_id'])) ? $_SESSION['manager_id'] :'-11' ;
         $userId = (isset($_SESSION['user_id'])) ? $_SESSION['user_id'] :'-11';
          
    	try{
	    	if ($projectId > 0 && $projectId != -11) {
	    		$sql = "select distinct  weekNumber from `tms_time_sheet` where `start_time` between '"
	    				. $this->php2MySqlTime($sd) . "' and '" . $this->php2MySqlTime($ed) . "'"
	    						. " and `project_id` = " . $projectId . "  and `manager_id` = " . $managerId . " and `user_id` = " . $userId . " ORDER BY weekNumber";
	    	} else if ($projectId == -11) {
	    		$sql = "select distinct  weekNumber from `tms_time_sheet` where `start_time` between '"
	    				. $this->php2MySqlTime($sd) . "' and '" . $this->php2MySqlTime($ed) . "'"
	    						. " and `user_id` = " . $userId  . " ORDER BY weekNumber";
	    	}
	    	$ret['sql_view'] = $sql;
	    	$handle = $this->db->query($sql);//weekNumber
	    	$ret['eventview'] = array();
	    	foreach ($handle->result() as $row){
	    		$weekNo = $row->weekNumber;
	    		$sqlView = "SELECT GROUP_CONCAT(`time_sheet_id` ORDER BY `time_sheet_id` ASC SEPARATOR '#') AS TimeSheetId , SUM(`hours`) AS H, sum(`overtime`) AS O , SUM(`hours` + `overtime`) AS TH FROM `tms_time_sheet` WHERE  `user_id` = ".$userId." And weekNumber = ".$weekNo;
	    		$handle1 = $this->db->query($sqlView);//weekNumber
	    		$ret['view'] = array();
	    		foreach ($handle1->result() as $row1){
	    			$ret['view'] = array(
	    					$row1->TimeSheetId,$row1->H,$row1->O,$row1->TH,
	    			);
	    		}
	    		$sqlApproval = "SELECT approval_id , status FROM `tms_time_sheet_approval` WHERE submitted_by=".$userId." And  weekNumber = ".$weekNo;
	    		$handle2 = $this->db->query($sqlApproval);//weekNumber
	    		$approvalId = 0;
	    		$status = "Submit";
	    		foreach ($handle2->result() as $row2){
	    			$approvalId = $row2->approval_id;
	    			$status = $row2->status;
	    		}
	    		array_push($ret['view'], $approvalId, $status);
	    		$ret['eventview'][] = array(
	    			$weekNo=>$ret['view']
	    		);
	    	}
        }catch (Exception $e) {
    		$ret['error'] = $e->getMessage();
    	}
    	return $ret;
    }

    /* Get All Task Based on View Type */

    function listCalendar($day, $type) {
        $phpTime = $this->js2PhpTime($day);
        switch ($type) {
            case "month":
                $st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
                $et = mktime(0, 0, -1, date("m", $phpTime) + 1, 1, date("Y", $phpTime));
                break;
            case "agendaWeek":
                //suppose first day of a week is monday 
               // $sunday = date("d", $phpTime) - date('N', $phpTime)+1;
                //echo date('N', $phpTime);
                $st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
                $et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime) + 7, date("Y", $phpTime));
                break;
            case "agendaDay":
                $st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
                $et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime) + 1, date("Y", $phpTime));
                break;
        }
        return $this->listCalendarByRange($st, $et);
    }

    /* Update the task based on Task id */

    function updateCalendar($id, $st, $et) {
        $ret = array();
        try {
            $projectId = $_SESSION['project_id'];
            $projectCatId = $_SESSION['category_id'];
            $managerId = $_SESSION['manager_id'];
            $userId = $_SESSION['user_id'];
          
           
            if ($projectId > 0 && $projectId != -11) {
                $sql = "update `tms_time_sheet` set"
                        . " `start_time` = '" . $this->php2MySqlTime($this->js2PhpTime($st)) . "', "
                        . " `end_time`   = '" . $this->php2MySqlTime($this->js2PhpTime($et)) . "', "
                        . " `project_id` =  " . $projectId . "  , "
                        . " `project_category_id` = " . $projectCatId
                        . "  where `time_sheet_id`=" . $id;
            } else if ($projectId == -11) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = "Please select any one project,except `All project` !";
            }

            if ($this->db->query($sql) == false) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = $this->db->_error_message();
            } else {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'Succefully';
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    /* Update the Details of Task based on task id */

    function updateDetailedCalendar($id, $st, $et, $name, $ade, $dscr, $color,  $projectId, $projectCatId ,$managerId , $hours , $overtime ,$weekNumber) {
        $ret = array();
        try {
            $userId = $_SESSION['user_id'];
           
            if ($projectId > 0 && $projectId != -11) {
                $sql = "update `tms_time_sheet` set"
                        . " `start_time`='" . $this->php2MySqlTime($this->js2PhpTime($st)) . "', "
                        . " `end_time`='" . $this->php2MySqlTime($this->js2PhpTime($et)) . "', "
                        . " `task_name`='" . $name . "', "
                        . " `isalldayevent`='" . $ade . "', "
                        . " `task_desc`='" . $dscr . "', "
                        . " `color`='" . $color . "' , "
                        . " `project_id` =  " . $projectId . "  , "
                        . " `project_category_id` = " . $projectCatId ." , "
                        . " `manager_id` = " . $managerId . " , "
                        . " `hours` = " . $hours . " , "
                        . " `overtime` = " . $overtime ." , "
                        . " `weekNumber` = " . $weekNumber
                        . "  where `time_sheet_id`=" . $id;
                $ret['sql'] = $sql;
            } else if ($projectId == -11) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = "Please select any one project,except `All project` !";
            }

            if ($this->db->query($sql) == false) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = $this->db->_error_message();
            } else {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'Succefully';
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    /* Detelet the Task based on Task Id */

    function removeCalendar($id) {
        $ret = array();
        try {
            $projectId = $_SESSION['project_id'];
            $projectCatId = $_SESSION['category_id'];
            $managerId = $_SESSION['manager_id'];
            $userId = $_SESSION['user_id'];
          
           
            $sql = "delete from `tms_time_sheet` where `time_sheet_id`=" . $id ." and `user_id` = ".$userId;
            if ($this->db->query($sql) == false) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = $this->db->_error_message();
            } else {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'Succefully Deleted !';
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }
    public function getTimesheetEventcountday() {
        $where = '';
        if (isset($_SESSION['project_id']) && $_SESSION['project_id'] != '-11') {
            $where = "where tms_time_sheet.project_id=$_SESSION[project_id] and tms_time_sheet.user_id=$_SESSION[user_id]";
        }else{
             $where = "where tms_time_sheet.user_id=$_SESSION[user_id]";
        }
        $sql = $this->db->query("select sum(hours+overtime) as total,CAST(start_time AS DATE) as date from tms_time_sheet $where  group by date");
        return $result = $sql->result_array();
    }
    /* Get All Taks Deatils based on start time and end time */

    function getTimeSheetDetails($sd, $ed) {
        $ret = array();
        $ret['events'] = array();
        $sd = $sd . " 00:00:00";
        $ed = $ed . " 23:59:59";
        try {
            $projectId = $_SESSION['project_id'];
            //$projectCatId = $_SESSION['category_id'];
            $managerId = $_SESSION['manager_id'];
            $userId = $_SESSION['user_id'];
          
           
            if ($projectId > 0 && $projectId != -11) {
                $sql = "select * from `tms_time_sheet` where `start_time` between '"
                        . $sd . "' and '" . $ed . "'" . " and `project_id` = " . $projectId . " and `manager_id` = " . $managerId . " and `user_id` = " . $userId;
            } else if ($projectId == -11) {
                $sql = "select * from `tms_time_sheet` where `start_time` between '"
                        . $sd . "' and '" . $ed . "'" . "  and `user_id` = " . $userId;
                // .$sd."' and '". $ed."'"."  and `manager_id` = ".$managerId." and `user_id` = ".$userId;
            }
            $ret["sql"] = $sql;
            $handle = $this->db->query($sql);
            foreach ($handle->result() as $row){
                $ret['events'][] = array(
                    $row->time_sheet_id,
                    $row->task_name,
                    $this->php2JsTime($this->mySql2PhpTime($row->start_time)),
                    $this->php2JsTime($this->mySql2PhpTime($row->end_time)),
                    $row->IsAllDayEvent,
                    0, //more than one day event
                    //$row->InstanceType,
                    0, //Recurring event,
                    $row->color,
                    1, //editable
                    $row->location,
                    ''//$attends
                );
            }
        } catch (Exception $e) {
            $ret['error'] = $e->getMessage();
        }
        return $ret;
    }

    /* Add New Time sheet Approval Status */

    function approvedTimeSheet($timeSheetIds,$weekNumber,$comment) {

        $ret = array();
        try {
            $projectId = $_SESSION['project_id'];
            $projectCatId = $_SESSION['category_id'];
            $clientId = $_SESSION['client_id'];
          	if($clientId == "")$clientId = -11;
           
            $submittedBy = $_SESSION['user_id']; //Take User From Session
            $status = "Pending";
            $action_by = $_SESSION['manager_id'];
            $now = date('Y-m-d H:i:s');
            $sql = "insert into `tms_time_sheet_approval` (`time_sheet_id`, `submit_date`, `submitted_by`,`action_by`, `status` , `project_id` , `client_id` ,`weekNumber` ,`comment`) values ('"
                    . $timeSheetIds . "', '"
                    . $this->php2MySqlTime($this->js2PhpTime($now)) . "', '"
                    . $submittedBy . "', '"
                    . $action_by . "', '"
                    . $status . "' , "
                    . $projectId . " , "
                    . $clientId ." , "
                    . $weekNumber ." , '"
                    . $comment ." ' "
                    . " )";
            if ($this->db->query($sql) == false) {
                $ret['IsSuccess'] = false;
                $ret['Msg'] = $this->db->_error_message();
            } else {
                $ret['IsSuccess'] = true;
                $ret['Msg'] = 'success' + $timeSheetIds;
                $ret['Data'] = $this->db->insert_id();
                $timeSheetIds = str_replace('##', ',', $timeSheetIds);
                $sql ="UPDATE `timesheet_db`.`tms_time_sheet` SET `status` = 'Pending' WHERE `tms_time_sheet`.`time_sheet_id` in($timeSheetIds)";
                $this->db->query($sql);
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    /* Get Time sheet Approval Status */

    function checkApprovalStatus($weekNumber) {
        $ret = array();
        try{
            $submittedBy = $_SESSION['user_id']; //Take User From Session
            
            $sql = "select status from `tms_time_sheet_approval` where `weekNumber` = "
                    .$weekNumber
                    . " and `submitted_by` =" . $submittedBy;
            $ret['sql'] = $sql;
            $handle = $this->db->query($sql);
            $ret['IsSuccess'] = true;
            $ret['Msg'] = "New";
            foreach($handle->result() as $row){
                $ret['IsSuccess'] = true;
                $ret['Msg'] = $row->status;
            }
        }catch(Exception $e){
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    function getProjectList() {
        $userId = $_SESSION['user_id'];
        $ret = array();
        $ret['project'] = array();
        try {
            $sql = "SELECT p.project_id , p.project_name , p.color,p.manager_id , p.client_id FROM `tms_project` p, `tms_project_employee` pe WHERE pe.employee_id = " . $userId . " and p.project_id = pe.project_id";
            $ret['sql'] = $sql;
            $handle = $this->db->query($sql);
            foreach ($handle->result() as $row){
                $ret['project'][] = array(
                    $row->project_id,
                    $row->project_name,
                    $row->color,
                    $row->manager_id,
                    $row->client_id,
                );
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    function getProjectCategoryList($projectId) {
        $userId = $_SESSION['user_id'];
        $ret = array();
        $ret['project'] = array();
        try {
          
           
            $submittedBy = $_SESSION['user_id']; //Take User From Session
            $sql = "SELECT cat_id , cat_name FROM `tms_categoris`   WHERE project_id = " . $projectId . " and enabled_flag = 'Y'";
            $handle = $this->db->query($sql);
            $ret['sql'] = $sql;
            foreach ($handle->result() as $row){
                $ret['category'][] = array(
                    $row->cat_id,
                    $row->cat_name
                );
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }

    function js2PhpTime($jsdate) {
        if (preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches) == 1) {
            return mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
        } else if (preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches) == 1) {
            return mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
        }
    }

    function php2JsTime($phpDate) {
        return date("m/d/Y H:i", $phpDate);
    }

    function php2MySqlTime($phpDate) {
        return date("Y-m-d H:i:s", $phpDate);
    }

    function mySql2PhpTime($sqlDate) {
        $arr = date_parse($sqlDate);
        return mktime($arr["hour"], $arr["minute"], $arr["second"], $arr["month"], $arr["day"], $arr["year"]);
    }

    function gethoursManagerProject($timesheetIds) {
    	$ret = array();
        $userId = $_SESSION['user_id'];
        $ids = implode(',', $timesheetIds);
        // $status = checkApprovalStatusTimesheet($ids);
      
       
        $sql = "select *, tp.project_name , te.emp_name from `tms_time_sheet` as tts Join tms_employee as te on te.user_id = tts.manager_id JOIN tms_project as tp on tp.project_id = tts.project_id where time_sheet_id in($ids)"
                . " and tts.`user_id` = " . $userId . " GROUP BY tts.project_id  ";
        //." and `manager_id` = ".$managerId." and `user_id` = ".$userId;
        $handle = $this->db->query($sql);
        foreach ($handle->result() as $row){
            $ret['timesheet'][] = $row;
        }
        return $ret;
    }

    function checkApprovalStatusTimesheet($timeSheetId) {
        $ret = array();
        try {
            $submittedBy = $_SESSION['user_id']; //Take User From Session
            $sql = "select status from `tms_time_sheet_approval` where `time_sheet_id` like '%"
                    . $this->db->real_escape_string($timeSheetId)
                    . "%' and `submitted_by` =" . $submittedBy;
            $ret['sql'] = $sql;
            $handle = $this->db->query($sql);
            if ($row = mysql_fetch_object($handle)) {

                return $row->status;
            } else {
                return FALSE;
            }
        } catch (Exception $e) {
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e->getMessage();
        }
        return $ret;
    }
    function AddUserDefinedTask($taskName , $taskColor){
    	$ret = array();
    	try {
    		$userId = $_SESSION['user_id']; //Take User From Session
    		$sql = "insert into `tms_userdefined_tasks` (`task_name`, `color`,`user_id`) values ('"
    				. $taskName . "', '"
    				. $taskColor . "', "
    				. $userId
    				. " )";
    		if($this->db->query($sql) == false){
    			$ret['IsSuccess'] = false;
    			$ret['Msg'] = $this->db->_error_message();
    		}else{
    			$ret['IsSuccess'] = true;
    			$ret['Msg'] = 'success' + $timeSheetIds;
    			$ret['Data'] = $this->db->insert_id();
    		}
    	}catch(Exception $e){
    		$ret['IsSuccess'] = false;
    		$ret['Msg'] = $e->getMessage();
    	}
    	return $ret;
    }
    function getUserDefinedTasks() {
    	$ret = array();
    	$ret['userDefinedTasks'] = array();
    	try {
    		$userId = $_SESSION['user_id'];
    		$sql = "select * from `tms_userdefined_tasks` where `user_id` = " . $userId;
    		
    		$ret["sql"] = $sql;
    		$handle = $this->db->query($sql);
    		foreach ($handle->result() as $row){
    			$ret['userDefinedTasks'] = array(
    					$row->id,
    					$row->task_name,
    					$row->color,
    					$row->user_id
    			);
    		}
    	}catch (Exception $e){
    		$ret['error'] = $e->getMessage();
    	}
    	return $ret;
    }
}
