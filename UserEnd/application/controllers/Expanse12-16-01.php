<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashbord
 *
 * @author SKM
 */
class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('client_model');
        $this->load->model('project_model');
        $this->load->model('vendor_model');
        $this->load->model('employeeModel');
        $this->load->helper("url");
        $this->load->library('form_validation');
         if (!isset($_SESSION['role']) || $_SESSION['role']!='admin')
        {
          redirect(main_url());
        }
    }

    public function index() {
       $data['totalcount']= array(
           'totalproject'=> count($this->project_model->getAllProject()),
           'totalclient'=> count($this->project_model->getAllClient()),
           'totalvendor'=>  count($this->vendor_model->getAllVendor()),
           'totalmembers'=>  count($this->employeeModel->getAllUser())
       );
       $data['latestTime'] = $this->employeeModel->getalltimesheet();
       $data['latestMembers'] = $this->employeeModel->getAllLettestUser();
        $data['latestProjects'] = $this->project_model->getAllLattestProject();
       $data['page']='pages/dashboard';
        $this->load->view('main', $data);
    }

}
