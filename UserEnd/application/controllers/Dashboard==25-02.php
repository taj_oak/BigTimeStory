<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashbord
 *
 * @author SKM
 */
class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('form_validation');
        $this->load->model("DashboardModel");
         if (!isset($_SESSION['role']) || $_SESSION['role']!='user')
        {
          redirect(main_url());
        }
    }

    public function index() {
      //  die('ddd');
      $data['totalcount']= array(
          'totalproject'=> count($this->DashboardModel->getAllProject()),
          'totalclient'=> count($this->DashboardModel->getAllClient()),
          'totalExpanse'=> $this->DashboardModel->getTotalExpanse()->totalexp,
          'totalTime'=>  ($this->DashboardModel->getTotalTime()->totaltime)
       );
       $data['latestTime'] = $this->DashboardModel ->getalltimesheet();
       $data['latestExpense'] = $this->DashboardModel->getallExpanse();
       $data['latestweeklyTime']=  $this->DashboardModel->getLetestWeelytimesheet();
    //   $data['latestProjects'] = $this->project_model->getAllLattestProject();
       $data['page']='pages/dashboard';
       $this->load->view('main', $data);
    }
    public function showprojects() {
       // $data['allmanagers'] = $this->DashboardModel->getAllManager();
        $data['allclients'] = $this->DashboardModel->getAllClient();
        $data['allprojects'] = $this->DashboardModel->getAllProject();
        $data['page'] = 'pages/project';
        $this->load->view('main', $data);  
    }

}
