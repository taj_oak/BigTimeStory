<?php

class SummaryReport_ctrl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('summaryReport_model');
        $this->load->helper("url");
        $this->load->library("Pdf");
           if (!isset($_SESSION['role']) || $_SESSION['role']!='user')
        {
          redirect(main_url());
        }
    }
    public function index() {
        $data['allprojects']= $this->summaryReport_model->getAllProject();
       	$data['page']='pages/summaryReport';
         $this->load->view('main', $data);
        //$this->load->view('pages/summaryReport',$data);
    } 
    
    public function getClient($id){
         if(isset($_POST['projectId'])){
            $projectId=implode(", ",$_POST['projectId']);
         }else{
             $projectId = 0;
         }
         $resultC = $this->summaryReport_model->getprojectwiseAllClient($id);
         $optionC="<option value=''>Select Client..</option>";
         if($resultC->num_rows() > 0){
                foreach($resultC->result() as $listC){
                        $optionC.="<option value='".$listC->client_id."'>".$listC->client_name."</option>";
                }
        }
        echo $optionC;
    }
    
    public function getEmployee($id){
         if(isset($_POST['projectId'])){
            $projectId=implode(", ",$_POST['projectId']);
         }else{
             $projectId = 0;
         }
         $resultE = $this->summaryReport_model->getprojectwiseAllEmployee($id);
         $optionE="<option value=''>Select Employee..</option>";
         if($resultE->num_rows() > 0){
                foreach($resultE->result() as $listE){
                        $optionE.="<option value='".$listE->id."'>".$listE->emp_name."</option>";
                }
        }
        echo $optionE;
    }
    
     public function getAssignedWorkingTimeReportforChart(){
         if(isset($_POST['projectId'])){
            $projectId=implode(", ",$_POST['projectId']);
         }else{
             $projectId = 0;
         }
         if(isset($_POST['clientId'])){
            $clientId=implode(", ",$_POST['clientId']);
         }else{
             $clientId = 0;
         }
         if(isset($_POST['employeeId'])){
              $employeeId= implode(", ", $_POST['employeeId']);
         }else{
             $employeeId = 0;
         }
        
         $sheettime=$_POST['sheettime'];
         $timeArr = explode("-",$sheettime);
         $startTime = date_format(new DateTime($timeArr[0]), 'Y-m-d H:i:s');
         $endTime = date_format(new DateTime($timeArr[1]), 'Y-m-d H:i:s');
         header('Content-Type:application/x-json;charset=utf-8');
         echo(json_encode($this->summaryReport_model->getAssignedWorkingTimeReportforChart($projectId,$clientId,$employeeId,$startTime,$endTime)));
           
    }
    
    public function getAssignedWorkingTimeReportforDetailedReport(){
         if(isset($_POST['projectId'])){
            $projectId=implode(", ",$_POST['projectId']);
         }else{
             $projectId = 0;
         }
         if(isset($_POST['clientId'])){
            $clientId=implode(", ",$_POST['clientId']);
         }else{
             $clientId = 0;
         }
         if(isset($_POST['employeeId'])){
              $employeeId= implode(", ", $_POST['employeeId']);
         }else{
             $employeeId = 0;
         }
         $sheettime=$_POST['sheettime'];
         $reportType=$_POST['reportType'];
         $timeArr = explode("-",$sheettime);
         $startTime = date_format(new DateTime($timeArr[0]), 'Y-m-d H:i:s');
         $endTime = date_format(new DateTime($timeArr[1]), 'Y-m-d H:i:s');
         header('Content-Type:application/x-json;charset=utf-8');
         echo(json_encode($this->summaryReport_model->getAssignedWorkingTimeReportforDetailedReport($projectId,$clientId,$employeeId,$startTime,$endTime)));
           
    }
    
    
    public function getAssignedWorkingTimeReportforSReport(){
         $projectId=$_POST['projectId'];
         $clientId=implode(", ",$_POST['clientId']);
         $employeeId=$_POST['employeeId'];
         $sheettime=$_POST['sheettime'];
         $reportType=$_POST['reportType'];
         $timeArr = explode("-",$sheettime);
         $startTime = date_format(new DateTime($timeArr[0]), 'Y-m-d H:i:s');
         $endTime = date_format(new DateTime($timeArr[1]), 'Y-m-d H:i:s');
         $sum= 0;
         $html="<tr><td width='30%'><strong>Project</strong></td><td width='30%'><strong>User</strong></td><td width='30%'><strong>Total Duration</strong></td></tr>";
         foreach($projectId as $pId){
            foreach($employeeId as $eId){ 
               $result = $this->summaryReport_model->getAssignedWorkingTimeReportforSReport($pId,$clientId,$eId,$startTime,$endTime);
               if($result != null){
                if($result->num_rows() > 0){
                    foreach($result->result() as $list){
                            $projectName=$list->project_name;
                            $empName = $list->emp_name;
                            $eTime = strtotime($list->end_time);
                            $sTime = strtotime($list->start_time);
                            $diff = $eTime-$sTime;
                            $hrs = $diff / 3600;
                            $sum += $hrs;    
                    }
                     $html.= "<tr><td>".$projectName."</td><td>".$empName."</td><td>".$sum."</td></tr>";
                     $sum=0;
                }
               }
               
            }
         }
          echo $html; 
    }
    
    public function create_csv(){
        $this->load->dbutil();
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\r\n";
        $projectId=$_POST['csv_project_id'];
        $clientId=$_POST['csv_client_id'];
        $employeeId=  $_POST['csv_employee_id'];
        $sheettime=$_POST['csv_shtime'];
        $timeArr = explode("-",$sheettime);
        $startTime = date_format(new DateTime($timeArr[0]), 'Y-m-d H:i:s');
        $endTime = date_format(new DateTime($timeArr[1]), 'Y-m-d H:i:s');
        $query = $this->summaryReport_model->exportToCSV($projectId,$clientId,$employeeId,$startTime,$endTime);
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download('Detailed_Report.csv', $data);
    }
    
    public function create_pdf(){
        if(isset($_POST['pdf_project_id'])){
            $projectId=$_POST['pdf_project_id'];
         }else{
             $projectId = 0;
         }
         if(isset($_POST['pdf_client_id'])){
            $clientId=$_POST['pdf_client_id'];
         }else{
             $clientId = 0;
         }
         if(isset($_POST['pdf_employee_id'])){
              $employeeId= $_POST['pdf_employee_id'];
         }else{
             $employeeId = 0;
         }
        
         $sheettime=$_POST['pdf_shtime'];
         
         $timeArr = explode("-",$sheettime);
         
         $startTime = date_format(new DateTime($timeArr[0]), 'Y-m-d H:i:s');
         $endTime = date_format(new DateTime($timeArr[1]), 'Y-m-d H:i:s');
         $result = $this->summaryReport_model->exportToPDF($projectId,$clientId,$employeeId,$startTime,$endTime);
          
         $sum=0;
 
         $html="<p><h4><span>Task</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>User</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Time</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>Duration</span></h4></p>";
          
        if($result != null){
                    foreach($result->result() as $list){
                            $projectName=$list->project_name;
                            $taskName=$list->task_name;
                            $empName = $list->emp_name;
                            $eTime = strtotime($list->end_time);
                            $sTime = strtotime($list->start_time);
                            $diff = $eTime-$sTime;
                            $hrs = $diff / 3600;
                            $sum += $hrs;  
                            
                       $html.="<p><span>".$projectName."|".$taskName."</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>".$empName."</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>".$list->start_time."-".$list->end_time."</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>".$hrs."</span>";
                    }
                    
         }
         
         $html.="<p><h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total:".$sum."</h3></p>";
      
        $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $title = "Detailed Report";
        $obj_pdf->SetTitle($title);
        $obj_pdf->SetHeaderData('', '', $title, '');
        $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $obj_pdf->SetDefaultMonospacedFont('helvetica');
        $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $obj_pdf->SetFont('helvetica', '', 9);
        $obj_pdf->setFontSubsetting(false);
        $obj_pdf->AddPage();
        $content = $html;
        ob_end_clean();
        $obj_pdf->writeHTML($content, true, false, true, false, '');
        $obj_pdf->Output('detailedreport.pdf', 'D');
    }
}
?>

