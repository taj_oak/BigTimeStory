<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashbord
 *
 * @author SKM
 */
class Expanse extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->model('client_model');
       // $this->load->model('project_model');
      //  $this->load->model('vendor_model');
      //  $this->load->model('employeeModel');
        $this->load->helper("url");
        $this->load->library('form_validation');
         if (!isset($_SESSION['role']) || $_SESSION['role']!='user')
        {
          redirect(main_url());
        }
    }

    public function index() {
       $data['page']='pages/expanse';
        $this->load->view('main', $data);
    }

}
