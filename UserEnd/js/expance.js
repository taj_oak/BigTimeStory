/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var baseurl = 'http://dev.urplace.in/UserEnd/';
function getTheTeamList(team_id, id) {

    $.ajax({
        url: baseurl + 'groupmanage/getTeamList/',
        type: "POST",
        data: {'team_id': team_id},
        success: function (result) {
            var data = JSON.parse(result);
            $.each(data, function (i, item) {
                var html = " <li> <span><?= $emp->emp_name ?></span> <small class=\"alert-danger\"><?= $emp->email ?></small><div class=\"tools\"> <i class=\"fa fa-trash-o\"><a href=\"<?= base_url('projectAssigned_ctrl/deleteAssignedProject').'/'.$emp->user_id.'/'.$emp->project_id  ?>\"> Delete</a></i> </div> </li>";
                var xyz = "sasasass";
                $('#' + id).append(xyz);
            });
        }
    });
}


$(function () {
    $('#billdate').datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: '+5d',
        changeMonth: true,
        changeYear: true,
        altField: "#idTourDateDetailsHidden",
        altFormat: "yy-mm-dd"
    });
    getExpenseEvent();
    monthViewCreation();
    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
        ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text())
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1070,
                revert: true, // will cause the event to go back to its
                revertDuration: 0,
            });
        });
    }
    ini_events($('#external-events div.external-event'));
    function indexReturn(arr, date) {
        for (var i = 0, len = arr.length; i < len; i++) {
            console.log(date);
            console.log(arr[i].date);
            if (arr[i].date === date) {
                return i;
            }
        }
    }
    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)

    var date = new Date();
    var d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear();
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,'
        },
        buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
        },
        //Random default events
        events: [
        ],
        editable: true,
        droppable: true,
        weekNumbers: true,
        eventClick: function (callEvent) {
            editExpense(callEvent);
        },
        // this allows things to be dropped onto the calendar !!!
        drop: function (date, allDay) { // this function is called when something is dropped
            // alert(date)
            // retrieve the dropped element's stored Event Object
              date = (moment(date).format('YYYY/MM/DD hh:mm'));
            $(this).closest('form').find("input[type=text], textarea,input[type=number],").val("");

            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            console.log(originalEventObject);
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");
            console.log(copiedEventObject);
            var id = $('#external-events div.external-event:contains("' + originalEventObject.title + '")').attr('id');
            var millege = $('#external-events div.external-event:contains("' + originalEventObject.title + '")').attr('data-millege');
            console.log(millege);
            date = new Date(date);
            console.log(date);
            var d = date.getDate();
            var m = date.getMonth() + 1;

            var y = date.getFullYear();
            $('#modalTitle').val(id);
            //alert(originalEventObject.title);
            if (millege > 0) {
                $('#mileage').attr('readonly', false);
                $('#rateinput').html('Rate (' + '$' + millege + ')');
                $('#mileage').keyup(function () {

                    var tm = ($(this).val());
                    var bill = tm * millege;
                    $('#billamt').val(bill);

                });
            } else {
                $('#mileage').attr('readonly', true);
            }

            $("#categoryadding").html('Adding expense on ' + originalEventObject.title);
            $('#billdate').val(m + '/' + d + '/' + y);
            $('#fullCalModal').modal();

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        eventAfterAllRender: function (view) {
            if (view.name == 'month') {
                $('td.fc-day').each(function () {
                    var date = $(this).attr('data-date');
                    console.log(dates);
                    var index =  $.inArray(date,dates);
                    if(index==-1){
                    $(this).html('<span class="badge label-default" >$0</span>');
                  }else{
                      $(this).html('<span class="badge label-success" >$'+count[index].total+'</span>');
                  }
                });
            }
        },
    });

    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
        e.preventDefault();
        //Save color
        currColor = $(this).css("color");
        //Add color effect to button
        $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
    });
    $("#add-new-event").click(function (e) {
        e.preventDefault();
        //Get value and make sure it is not null
        var val = $("#new-event").val();
        if (val.length == 0) {
            return;
        }

        //Create events
        var event = $("<div />");
        event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
        event.html(val);
        $('#external-events').prepend(event);

        //Add draggable funtionality
        ini_events(event);

        //Remove event from text input
        $("#new-event").val("");
    });

});