-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 08, 2015 at 01:05 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `timesheet_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tms_time_sheet_approval`
--

CREATE TABLE IF NOT EXISTS `tms_time_sheet_approval` (
  `approval_id` int(10) NOT NULL,
  `time_sheet_id` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `submit_date` date DEFAULT NULL,
  `submitted_by` int(10) DEFAULT NULL,
  `status` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `action_by` int(10) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `who_created` int(10) DEFAULT NULL,
  `who_updated` int(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_time_sheet_approval`
--

INSERT INTO `tms_time_sheet_approval` (`approval_id`, `time_sheet_id`, `submit_date`, `submitted_by`, `status`, `action_by`, `action_date`, `date_created`, `date_updated`, `who_created`, `who_updated`) VALUES
(8, '1', '1970-01-01', 1, 'Requested', NULL, NULL, NULL, NULL, NULL, NULL),
(10, '2##3', '1970-01-01', 1, 'Requested', NULL, NULL, NULL, NULL, NULL, NULL),
(11, '4', '1970-01-01', 1, 'Approved', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tms_time_sheet_approval`
--
ALTER TABLE `tms_time_sheet_approval`
  ADD PRIMARY KEY (`approval_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tms_time_sheet_approval`
--
ALTER TABLE `tms_time_sheet_approval`
  MODIFY `approval_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
