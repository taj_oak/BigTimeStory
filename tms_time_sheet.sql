-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 08, 2015 at 01:05 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `timesheet_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tms_time_sheet`
--

CREATE TABLE IF NOT EXISTS `tms_time_sheet` (
  `time_sheet_id` int(10) NOT NULL,
  `project_id` int(10) DEFAULT NULL,
  `project_category_id` int(10) DEFAULT NULL,
  `manager_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `task_name` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `task_desc` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `who_created` int(10) DEFAULT NULL,
  `who_updated` int(10) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `location` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `IsAllDayEvent` smallint(6) NOT NULL,
  `color` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `RecurringRule` varchar(500) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_time_sheet`
--

INSERT INTO `tms_time_sheet` (`time_sheet_id`, `project_id`, `project_category_id`, `manager_id`, `user_id`, `task_name`, `task_desc`, `start_time`, `end_time`, `who_created`, `who_updated`, `date_created`, `date_updated`, `location`, `IsAllDayEvent`, `color`, `RecurringRule`) VALUES
(1, NULL, NULL, NULL, NULL, 'Test On 1', NULL, '2015-11-01 00:00:00', '2015-11-01 06:30:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(2, NULL, NULL, NULL, NULL, 'Test', NULL, '2015-11-02 17:00:00', '2015-11-02 22:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(3, NULL, NULL, NULL, NULL, 'new task', NULL, '2015-11-04 09:00:00', '2015-11-04 11:30:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(4, NULL, NULL, NULL, NULL, '3rd task', NULL, '2015-11-10 05:30:00', '2015-11-10 08:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tms_time_sheet`
--
ALTER TABLE `tms_time_sheet`
  ADD PRIMARY KEY (`time_sheet_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tms_time_sheet`
--
ALTER TABLE `tms_time_sheet`
  MODIFY `time_sheet_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
